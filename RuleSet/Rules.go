package RuleSet

type DDLRULE struct {
	DDL           string //DDL
	INCLUDE       string //INCLUDE
	ALL           string //ALL MAPPED UNMAPPED
	MAPPED        string //ALL MAPPED UNMAPPED
	UNMAPPED      string //ALL MAPPED UNMAPPED
	EXCLUDE       string //EXCLUDE
	OPERATIONTYPE string //OPERATIONTYPE
	OPERTYPE      string //操作类型名称
	OBJECTTYPE    string //OBJECTTYPE
	OBJTYPE       string //对象类型
	OBJECTNAME    string //OBJECTNAME
	OBJNAME       string //对象名称
	COMMA         string //逗号
	SEMICOLON     string //分号
}

func InitDDLRule(i DDLRULE) *DDLRULE {
	var ddl *DDLRULE = &DDLRULE{}
	ddl.DDL = i.DDL
	ddl.INCLUDE = i.INCLUDE
	ddl.ALL = i.ALL
	ddl.MAPPED = i.MAPPED
	ddl.UNMAPPED = i.UNMAPPED
	ddl.EXCLUDE = i.EXCLUDE
	ddl.OPERATIONTYPE = i.OPERATIONTYPE
	ddl.OPERTYPE = i.OPERTYPE
	ddl.OBJECTTYPE = i.OBJECTTYPE
	ddl.OBJTYPE = i.OBJTYPE
	ddl.OBJECTNAME = i.OBJECTNAME
	ddl.OBJNAME = i.OBJNAME
	ddl.COMMA = i.COMMA
	ddl.SEMICOLON = i.SEMICOLON
	return ddl
}

type TABLERULE struct {
	TABLE             string //TABLE
	TABLEEXCLUDE      string //TABLEEXCLUDE
	MAP               string //MAP
	TARGET            string // TARGET
	MAPEXCLUDE        string //MAPEXCLUDE
	PARTITION         string //PARTITION
	SUBPARTITION      string //SUBPARTITION
	FILTER            string //FILTER
	Left_parentheses  string //Left_parentheses
	Right_parentheses string //OBJECTTYPE
	ONINSERT          string //ONINSERT
	ONUPDATE          string //ONUPDATE
	ONDELETE          string //ONDELETE
	IGINSERT          string //IGINSERT
	IGUPDATE          string //IGUPDATE
	IGDELETE          string //IGDELETE
	COMMA             string //COMMA
	SEMICOLON         string //SEMICOLON
}

func InitTABLERule(i TABLERULE) *TABLERULE {

	var table *TABLERULE = &TABLERULE{}

	table.TABLE = i.TABLE
	table.TABLEEXCLUDE = i.TABLEEXCLUDE
	table.MAP = i.MAP
	table.TARGET = i.TARGET
	table.MAPEXCLUDE = i.MAPEXCLUDE
	table.PARTITION = i.PARTITION
	table.SUBPARTITION = i.SUBPARTITION
	table.FILTER = i.FILTER
	table.Left_parentheses = i.Left_parentheses
	table.Right_parentheses = i.Right_parentheses
	table.ONINSERT = i.ONINSERT
	table.ONUPDATE = i.ONUPDATE
	table.ONDELETE = i.ONDELETE
	table.IGINSERT = i.IGINSERT
	table.IGUPDATE = i.IGUPDATE
	table.IGDELETE = i.IGDELETE
	table.COMMA = i.COMMA
	table.SEMICOLON = i.SEMICOLON
	return table
}
