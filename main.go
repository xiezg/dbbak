/*************************************************************************
# File Name: main.go
# Author: xiezg
# Mail: xzghyd2008@hotmail.com
# Created Time: 2019-05-30 19:00:36
# Last modified: 2019-09-19 18:27:20
************************************************************************/
package main

import "flag"

//import "bufio"
//import "bytes"
import "time"
import "dbbak/apply"
import "dbbak/manage"
import "dbbak/capture"
import "dbbak/license"
import "github.com/golang/glog"

//import "dbbak/Implementation"

var mode = flag.String("mode", "http", "work mode with:http|capture|apply default http")
var name = flag.String("name", "", "task name")
var reset = flag.Bool("reset", false, "reset apply metadata dirdata file")
var printInfo = flag.Bool("info", false, "print task metadata info")

func main() {

	flag.Parse()

	//inReader := bufio.NewReader(bytes.NewBufferString(`MAP stradm.*, TARGET stradm.*;
	//DDL INCLUDE ALL;`))
	//	scanner := bufio.NewScanner(inReader)
	//	scanner.Split(bufio.ScanLines)
	//
	//	for scanner.Scan() {
	//
	//		temp := scanner.Text()
	//		if len(temp) == 0 || temp == "\r\n" || temp == "\n" || temp == "\t" {
	//			continue
	//		}
	//		Implementation.ReplicatCheck(temp)
	//	}
	//
	//	return

	defer glog.Flush()

	go func() {
		c := time.Tick(3 * time.Second)
		for range c {
			glog.Flush()
		}
	}()

	if !license.CheckLicense() {
		return
	}

	if *mode == "http" {
		manage.Start()
	}

	if *mode == "capture" {
		capture.Start(*name, *printInfo)
	}

	if *mode == "apply" {
		apply.Start(*name, *printInfo, *reset)
	}
}
