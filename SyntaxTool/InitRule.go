package SyntaxTool

import "dbbak/RuleSet"

//加载DDL语法规则
func InitDDL() *RuleSet.DDLRULE {
	var ddl RuleSet.DDLRULE
	ddl.DDL = "DDL"
	ddl.INCLUDE = "INCLUDE"
	ddl.ALL = "ALL"
	ddl.MAPPED = "MAPPED"
	ddl.UNMAPPED = "UNMAPPED"
	ddl.EXCLUDE = "EXCLUDE"

	ddl.OPERATIONTYPE = "OPERATIONTYPE"
	ddl.OPERTYPE = "OPERTYPE" //操作类型名

	ddl.OBJECTTYPE = "OBJECTTYPE"
	ddl.OBJTYPE = "OBJTYPE" //对象类型

	ddl.OBJECTNAME = "OBJECTNAME"
	ddl.OBJNAME = "OBJNAME" //对象名

	ddl.COMMA = ","
	ddl.SEMICOLON = ";"

	r := RuleSet.InitDDLRule(ddl)
	return r
}

//加载TABLE语法规则
func InitTABLE() *RuleSet.TABLERULE {
	var table RuleSet.TABLERULE
	table.TABLE = "TABLE"
	table.MAP = "MAP"
	table.TARGET = "TARGET"
	table.MAPEXCLUDE = "MAPEXCLUDE"
	table.TABLEEXCLUDE = "TABLEEXCLUDE"
	table.PARTITION = "PARTITION"
	table.SUBPARTITION = "SUBPARTITION"
	table.FILTER = "FILTER"
	table.Left_parentheses = "("
	table.Right_parentheses = ")"
	table.ONINSERT = "ONINSERT"
	table.ONUPDATE = "ONUPDATE"
	table.ONDELETE = "ONDELETE"
	table.IGINSERT = "IGINSERT"
	table.IGUPDATE = "IGUPDATE"
	table.IGDELETE = "IGDELETE"
	table.COMMA = ","
	table.SEMICOLON = ";"
	r := RuleSet.InitTABLERule(table)
	return r
}
