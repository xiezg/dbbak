package SyntaxTool

import (
	"fmt"
	"strings"
)

//格式化语法
func Format(contents string) (str []string, err error) {

	var SyntaxElement []string
	if len(contents) > 0 {
		res := []rune(contents)

		var dqm bool = false
		var tmp string
		for _, vau := range res {
			r := fmt.Sprintf("%c", vau)

			switch r {
			case "\"":
				if dqm == false {
					tmp += r
					dqm = true
				} else {
					tmp += r
					dqm = false
				}
				break
			case " ":
				if dqm == false {
					if tmp != "" {
						SyntaxElement = append(SyntaxElement, tmp)
						tmp = ""
					}
				} else {
					tmp += r
				}
				break
			case ",":
				if dqm == false {
					if tmp != "" {
						SyntaxElement = append(SyntaxElement, tmp)
						tmp = ""
					}
					tmp += r
					SyntaxElement = append(SyntaxElement, tmp)
					tmp = ""
				} else {
					tmp += r
				}
				break
			case ";":
				if dqm == false {
					if tmp != "" {
						SyntaxElement = append(SyntaxElement, tmp)
						tmp = ""
					}
					tmp += r
					SyntaxElement = append(SyntaxElement, tmp)
					tmp = ""
				} else {
					tmp += r
				}
				break
			case "(":

				if dqm == false {
					if tmp != "" {
						SyntaxElement = append(SyntaxElement, tmp)
						SyntaxElement = append(SyntaxElement, r)
						tmp = ""
					} else {
						tmp += r
						SyntaxElement = append(SyntaxElement, tmp)
						tmp = ""
					}

				} else {
					tmp += r
				}

				break
			case ")":
				if dqm == false {
					if tmp != "" {
						SyntaxElement = append(SyntaxElement, tmp)
						SyntaxElement = append(SyntaxElement, r)
						tmp = ""
					} else {
						tmp += r
						SyntaxElement = append(SyntaxElement, tmp)
						tmp = ""
					}

				} else {
					tmp += r
				}
			default:
				tmp += r
			}

		}

		if len(SyntaxElement) > 2 {
			str = SyntaxElement
			return str, nil
		} else {
			return nil, fmt.Errorf("ERROR-DRB-00002: Syntax cannot be format")
		}
	} else {
		return nil, fmt.Errorf("ERROR-DRB-00002: Syntax cannot be format")
	}

	return nil, nil
}

//去除文本首尾空格
func ClearBackspace(syntax string) (res string) {
	if syntax != "" {
		syntax = strings.TrimSpace(syntax)
	}
	return syntax
}

//注释检查
func CommentCheck(Syntax string) bool {
	//判断--/# 注释符号, 如果有注释,那么语法也不会进行检查.
	switch {
	case strings.HasPrefix(Syntax, "--") == true:
		return true
	case strings.HasPrefix(Syntax, "#") == true:
		return true
	default:
		return false
	}
}

//比较语法中的关键词和预估关键词是否一致.
func KeyCompare(key string, reskey []string) bool {
	for _, v := range reskey {
		if strings.EqualFold(key, v) == true {
			return true
		}
	}
	return false
}

//效验对象名称
func ObjectNameCheck(on string) bool {
	var result bool = true
	var owner string
	var table string
	var osu int
	if len(on) > 0 {
		ccount := strings.Count(on, "\"")
		pcount := strings.Count(on, ".")
		if ccount%2 != 0 && pcount > 0 {
			return false
		} else {
			oind := strings.Index(on, ".")
			if oind != -1 {
				owner = on[:oind]
				table = on[oind+1:]
				//fmt.Println("owner:", owner)
				//fmt.Println("table:", table)

				ohp := strings.HasPrefix(owner, "\"")
				ohs := strings.HasSuffix(owner, "\"")
				thp := strings.HasPrefix(table, "\"")
				ths := strings.HasSuffix(table, "\"")

				if ohp == ohs {
					if len(owner) < 2 {
						return false
					} else {
						osu = 1
					}
				} else {
					return false
				}

				if osu == 1 {
					if thp == ths {
						if len(table) < 1 {
							return false
						}
					} else {
						return false
					}
				} else {
					return false
				}

			}
		}

	}

	return result

}
