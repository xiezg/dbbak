package SyntaxTool

import (
	"strings"
)

type KeyInfo struct {
	LastKeyName string //上一次效验关键字
	OPERTYPE    string
	OBJTYPE     string
	OBJNAME     string
}

type TABLEKeyInfo struct {
	LastKeyName string //上一次效验关键字
}

type KeyState interface {
	RecDDLKey(key string) (str []string)
}

//DDL预估关键词
func (k *KeyInfo) RecDDLKey(key string) (str []string) {
	d := *InitDDL()
	switch {
	//DDL下一个值必须是INCLUDE
	case strings.EqualFold(key, d.DDL) == true:
		str = append(str, d.INCLUDE)
		return str

	//INCLUDE 下一个值: ALL MAPPED UNMAPPED OPERATIONTYPE OBJECTTYPE OBJECTNAME
	case strings.EqualFold(key, d.INCLUDE) == true:
		str = append(str, d.ALL)
		str = append(str, d.MAPPED)
		str = append(str, d.UNMAPPED)
		str = append(str, d.OPERATIONTYPE)
		str = append(str, d.OBJECTTYPE)
		str = append(str, d.OBJECTNAME)
		return str

	//ALL 下一个值: 逗号, 分号,
	case strings.EqualFold(key, d.ALL) == true:
		str = append(str, d.COMMA)
		str = append(str, d.SEMICOLON)
		return str

	//MAPPED 下一个值: 逗号, 分号,
	case strings.EqualFold(key, d.MAPPED) == true:
		str = append(str, d.COMMA)
		str = append(str, d.SEMICOLON)
		return str

	//UNMAPPED 下一个值: 逗号, 分号,
	case strings.EqualFold(key, d.UNMAPPED) == true:
		str = append(str, d.COMMA)
		str = append(str, d.SEMICOLON)
		return str

	//逗号 下一个值:
	case strings.EqualFold(key, d.COMMA) == true:
		if k.OBJNAME == "" && k.OPERTYPE == "" && k.OBJTYPE == "" {
			str = append(str, d.EXCLUDE)
		} else {
			str = append(str, d.OPERATIONTYPE)
			str = append(str, d.OBJECTTYPE)
			str = append(str, d.OBJECTNAME)
			str = append(str, d.INCLUDE)
			str = append(str, d.EXCLUDE)
		}
		return str

	//EXCLUDE 下一个值:
	case strings.EqualFold(key, d.EXCLUDE) == true:
		str = append(str, d.OPERATIONTYPE)
		str = append(str, d.OBJECTTYPE)
		str = append(str, d.OBJECTNAME)
		return str

	//OBJNAME 下一个值是
	case strings.EqualFold(key, d.OBJNAME) == true:
		str = append(str, d.COMMA)
		str = append(str, d.SEMICOLON)
		k.OBJNAME = d.OBJNAME
		return str

	//OPERTYPE 下一个值是
	case strings.EqualFold(key, d.OPERTYPE) == true:
		str = append(str, d.COMMA)
		str = append(str, d.SEMICOLON)
		k.OPERTYPE = d.OPERTYPE
		return str
	//OBJTYPE 下一个值是
	case strings.EqualFold(key, d.OBJTYPE) == true:
		str = append(str, d.COMMA)
		str = append(str, d.SEMICOLON)
		k.OBJTYPE = d.OBJTYPE
		return str
	}

	return
}

/*func (k *TABLEKeyInfo) RecDDLKey(key string) (str []string) {
	t := *InitTABLE()
	switch key {
	case t.TABLE:
	}

	return
}
*/
