/*************************************************************************
# File Name: ../dbbak/license/license.go
# Author: xiezg
# Mail: xzghyd2008@hotmail.com
# Created Time: 2019-09-12 02:02:23
# Last modified: 2019-09-19 16:48:40
************************************************************************/
package license

import "time"
import "strconv"
import "strings"
import "io/ioutil"
import "license/machineid"
import "github.com/golang/glog"
import "github.com/farmerx/gorsa"

const (
	LicenseFilePath = "../conf/LicenseFile"
)

func CheckLicense() bool {

	Pubkey, err := ioutil.ReadFile("../conf/rsa_public_key.pem")
	if err != nil {
		glog.Errorf("read file ../conf/rsa_public_key.pem fails. err:%v", err)
		return false
	}

	if err := gorsa.RSA.SetPublicKey(string(Pubkey)); err != nil {
		glog.Errorf("set private key fails.err:%v", err)
		return false
	}

	prienctypt, err := ioutil.ReadFile(LicenseFilePath)
	if err != nil {
		glog.Errorf("read license fails. err:%v", err)
		return false
	}

	pubdecrypt, err := gorsa.RSA.PubKeyDECRYPT(prienctypt)
	if err != nil {
		glog.Errorf("decrypt license fails. err:%v", err)
		return false
	}

	msg := strings.Split(string(pubdecrypt), "=")

	if machineid.GetMachineID() != msg[0] {
		glog.Errorf("license invalid")
		return false
	}

	unixTm, err := strconv.Atoi(msg[1])
	if err != nil {
		glog.Errorf("license damage")
		return false
	}

	return int64(unixTm) > time.Now().Unix()
}
