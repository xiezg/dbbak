/*************************************************************************
# File Name: charset/charset.go
# Author: xiezg
# Mail: xzghyd2008@hotmail.com
# Created Time: 2019-08-08 19:11:06
# Last modified: 2019-08-08 19:15:23
************************************************************************/

package charset

import "golang.org/x/text/encoding/unicode"
import "golang.org/x/text/encoding/simplifiedchinese"

func UTF16BigEndianToGBK(b []byte) ([]byte, error) {

	encoder := simplifiedchinese.GBK.NewEncoder()
	decoder := unicode.UTF16(unicode.BigEndian, unicode.IgnoreBOM).NewDecoder()

	b, err := decoder.Bytes(b)
	if err != nil {
		return nil, err
	}

	b, err = encoder.Bytes(b)
	if err != nil {
		return nil, err
	}

	return b, nil
}
