# dbbak

#
go get gopkg.in/goracle.v2

#
install Oracle Client library
https://oracle.github.io/odpi/doc/installation.html#linux


    Download an Oracle 18, 12, or 11.2 “Basic” or “Basic Light” zip file: 64-bit or 32-bit, matching your application architecture.

    Unzip the package into a single directory that is accessible to your application. For example:

    mkdir -p /opt/oracle
    cd /opt/oracle
    unzip instantclient-basic-linux.x64-12.2.0.1.0.zip

    Install the libaio package with sudo or as the root user. For example:

    sudo yum install libaio

    On some Linux distributions this package is called libaio1 instead.

    If there is no other Oracle software on the machine that will be impacted, permanently add Instant Client to the runtime link path. For example, with sudo or as the root user:

    sudo sh -c "echo /opt/oracle/instantclient_18_3 > /etc/ld.so.conf.d/oracle-instantclient.conf"
    sudo ldconfig

    Alternatively, set the environment variable LD_LIBRARY_PATH to the appropriate directory for the Instant Client version. For example:

    export LD_LIBRARY_PATH=/opt/oracle/instantclient_18_3:$LD_LIBRARY_PATH

    If you intend to co-locate optional Oracle configuration files such as tnsnames.ora, sqlnet.ora or oraaccess.xml with Instant Client, then create a network/admin subdirectory, if it does not exist. For example:

    mkdir -p /opt/oracle/instantclient_12_2/network/admin

    This is the default Oracle configuration directory for applications linked with this Instant Client.

    Alternatively, Oracle configuration files can be put in another, accessible directory. Then set the environment variable TNS_ADMIN to that directory name.


yum install -y oracle-instantclient18.3-basic-18.3.0.0.0-3.x86_64
yum install -y oracle-instantclient18.3-tools-18.3.0.0.0-3.x86_64
yum install -y oracle-instantclient18.3-odbc-18.3.0.0.0-3.x86_64
yum install -y oracle-instantclient18.3-devel-18.3.0.0.0-3.x86_64
yum install -y oracle-instantclient18.3-sqlplus-18.3.0.0.0-3.x86_64
yum install -y oracle-instantclient18.3-jdbc-18.3.0.0.0-3.x86_64

ip : httk.net
SID: httk
port: 3577

进入操作系统后, su - oracle : 可以sqlplus / as sysdba
这可里有最高权限

https://docs.oracle.com/en/database/oracle/oracle-database/12.2/lnoci/loe.html

