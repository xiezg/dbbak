/*************************************************************************
# File Name: log/log.go
# Author: xiezg
# Mail: xzghyd2008@hotmail.com
# Created Time: 2019-08-15 19:57:48
# Last modified: 2019-08-20 10:31:28
************************************************************************/

package log

const (
	LOG_LEVEL0 = iota
	LOG_LEVEL1
	LOG_LEVEL2
	LOG_LEVEL3
	LOG_LEVEL4
	LOG_LEVEL5
	LOG_LEVEL6
	LOG_LEVEL7
	LOG_LEVEL8
	LOG_LEVEL9
	LOG_LEVEL10
)
