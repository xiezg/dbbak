/*************************************************************************
# File Name: ini/proc_info.go
# Author: xiezg
# Mail: xzghyd2008@hotmail.com
# Created Time: 2019-06-28 13:34:09
# Last modified: 2019-07-24 23:00:35
************************************************************************/
package ini

import "syscall"

func IsExistFile(fname string) bool {
	return syscall.Access(fname, syscall.F_OK) == nil
}
