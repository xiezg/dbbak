/*************************************************************************
# File Name: ini/ini.go
# Author: xiezg
# Mail: xzghyd2008@hotmail.com
# Created Time: 2019-05-31 18:12:41
# Last modified: 2019-08-15 19:54:24
************************************************************************/
package ini

import "os"
import "fmt"
import "time"
import "github.com/go-ini/ini"

const (
	CONFIG_PATH = "../conf/config.ini"
)

var G_http_listenAddr string
var G_monitor_listenAddr string
var G_AccountMap map[string]string
var G_heartbeat_interval time.Duration
var G_restart_task_interval time.Duration
var G_net_io_timeout time.Duration
var G_taskLevel int

func parse_ini_file() error {

	cfg, err := ini.Load(CONFIG_PATH)
	if err != nil {
		return err
	}

	logSection := cfg.Section("log")
	if logSection == nil {
		return fmt.Errorf("cat't get configure file [log] info")
	}

	G_taskLevel = logSection.Key("taskLevel").MustInt(0)

	globalSection := cfg.Section("global")
	if globalSection == nil {
		return fmt.Errorf("cat't get configure file [global] info")
	}

	G_heartbeat_interval = time.Duration(globalSection.Key("heartbeat_interval").MustInt(3))
	G_net_io_timeout = time.Duration(globalSection.Key("net_io_timeout").MustInt(5))
	G_restart_task_interval = time.Duration(globalSection.Key("restart_task_interval").MustInt(5)) * time.Second

	accountSection := cfg.Section("account")
	if accountSection == nil {
		return fmt.Errorf("cat't get configure file [account] info")
	}

	G_AccountMap = accountSection.KeysHash()

	manageProcSection := cfg.Section("manage_proc")
	if manageProcSection == nil {
		return fmt.Errorf("cat't get configure file [manage proc] info")
	}

	G_http_listenAddr = manageProcSection.Key("http_listen_addr").MustString(":6001")
	G_monitor_listenAddr = manageProcSection.Key("monitor_listen_addr").MustString(":6002")

	return nil
}

func init() {

	ini.PrettyFormat = false

	if err := parse_ini_file(); err != nil {
		fmt.Fprintf(os.Stderr, "Load config file[%s] fails. errmsg:%v\n", CONFIG_PATH, err)
		os.Exit(1)
	}
}
