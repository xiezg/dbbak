/*************************************************************************
# File Name: apply/servcie.go
# Author: xiezg
# Mail: xzghyd2008@hotmail.com
# Created Time: 2019-06-28 18:22:11
# Last modified: 2019-09-19 22:36:45
************************************************************************/
package apply

import "os"
import "io"
import "fmt"
import "time"
import "path"
import "sync"
import "bytes"
import "dbbak/oci"
import "dbbak/log"
import "dbbak/task"
import "encoding/hex"
import "github.com/golang/glog"

const (
	READ_BUF_SIZE = 1024 * 1024 * 64
)

type LCRChunk struct {
	lcr        oci.LCR
	nextOffset int64
	fileindex  int
}

var c chan LCRChunk

func ExecLCR(applyInfo *task.Apply_process_info, db *oci.OracleDb) error {

	ticker := time.NewTicker(time.Second)
	defer ticker.Stop()

	var prevTxid string
	var txBeginTm time.Time

	for !applyInfo.ShutDownFlags {

		select {
		case item := <-c:

			beginTm := time.Now()

			if prevTxid == "" {
				prevTxid = item.lcr.GetTxid()
				txBeginTm = time.Now()
				glog.V(log.LOG_LEVEL6).Info("begin transaction:", prevTxid)
			}

			glog.V(log.LOG_LEVEL10).Info(item.lcr.String())

			if prevTxid != item.lcr.GetTxid() {

				glog.V(log.LOG_LEVEL6).Infof("Rollback transaction prevTxid:%s curTxid:%s", prevTxid, item.lcr.GetTxid())
				if err := db.Rollback(); err != nil {
					glog.Error("rollback fails.", err)
					return err
				}

				prevTxid = item.lcr.GetTxid()
			}

			comPare, err := item.lcr.SCNCompare(db, applyInfo.CSN)
			if err != nil {
				return fmt.Errorf("SCNCompare fails. err:%v", err)
			}

			//filter := applyInfo.Match(item.lcr)
			filter := true

			if comPare > 0 && filter {
				if err := applyInfo.MatchAndMapped(item.lcr).ExecSQL(db); err != nil {
					glog.Errorf("ExecSQL fails. lcr:\n%verr:%v", item.lcr.String(), err)
					return err
				}
			} else {
				glog.Infof("lcr[%s] Filter drop comPare[%v] filter[%v]", item.lcr.String(), comPare, filter)
			}

			scn, err := item.lcr.GetSCN(db)
			if err != nil {
				return fmt.Errorf("GetSCN fails. err:%v", err)
			}

			if scn > applyInfo.CSN {
				applyInfo.CSN = scn
			}

			applyInfo.NextLogFileOffset = item.nextOffset
			applyInfo.CurLogFileIndex = item.fileindex

			glog.V(log.LOG_LEVEL8).Infof("CurLogFileIndex:%d NextLogFileOffset:%d time:%d us", applyInfo.CurLogFileIndex, applyInfo.NextLogFileOffset, (time.Now().UnixNano()-beginTm.UnixNano())/1000)

			if item.lcr.CmdType() == oci.OCI_LCR_ROW_CMD_COMMIT {
				prevTxid = ""
				if err := applyInfo.Serialize(); err != nil {
					return fmt.Errorf("applyInfo.Serialize fails. err:%v", err)
				}
				txNanoTm := time.Now().UnixNano() - txBeginTm.UnixNano()
				glog.V(log.LOG_LEVEL6).Infof("commit transaction:%s time:%d ms", item.lcr.GetTxid(), txNanoTm/1000/1000)
			}
		case <-ticker.C:
			glog.V(log.LOG_LEVEL7).Info("DING")

			if len(c) == 0 {
				glog.V(log.LOG_LEVEL7).Info("LCRChunk chan empty")
			}
		}
	}

	return nil
}

func UnSerializeLCRFromBuffer(applyInfo *task.Apply_process_info, fileIndex int, fileOffset int64, b []byte) (int, error) {

	var offset int
	r := bytes.NewBuffer(b)
	flags := make([]byte, len(oci.FLAGS_DDL_LCR))

	ticker := time.NewTicker(time.Second)
	defer ticker.Stop()

	for {

		var lcr oci.LCR = nil

		offset = len(b) - r.Len()

		if n, err := r.Read(flags); n != len(flags) {
			if err == io.EOF {
				err = nil
			}

			if err != nil {
				return offset, fmt.Errorf("read flags fails. offset:%d err:%v", offset, err)
			}

			return offset, nil
		}

		if bytes.Equal(flags, oci.FLAGS_ROW_LCR) {
			var obj oci.Row_lcr_t
			if err := obj.Unserialize(r); err == nil {
				lcr = &obj
			} else if err == io.ErrUnexpectedEOF {
				glog.V(log.LOG_LEVEL6).Infof("Row_lcr_t Unserialize fails. offset:%d err:%v", offset, err)
				return offset, nil
			} else if err == io.EOF {
				glog.V(log.LOG_LEVEL6).Infof("Row_lcr_t Unserialize fails. offset:%d err:%v", offset, err)
				return offset, nil
			} else if err == oci.NOT_SUPPORT_CMD_TYPE {
				glog.Warningf("Row_lcr_t Unserialize fails.LCR:%v offset:%d err:%v", lcr.String(), offset, err)
				continue
			} else {
				return offset, fmt.Errorf("Row_lcr_t Unserialize fails. offset:%d err:%v", offset, err)
			}
		} else if bytes.Equal(flags, oci.FLAGS_DDL_LCR) {
			var obj oci.Ddl_lcr_t
			if err := obj.Unserialize(r); err == nil {
				lcr = &obj
			} else if err == io.ErrUnexpectedEOF {
				glog.V(log.LOG_LEVEL6).Infof("Ddl_lcr_t Unserialize fails. offset:%d err:%v", offset, err)
				return offset, nil
			} else if err == io.EOF {
				glog.V(log.LOG_LEVEL6).Infof("Ddl_lcr_t Unserialize fails. offset:%d err:%v", offset, err)
				return offset, nil
			} else {
				return offset, fmt.Errorf("Ddl_lcr_t Unserialize fails. offset:%d err:%v", offset, err)
			}
		} else {
			glog.V(log.LOG_LEVEL6).Info("unknow flags ", hex.EncodeToString(flags), fileIndex, fileOffset, offset)
			return offset, nil
		}

		if lcr == nil {
			glog.Warning("WARNING!!!! LCR nil")
			continue
		}

	RETRY:
		select {
		case c <- LCRChunk{lcr, fileOffset + int64(len(b)-r.Len()), fileIndex}:
		case <-ticker.C:
			glog.V(log.LOG_LEVEL7).Info("DING")
			if applyInfo.ShutDownFlags {
				return offset, fmt.Errorf("applyInfo.ShutDown")
			}
			goto RETRY
		}
	}
}

func UnSerializeLCRFromFile(applyInfo *task.Apply_process_info, fileIndex int, fileOffset int64) error {

	fpath := task.DbLogFileName(applyInfo.LogFileNamePrefix, fileIndex)

	fd, err := os.Open(fpath)
	if err != nil {
		return fmt.Errorf("open file[%s] fails. err:%v", fpath, err)
	}

	defer fd.Close()

	if _, err := fd.Seek(fileOffset, os.SEEK_SET); err != nil {
		return fmt.Errorf("seek [%s] offset:%d fails. err:%v", fpath, fileOffset, err)
	}

	buf := make([]byte, READ_BUF_SIZE, READ_BUF_SIZE)

	for !applyInfo.ShutDownFlags {

		fileOffset, err := fd.Seek(0, os.SEEK_CUR)
		if err != nil {
			return fmt.Errorf("seek [%s] offset:%d fails. err:%v", fpath, 0, err)
		}

		chunksize, err := fd.Read(buf)

		glog.V(log.LOG_LEVEL8).Infof("read file[%s] offset:%d chunksize:%d err:%v", fpath, fileOffset, chunksize, err)

		if chunksize == 0 {
			if err == io.EOF || err == nil {
				glog.V(log.LOG_LEVEL6).Infof("read file[%s] EOF and sleep", fpath)
				time.Sleep(time.Second * 3)
				continue
			}

			if err != nil {
				return fmt.Errorf("read file[%s] fails. err:%v", fpath, err)
			}
		}

		nread, err := UnSerializeLCRFromBuffer(applyInfo, fileIndex, fileOffset, buf[:chunksize])
		if err != nil {
			return fmt.Errorf("UnSerializeLCRFromBuffer fails. file[%s] fileOffset:%d err:%v", fpath, fileOffset, err)
		}

		if nread == chunksize {
			continue
		}

		if bytes.Equal(buf[nread:chunksize], task.DB_LOG_TERMINAL_FLAGS) {
			glog.V(log.LOG_LEVEL6).Info("read file end")
			return nil
		}

		if nread == 0 {
			return fmt.Errorf("chunk buffer can't recognition. fileIndex:%d offset:%d HEX:%v", fileIndex, fileOffset, hex.EncodeToString(buf[nread:chunksize]))
		}

		if _, err := fd.Seek(int64(nread-chunksize), os.SEEK_CUR); err != nil {
			return fmt.Errorf("seek [%s] %d fails. err:%v", fpath, nread-chunksize, err)
		}

		glog.V(log.LOG_LEVEL6).Info("sleep for next file chunk")
		time.Sleep(time.Second * 3)
	}

	return nil
}

func Start(name string, printInfo bool, reset bool) (err error) {

	defer func() {
		if err != nil {
			glog.Error(name, err)
		}
	}()

	var wg sync.WaitGroup
	c = make(chan LCRChunk, 5000)

	var applyInfo task.Apply_process_info

	if err := applyInfo.UnserializeFromFile(path.Join("../metadata", name)); err != nil {
		return err
	}

	applyInfo.ShutDownFlags = false
	glog.Info("metadata:\n", applyInfo.String())

	if printInfo {
		fmt.Println(applyInfo.String())
		return nil
	}

	if reset {
		applyInfo.CurLogFileIndex = 0
		applyInfo.NextLogFileOffset = 0
		applyInfo.Serialize()
		fmt.Printf("%s\n", applyInfo.String())
		return nil
	}

	db, err := oci.InitOracleDB(applyInfo.DbAddr[0], applyInfo.DbUserName, applyInfo.DbUserPwd)
	if err != nil {
		return err
	}

	wg.Add(1)
	go func() {
		defer wg.Done()
		if err := ExecLCR(&applyInfo, db); err != nil {
			glog.Error("ExecLCR fails.", err)
			applyInfo.ShutDownFlags = true
			return
		}
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()

		fileIndex := applyInfo.CurLogFileIndex
		fileOffset := applyInfo.NextLogFileOffset

		for !applyInfo.ShutDownFlags {
			if err := UnSerializeLCRFromFile(&applyInfo, fileIndex, fileOffset); err != nil {
				glog.Errorf("UnSerializeLCRFromFile fails. err:%v", err)
				applyInfo.ShutDownFlags = true
				return
			}

			fileIndex++
			fileOffset = 0
		}
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		for !applyInfo.ShutDownFlags {
			applyInfo.SendMonitorMsg()
			time.Sleep(time.Second * 1)
		}

		if applyInfo.ShutDownFlags {
			glog.Info("Monitor task exit")
		}
	}()

	wg.Wait()

	glog.Info("task exit")

	return nil
}
