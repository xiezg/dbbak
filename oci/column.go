/*************************************************************************
# File Name: capture/column.go
# Author: xiezg
# Mail: xzghyd2008@hotmail.com
# Created Time: 2019-07-10 23:00:45
# Last modified: 2019-09-07 22:15:11
************************************************************************/
package oci

//#include <stdlib.h>
//#include <oci.h>
//#include <string.h>
//oratext * oratextpidx( oratext **p, int i ){return p[i];}
//ub2 ub2idx( ub2* p, int i ){return p[i];}
//ub1 ub1idx( ub1* p, int i ){return p[i];}
//oraub8 oraub8idx( oraub8* p, int i ){return p[i];}
//void* voidpidx( void**p, int i ){return p[i];}
//OCIInd ociind2idx( OCIInd* p, int i ){return p[i];}
import "C"
import "fmt"
import "regexp"
import "unsafe"
import "strings"
import "encoding/hex"
import "dbbak/charset"

type columns_t struct {
	Num_columns    int
	Column_type    int
	Column_names   []string
	Column_dtyp    []int
	Column_csetfp  []int
	Column_flags   []int64
	Column_csid    []int
	Column_indp    []int
	Column_valuesp [][]byte
	descriptor     []func()
}

func (obj *columns_t) Release() {

	for _, fn := range obj.descriptor {
		fn()
	}

	obj.descriptor = make([]func(), 0)
}

func (obj *columns_t) String() string {

	str := fmt.Sprintf("columns_t:Num_columns:%d type:%d\n", obj.Num_columns, obj.Column_type)
	for i := 0; i < obj.Num_columns; i++ {
		str = str + fmt.Sprintf("Column_names:%v", obj.Column_names[i])
		str = str + fmt.Sprintf(" Column_dtyp:%v", obj.Column_dtyp[i])
		str = str + fmt.Sprintf(" Column_csetfp:%v", obj.Column_csetfp[i])
		str = str + fmt.Sprintf(" Column_flags:%b", obj.Column_flags[i])
		str = str + fmt.Sprintf(" Column_indp:%v", obj.Column_indp[i])
		str = str + fmt.Sprintf(" Column_csid:%v", obj.Column_csid[i])
		str = str + fmt.Sprintf(" Column_valuesp size:%v 0x%v\n", len(obj.Column_valuesp[i]), hex.EncodeToString(obj.Column_valuesp[i]))
	}

	return str
}

func (obj *columns_t) bind_chunk(chunk *chunk_t) bool {

	for i := 0; i < obj.Num_columns; i++ {
		if obj.Column_names[i] == chunk.Column_names {

			if len(chunk.Column_valuesp) > 0 {
				obj.Column_valuesp[i] = append(obj.Column_valuesp[i], chunk.Column_valuesp...)
				obj.Column_indp[i] = OCI_IND_NOTNULL
				obj.Column_flags[i] = obj.Column_flags[i] | chunk.Column_flags
			}

			return true
		}
	}

	if obj.Column_type == OCI_LCR_ROW_COLVAL_NEW {
		obj.Num_columns = obj.Num_columns + 1
		obj.Column_names = append(obj.Column_names, chunk.Column_names)
		obj.Column_dtyp = append(obj.Column_dtyp, chunk.Column_dtyp)
		obj.Column_csetfp = append(obj.Column_csetfp, 0)
		obj.Column_flags = append(obj.Column_flags, chunk.Column_flags)
		obj.Column_csid = append(obj.Column_csid, chunk.Column_csid)

		if len(chunk.Column_valuesp) > 0 {
			obj.Column_indp = append(obj.Column_indp, OCI_IND_NOTNULL)
			obj.Column_valuesp = append(obj.Column_valuesp, chunk.Column_valuesp)
		}

		return true
	}

	return false
}

func (obj *columns_t) get_column_placeholder_name(i int) string {

	if i >= len(obj.Column_names) {
		//print warning message
		return ""
	}

	if i >= len(obj.Column_flags) {
		//print warning message
		return ""
	}

	if obj.Column_type == OCI_LCR_ROW_COLVAL_OLD {
		return ":OLD_" + obj.Column_names[i] + "_9527"
	}

	if obj.Column_type == OCI_LCR_ROW_COLVAL_NEW {
		return ":NEW_" + obj.Column_names[i] + "_9527"
	}

	return ""
}

func (obj *columns_t) bind_sqlt_timestamp(db *OracleDb, name string, dty int, b []byte) error {

	datetime, destory, err := db.LCRDateTimeToOCIDateTime(b, dty)
	if err != nil {
		return err
	}

	obj.descriptor = append(obj.descriptor, destory)

	return db.OCIBindByName2(name, unsafe.Pointer(&datetime), 8, dty)
}

func (obj *columns_t) bind_sqlt_interval(db *OracleDb, name string, dty int, b []byte) error {

	interval, destory, err := db.LCRIntervalToOCIInterval(b, dty)
	if err != nil {
		return err
	}

	obj.descriptor = append(obj.descriptor, destory)

	return db.OCIBindByName2(name, unsafe.Pointer(&interval), 8, dty)
}

func (obj *columns_t) bind_sqlt_clob(db *OracleDb, name string, b []byte) error {

	inarray := C.CBytes(b)

	obj.descriptor = append(obj.descriptor, func() {
		C.free(unsafe.Pointer(inarray))
	})

	return db.OCIBindByName2(name, inarray, len(b), SQLT_CLOB)
}

func (obj *columns_t) bind_sqlt_charset_utf16(db *OracleDb, name string, b []byte, flags int64, dty int) error {

	b, err := charset.UTF16BigEndianToGBK(b)
	if err != nil {
		return err
	}

	inarray := C.CBytes(b)

	obj.descriptor = append(obj.descriptor, func() {
		C.free(unsafe.Pointer(inarray))
	})

	return db.OCIBindByName2(name, inarray, len(b), dty)
}

func (obj *columns_t) bind_sqlt_char(db *OracleDb, name string, b []byte, flags int64) error {

	if flags&OCI_LCR_COLUMN_AL16UTF16 == OCI_LCR_COLUMN_AL16UTF16 {
		return obj.bind_sqlt_charset_utf16(db, name, b, flags, SQLT_CHR)
	}

	inarray := C.CBytes(b)

	obj.descriptor = append(obj.descriptor, func() {
		C.free(unsafe.Pointer(inarray))
	})

	return db.OCIBindByName2(name, inarray, len(b), SQLT_CHR)
}

func (obj *columns_t) bind_sqlt_bin(db *OracleDb, name string, b []byte, flags int64) error {

	if flags&OCI_LCR_COLUMN_AL16UTF16 == OCI_LCR_COLUMN_AL16UTF16 {
		return obj.bind_sqlt_charset_utf16(db, name, b, flags, SQLT_LNG)
	}

	inarray := C.CBytes(b)

	obj.descriptor = append(obj.descriptor, func() {
		C.free(unsafe.Pointer(inarray))
	})

	return db.OCIBindByName2(name, inarray, len(b), SQLT_BIN)
}

func (obj *columns_t) bind_sqlt_other(db *OracleDb, name string, dty int, b []byte, flags int64) (err error) {

	inarray := C.CBytes(b)

	obj.descriptor = append(obj.descriptor, func() {
		C.free(unsafe.Pointer(inarray))
	})

	return db.OCIBindByName2(name, inarray, len(b), dty)
}

func (obj *columns_t) bind_data(db *OracleDb) (err error) {

	for i := 0; i < obj.Num_columns; i++ {

		if obj.Column_indp[i] == OCI_IND_NULL {
			continue
		}

		colname := obj.get_column_placeholder_name(i)

		switch obj.Column_dtyp[i] {
		case SQLT_TIMESTAMP, SQLT_TIMESTAMP_TZ, SQLT_TIMESTAMP_LTZ:
			err = obj.bind_sqlt_timestamp(db, colname, obj.Column_dtyp[i], obj.Column_valuesp[i])
		case SQLT_INTERVAL_YM, SQLT_INTERVAL_DS:
			err = obj.bind_sqlt_interval(db, colname, obj.Column_dtyp[i], obj.Column_valuesp[i])
		case SQLT_CHR:
			err = obj.bind_sqlt_char(db, colname, obj.Column_valuesp[i], obj.Column_flags[i])
		case SQLT_BIN:
			err = obj.bind_sqlt_bin(db, colname, obj.Column_valuesp[i], obj.Column_flags[i])
		default:
			err = obj.bind_sqlt_other(db, colname, obj.Column_dtyp[i], obj.Column_valuesp[i], obj.Column_flags[i])
		}

		if err != nil {
			return err
		}
	}

	return nil
}

func (obj *columns_t) get_insert_val_express() string {

	if obj.Num_columns == 0 {
		return ""
	}

	if obj.Num_columns == 1 {

		if obj.Column_indp[0] == OCI_IND_NULL {
			return "null"
		}
		return obj.get_column_placeholder_name(0)
	}

	columns := obj.get_column_placeholder_name(0)
	if obj.Column_indp[0] == OCI_IND_NULL {
		columns = "null"
	}

	for i := 1; i < obj.Num_columns; i++ {

		columns = columns + ","

		if obj.Column_indp[i] == OCI_IND_NULL {
			columns = columns + "null"
		} else {
			columns = columns + obj.get_column_placeholder_name(i)
		}
	}

	return columns
}

func (obj *columns_t) get_where_express() string {

	if obj.Num_columns == 0 {
		return ""
	}

	if obj.Num_columns == 1 {

		if obj.Column_indp[0] == OCI_IND_NULL {
			return "\"" + obj.Column_names[0] + "\"" + " is null"
		}

		return "\"" + obj.Column_names[0] + "\"" + "=" + obj.get_column_placeholder_name(0)
	}

	express := "\"" + obj.Column_names[0] + "\"" + "=" + obj.get_column_placeholder_name(0)
	if obj.Column_indp[0] == OCI_IND_NULL {
		express = "\"" + obj.Column_names[0] + "\"" + " is null"
	}

	for i := 1; i < len(obj.Column_names); i++ {
		express = express + " AND "

		if obj.Column_indp[i] == OCI_IND_NULL {
			express = express + "\"" + obj.Column_names[i] + "\"" + "is null"
		} else {
			express = express + "\"" + obj.Column_names[i] + "\"" + "=" + obj.get_column_placeholder_name(i)
		}
	}

	return express
}

func (obj *columns_t) get_update_express() string {

	if obj.Num_columns == 0 {
		return ""
	}

	if obj.Num_columns == 1 {

		if obj.Column_indp[0] == OCI_IND_NULL {
			return "\"" + obj.Column_names[0] + "\"" + " = null"
		}

		return "\"" + obj.Column_names[0] + "\"" + "=" + obj.get_column_placeholder_name(0)
	}

	express := "\"" + obj.Column_names[0] + "\"" + "=" + obj.get_column_placeholder_name(0)
	if obj.Column_indp[0] == OCI_IND_NULL {
		express = "\"" + obj.Column_names[0] + "\"" + " = null"
	}

	for i := 1; i < len(obj.Column_names); i++ {
		express = express + ","

		if obj.Column_indp[i] == OCI_IND_NULL {
			express = express + "\"" + obj.Column_names[i] + "\"" + "= null"
		} else {
			express = express + "\"" + obj.Column_names[i] + "\"" + "=" + obj.get_column_placeholder_name(i)
		}
	}

	return express
}

func (obj *columns_t) get_insert_key_express() string {

	if obj.Num_columns == 0 {
		return ""
	}

	if obj.Num_columns == 1 {
		return "\"" + obj.Column_names[0] + "\""
	}

	columns := "\"" + obj.Column_names[0] + "\""

	for i := 1; i < obj.Num_columns; i++ {
		columns = columns + ","
		columns = columns + "\"" + obj.Column_names[i] + "\""
	}

	return columns
}

func parseSQLTIMESTAMP(str string) (string, string, string) {

	index := strings.IndexByte(str, '"')
	if index == -1 {
		return "", "", ""
	}

	str = str[index+1:]
	index = strings.IndexByte(str, '"')
	var name string = str[:index]
	str = str[index+1:]

	index = strings.IndexByte(str, '\'')
	str = str[index+1:]
	index = strings.IndexByte(str, '\'')
	var timetx string = strings.TrimSpace(str[:index])
	str = str[index+1:]

	index = strings.IndexByte(str, '\'')
	str = str[index+1:]
	index = strings.IndexByte(str, '\'')
	var timeval string = strings.TrimSpace(str[:index])
	str = str[index+1:]

	return name, timetx, timeval
}

func (obj *columns_t) parse_timestamp(db *OutBound, lcr unsafe.Pointer, column_value_type int) (map[string]string, error) {

	result := make(map[string]string)
	re := regexp.MustCompile(`"[^"]+"\s*=\s*TO_TIMESTAMP\(.+?\)`)

	sql_stmt, err := db.OCILCRWhereClauseGet(lcr)
	if err != nil {
		return nil, err
	}

	for _, item := range re.FindAllString(sql_stmt, -1) {

		name, val, ft := parseSQLTIMESTAMP(item)
		result[name] = val + "=" + ft
	}

	sql_stmt, err = db.OCILCRRowStmtGet(lcr)
	for _, item := range re.FindAllString(sql_stmt, -1) {

		name, val, ft := parseSQLTIMESTAMP(item)
		if column_value_type == OCI_LCR_ROW_COLVAL_OLD {
			result[name] = val + "=" + ft
			continue
		}

		if _, ok := result[name]; !ok {
			result[name] = val + "=" + ft
		}
	}

	return result, nil
}

func (obj *columns_t) parse_row_column_info(db *OutBound, lcr unsafe.Pointer, column_value_type int) error {

	var num_columns C.ub2
	var column_names **C.oratext = (**C.oratext)(C.calloc(8, MAX_COLUMNS))
	var column_name_lens *C.ub2 = (*C.ub2)(C.calloc(C.sizeof_ub2, MAX_COLUMNS))
	var column_dtyp *C.ub2 = (*C.ub2)(C.calloc(C.sizeof_ub2, MAX_COLUMNS))
	var column_valuesp **C.void = (**C.void)(C.calloc(8, MAX_COLUMNS))
	var column_alensp *C.ub2 = (*C.ub2)(C.calloc(C.sizeof_ub2, MAX_COLUMNS))
	var column_indp *C.OCIInd = (*C.OCIInd)(C.calloc(C.sizeof_OCIInd, MAX_COLUMNS))
	var column_csetfp *C.ub1 = (*C.ub1)(C.calloc(C.sizeof_ub1, MAX_COLUMNS))
	var column_flags *C.oraub8 = (*C.oraub8)(C.calloc(C.sizeof_oraub8, MAX_COLUMNS))
	var column_csid *C.ub2 = (*C.ub2)(C.calloc(C.sizeof_ub2, MAX_COLUMNS))

	defer func() {
		C.free(unsafe.Pointer(column_names))
		C.free(unsafe.Pointer(column_name_lens))
		C.free(unsafe.Pointer(column_dtyp))
		C.free(unsafe.Pointer(column_valuesp))
		C.free(unsafe.Pointer(column_indp))
		C.free(unsafe.Pointer(column_alensp))
		C.free(unsafe.Pointer(column_csetfp))
		C.free(unsafe.Pointer(column_flags))
		C.free(unsafe.Pointer(column_csid))
	}()

	retval := C.OCILCRRowColumnInfoGet(
		db.svchp,
		db.errhp,
		(C.ub2)(column_value_type),
		&num_columns,
		column_names,
		column_name_lens,
		column_dtyp,
		(*unsafe.Pointer)(unsafe.Pointer(column_valuesp)),
		column_indp,
		column_alensp,
		column_csetfp,
		column_flags,
		column_csid,
		lcr,
		MAX_COLUMNS,
		OCI_DEFAULT)

	if retval != OCI_SUCCESS {
		errnum, errmsg := db.ErrMsg()
		return fmt.Errorf("OCILCRDDLInfoGet fails retval:%d errnum:%d errmsg:%s", retval, errnum, errmsg)
	}

	timestamp, err := obj.parse_timestamp(db, lcr, column_value_type)
	if err != nil {
		return err
	}

	obj.Column_type = column_value_type
	obj.Num_columns = int(num_columns)

	for i := 0; i < obj.Num_columns; i++ {
		obj.Column_names = append(obj.Column_names, C.GoStringN((*C.char)(unsafe.Pointer(C.oratextpidx(column_names, C.int(i)))), (C.int)(C.ub2idx(column_name_lens, C.int(i)))))
		obj.Column_valuesp = append(obj.Column_valuesp, C.GoBytes(unsafe.Pointer(C.voidpidx((*unsafe.Pointer)(unsafe.Pointer(column_valuesp)), C.int(i))), (C.int)(C.ub2idx(column_alensp, C.int(i)))))
		obj.Column_dtyp = append(obj.Column_dtyp, int(C.ub2idx(column_dtyp, C.int(i))))
		obj.Column_csetfp = append(obj.Column_csetfp, int(C.ub1idx(column_csetfp, C.int(i))))
		obj.Column_flags = append(obj.Column_flags, int64(C.oraub8idx(column_flags, C.int(i))))
		obj.Column_csid = append(obj.Column_csid, int(C.ub2idx(column_csid, C.int(i))))
		obj.Column_indp = append(obj.Column_indp, int(C.ociind2idx(column_indp, C.int(i))))

		//case SQLT_TIMESTAMP, SQLT_TIMESTAMP_TZ, SQLT_TIMESTAMP_LTZ:
		if obj.Column_dtyp[i] == SQLT_TIMESTAMP || obj.Column_dtyp[i] == SQLT_TIMESTAMP_TZ || obj.Column_dtyp[i] == SQLT_TIMESTAMP_LTZ {
			if val, ok := timestamp[obj.Column_names[i]]; ok {
				obj.Column_valuesp[i] = []byte(val)
			}
		}
	}

	return nil
}
