/*************************************************************************
# File Name: capture/ddl_lcr.go
# Author: xiezg
# Mail: xzghyd2008@hotmail.com
# Created Time: 2019-07-07 17:49:15
# Last modified: 2019-09-19 23:41:21
************************************************************************/
package oci

//#include <stdlib.h>
//#include <oci.h>
import "C"
import "io"
import "fmt"
import "unsafe"
import "strings"
import "encoding/gob"
import "dbbak/Implementation/ddlcheck"

var FLAGS_DDL_LCR = []byte{0x6C, 0xB2, 0xDE, 0xA6}

type Ddl_lcr_t struct {
	Common_lcr_t
	Object_type      string //CLUSTER/FUNCTION/INDEX/OUTLINE/PACKAGE/PACKAGE BODY/PROCEDURE/SEQUENCE/SYNONYM/TABLE/TRIGGER/TYPE/USER/VIEW
	Ddl_text         string
	Logon_user       string
	Current_schema   string
	Base_table_name  string
	Base_table_owner string
	Flag             int
}

func (obj *Ddl_lcr_t) ExecFilter(filter *ddlcheck.DDlFiter) bool {

	if filter == nil {
		return false
	}

	return filter.Match(obj.Owner, obj.Object_type, obj.Oname, obj.Cmd_type)
}

func (obj *Ddl_lcr_t) Serialize(w io.Writer) error {
	if _, err := w.Write(FLAGS_DDL_LCR); err != nil {
		return err
	}

	return gob.NewEncoder(w).Encode(obj)
}

func (obj *Ddl_lcr_t) Unserialize(r io.Reader) error {
	return gob.NewDecoder(r).Decode(obj)
}

func (obj *Ddl_lcr_t) String() string {
	str := obj.Common_lcr_t.String()
	str = str + fmt.Sprintln("object_type:", obj.Object_type)
	str = str + fmt.Sprintln("ddl_text:", obj.Ddl_text)
	str = str + fmt.Sprintln("logon_user:", obj.Logon_user)
	str = str + fmt.Sprintln("current_schema:", obj.Current_schema)
	str = str + fmt.Sprintln("base_table_name:", obj.Base_table_name)
	str = str + fmt.Sprintln("base_table_owner:", obj.Base_table_owner)
	str = str + fmt.Sprintln("flag:", obj.Flag)

	return str
}

func (obj *Ddl_lcr_t) SetNewOwner(str string) {
	obj.Ddl_text = strings.Replace(obj.Ddl_text, obj.Owner, str, 1)
	obj.Owner = str
}

func (obj *Ddl_lcr_t) SetNewOname(str string) {
	obj.Ddl_text = strings.Replace(obj.Ddl_text, obj.Oname, str, 1)
	obj.Oname = str
}

func (obj *Ddl_lcr_t) prepare() {

	if obj.Cmd_type == "DROP TABLE" {
		obj.Ddl_text = fmt.Sprintf("DROP TABLE \"%s\".\"%s\"", obj.Owner, obj.Base_table_name)
	}
}

func (obj *Ddl_lcr_t) ExecSQL(db *OracleDb) error {

	obj.prepare()

	if obj.Cmd_type == "CREATE DATABASE LINK" {
		return nil
	}

	if err := db.OCIStmtPrepare2(obj.Ddl_text); err != nil {
		return err
	}

	defer func() {
		db.OCIStmtRelease()
	}()

	return db.OCIStmtExecute()
}

func (obj *Ddl_lcr_t) Parse_lcr(db *OutBound, lcr unsafe.Pointer) error {

	if err := obj.Common_lcr_t.get_lcr_header(db, lcr); err != nil {
		return err
	}

	var object_type *C.oratext
	var object_type_len C.ub2
	var ddl_text *C.oratext
	var ddl_text_len C.ub4
	var logon_user *C.oratext
	var logon_user_len C.ub2
	var current_schema *C.oratext
	var current_schema_len C.ub2
	var base_table_owner *C.oratext
	var base_table_owner_len C.ub2
	var base_table_name *C.oratext
	var base_table_name_len C.ub2
	var flag C.oraub8

	retval := C.OCILCRDDLInfoGet(
		db.svchp,
		db.errhp,
		&object_type,
		&object_type_len,
		&ddl_text,
		&ddl_text_len,
		&logon_user,
		&logon_user_len,
		&current_schema,
		&current_schema_len,
		&base_table_owner,
		&base_table_owner_len,
		&base_table_name,
		&base_table_name_len,
		&flag,
		lcr,
		OCI_DEFAULT)

	if retval != OCI_SUCCESS {
		errnum, errmsg := db.ErrMsg()
		return fmt.Errorf("OCILCRDDLInfoGet fails retval:%d errnum:%d errmsg:%s", retval, errnum, errmsg)
	}

	obj.Object_type = C.GoStringN((*C.char)(unsafe.Pointer(object_type)), C.int(object_type_len))
	obj.Ddl_text = C.GoStringN((*C.char)(unsafe.Pointer(ddl_text)), C.int(ddl_text_len))
	obj.Logon_user = C.GoStringN((*C.char)(unsafe.Pointer(logon_user)), C.int(logon_user_len))
	obj.Current_schema = C.GoStringN((*C.char)(unsafe.Pointer(current_schema)), C.int(current_schema_len))
	obj.Base_table_name = C.GoStringN((*C.char)(unsafe.Pointer(base_table_name)), C.int(base_table_name_len))
	obj.Base_table_owner = C.GoStringN((*C.char)(unsafe.Pointer(base_table_owner)), C.int(base_table_owner_len))
	obj.Flag = int(flag)

	return nil
}
