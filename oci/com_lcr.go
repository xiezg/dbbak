/*************************************************************************
# File Name: capture/com_lcr.go
# Author: xiezg
# Mail: xzghyd2008@hotmail.com
# Created Time: 2019-07-08 10:32:39
# Last modified: 2019-09-19 22:17:04
************************************************************************/
package oci

//#include <oci.h>
//#include <stdlib.h>
import "C"
import "io"
import "fmt"
import "unsafe"
import "encoding/hex"

//import "dbbak/Implementation/ddlcheck"

type LCR interface {
	String() string
	GetPosition() []byte
	Serialize(io.Writer) error
	ExecSQL(db *OracleDb) error
	CmdType() string
	GetTxid() string
	GetSCN(*OracleDb) (int, error)
	SCNCompare(*OracleDb, int) (int, error)
	GetOwner() string
	GetOname() string
	SetNewOwner(string)
	SetNewOname(string)
}

type Common_lcr_t struct {
	Src_db_name string //Canonicalized source database name
	Cmd_type    string //DML:OCI_LCR_ROW_CMD_INSERT/DELETE DDL:CREATE TABLE/DROP TABLE/ALTER TABLE
	Owner       string //Canonicalized table owner name
	Oname       string //Canonicalized table name
	Txid        string
	Position    []byte
	New_columns int
	Old_columns int
}

func (obj *Common_lcr_t) SetNewOwner(str string) {
	obj.Owner = str
}

func (obj *Common_lcr_t) SetNewOname(str string) {
	obj.Oname = str
}

func (obj *Common_lcr_t) GetOwner() string {
	return obj.Owner
}

func (obj *Common_lcr_t) GetOname() string {
	return obj.Oname
}

func (obj *Common_lcr_t) GetSCN(db *OracleDb) (int, error) {
	return db.GetSCNFromPosition(obj.Position)
}

func (obj *Common_lcr_t) SCNCompare(db *OracleDb, scn int) (int, error) {

	curSCN, err := db.GetSCNFromPosition(obj.Position)
	if err != nil {
		return -1, err
	}

	return db.OCILCRSCNCompare(C.int64_t(curSCN), C.int64_t(scn))
}

func (obj *Common_lcr_t) String() string {

	str := fmt.Sprintf("\nsrc_db_name:%s\n", obj.Src_db_name)
	str = str + fmt.Sprintf("cmd_type:%s\n", obj.Cmd_type)
	str = str + fmt.Sprintf("owner:%s\n", obj.Owner)
	str = str + fmt.Sprintf("oname:%s\n", obj.Oname)
	str = str + fmt.Sprintf("txid:%s\n", obj.Txid)
	str = str + fmt.Sprintf("new_columns:%d\n", obj.New_columns)
	str = str + fmt.Sprintf("old_columns:%d\n", obj.Old_columns)
	str = str + fmt.Sprintf("position:%s\n", hex.EncodeToString(obj.Position))

	return str
}

func (obj *Common_lcr_t) GetTxid() string {
	return obj.Txid
}

func (obj *Common_lcr_t) CmdType() string {
	return obj.Cmd_type
}

func (obj *Common_lcr_t) commit(db *OracleDb) error {

	if retval := C.OCITransCommit(db.svchp, db.errhp, OCI_SUCCESS); retval != OCI_SUCCESS {
		errnum, errmsg := db.ErrMsg()
		return fmt.Errorf("OCITransCommit fails. retval:%d errnum:%d errmsg:%s", retval, errnum, errmsg)
	}

	return nil
}

func (obj *Common_lcr_t) GetPosition() []byte {
	return obj.Position
}

func (obj *Common_lcr_t) get_lcr_header(db *OutBound, lcr unsafe.Pointer) error {

	var src_db_name, cmd_type, owner, oname, txid *C.oratext
	var src_db_name_len, cmd_type_len, owner_len, oname_len, txid_len C.ub2

	var position *C.ub1
	var position_len, new_columns, old_columns C.ub2

	retval := C.OCILCRHeaderGet(
		db.svchp,
		db.errhp,
		&src_db_name,
		&src_db_name_len,
		&cmd_type,
		&cmd_type_len,
		&owner,
		&owner_len,
		&oname,
		&oname_len,
		nil,
		nil,
		&txid,
		&txid_len,
		nil,
		&old_columns,
		&new_columns,
		&position,
		&position_len,
		nil,
		lcr,
		OCI_DEFAULT)

	if retval != OCI_SUCCESS {
		errnum, errmsg := db.ErrMsg()
		return fmt.Errorf("OCILCRHeaderGet fails retval:%d errnum:%d errmsg:%s", retval, errnum, errmsg)
	}

	obj.Src_db_name = C.GoStringN((*C.char)(unsafe.Pointer(src_db_name)), C.int(src_db_name_len))
	obj.Cmd_type = C.GoStringN((*C.char)(unsafe.Pointer(cmd_type)), C.int(cmd_type_len))
	obj.Owner = C.GoStringN((*C.char)(unsafe.Pointer(owner)), C.int(owner_len))
	obj.Oname = C.GoStringN((*C.char)(unsafe.Pointer(oname)), C.int(oname_len))
	obj.Txid = C.GoStringN((*C.char)(unsafe.Pointer(txid)), C.int(txid_len))
	obj.Position = C.GoBytes(unsafe.Pointer(position), C.int(position_len))
	obj.New_columns = int(new_columns)
	obj.Old_columns = int(old_columns)

	return nil
}
