/*************************************************************************
# File Name: capture/chunk.go
# Author: xiezg
# Mail: xzghyd2008@hotmail.com
# Created Time: 2019-07-10 15:59:24
# Last modified: 2019-09-08 11:05:16
************************************************************************/
package oci

//#cgo CFLAGS: -ggdb -I/usr/include/oracle/18.3/client64/
//#include <oci.h>
//#include <stdio.h>
//#include <stdlib.h>
//#include <string.h>
//#include <malloc.h>
//#include <ctype.h>
import "C"
import "fmt"
import "unsafe"
import "encoding/hex"

type chunk_t struct {
	Column_names   string
	Column_dtyp    int
	Column_flags   int64
	Column_csid    int
	Column_valuesp []byte
}

func (obj *chunk_t) String() string {

	str := fmt.Sprintf("chunk_t:Column_names:%v", obj.Column_names)
	str = str + fmt.Sprintf(" Column_dtyp:%v", obj.Column_dtyp)
	str = str + fmt.Sprintf(" Column_flags:%b", obj.Column_flags)
	str = str + fmt.Sprintf(" Column_csid:%v", obj.Column_csid)
	str = str + fmt.Sprintf(" Column_valuesp size:%v 0x%s\n", len(obj.Column_valuesp), hex.EncodeToString(obj.Column_valuesp))

	return str
}

func (obj *chunk_t) get_data(db *OutBound) (bool, error) {

	var column_name *C.oratext
	var column_name_len C.ub2
	var column_dty C.ub2
	var column_flag C.oraub8
	var column_csid C.ub2
	var chunk_bytes C.ub4
	var chunk_dat *C.ub1
	var flag C.oraub8

	retval := C.OCIXStreamOutChunkReceive(
		db.svchp,
		db.errhp,
		&column_name,
		&column_name_len,
		&column_dty,
		&column_flag,
		&column_csid,
		&chunk_bytes,
		&chunk_dat,
		&flag,
		OCI_DEFAULT)

	if retval != OCI_SUCCESS {
		errnum, errmsg := db.ErrMsg()
		return false, fmt.Errorf("OCIXStreamOutChunkReceive fails retval:%d errnum:%d errmsg:%s", retval, errnum, errmsg)
	}

	obj.Column_names = C.GoStringN((*C.char)(unsafe.Pointer((column_name))), (C.int)(column_name_len))
	obj.Column_dtyp = int(column_dty)
	obj.Column_flags = int64(column_flag)
	obj.Column_csid = int(column_csid)
	obj.Column_valuesp = C.GoBytes(unsafe.Pointer(chunk_dat), C.int(chunk_bytes))

	return flag&OCI_XSTREAM_MORE_ROW_DATA == OCI_XSTREAM_MORE_ROW_DATA, nil
}
