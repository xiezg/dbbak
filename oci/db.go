/*************************************************************************
# File Name: oci/db.go
# Author: xiezg
# Mail: xzghyd2008@hotmail.com
# Created Time: 2019-07-18 12:56:01
# Last modified: 2019-09-16 17:42:16
************************************************************************/
package oci

//#cgo CFLAGS: -ggdb -I/usr/include/oracle/18.3/client64/
//#cgo LDFLAGS:  -L /usr/lib/oracle/18.3/client64/lib/ -locci -lociei -Wl,-rpath=../lib/oracle/18.3/client64/lib/
//#include <oci.h>
//#include <stdio.h>
//#include <stdlib.h>
//#include <string.h>
//#include <malloc.h>
//#include <ctype.h>
//#include <stdint.h>
//#include <errno.h>
//long CGOOCISCNPositionToInt( OCISvcCtx *svchp, OCIError *errhp, char *position, ub2 position_len ){
//
//    ub4 result = 0;
//    sword retval = 0;
//    OCINumber scn, commit_scn;
//
//    errno = 0;
//
//    if( OCILCRSCNsFromPosition( svchp, errhp, (ub1*)position, position_len, &scn, &commit_scn, OCI_DEFAULT ) != OCI_SUCCESS ){
//        errno = -1;
//        return -1;
//    }
//
//    if( OCINumberToInt( errhp, &scn, sizeof(result), OCI_NUMBER_UNSIGNED, &result ) != OCI_SUCCESS ){
//        errno = -1;
//        return -1;
//    }
//
//    return result;
//}
import "C"
import "fmt"
import "net"
import "time"
import "unsafe"
import "strings"
import "dbbak/log"
import "github.com/golang/glog"

const (
	DEFAULT_FS_PREC = 6
	DML_SIZE        = 40000
)

const (
	TS_FORMAT   = "DD-MON-YYYY HH24:MI:SS.FF"
	TSZ_FORMAT  = "DD-MON-YYYY HH24:MI:SS.FF TZH:TZM"
	DATE_FORMAT = "Month dd, YYYY, HH:MI A.M."
)

type OracleDb struct {
	envhp    *C.OCIEnv
	errhp    *C.OCIError
	srvhp    *C.OCIServer
	svchp    *C.OCISvcCtx
	authp    *C.OCISession
	stmthp   *C.OCIStmt
	dbname   string
	uname    string
	password string
	txid     string
}

func OracleDBConnTest(dbname, uname, password string) error {

	var addr string = dbname
	if index := strings.LastIndexByte(dbname, '/'); index > 0 {
		addr = dbname[:index]
	}

	conn, err := net.DialTimeout("tcp", addr, time.Second*3)
	if err != nil {
		return err
	}

	conn.Close()

	db, err := InitOracleDB(dbname, uname, password)
	if err != nil {
		return err
	}

	db.disconnect_server()

	return nil
}

func InitOracleDB(dbname, uname, password string) (*OracleDb, error) {

	var db OracleDb

	if err := db.init_env_handle(); err != nil {
		return nil, err
	}

	db.dbname = dbname
	db.uname = uname
	db.password = password

	if err := db.connect_server(); err != nil {
		return nil, err
	}

	return &db, nil
}

func CreateOutBound(dbname, uname, password, taskname string) error {

	sql := `declare
  tables  dbms_utility.uncl_array;
  schemas dbms_utility.uncl_array;
begin
    tables(1)  := null;
    schemas(1) := null;
  dbms_xstream_adm.create_outbound(
    server_name     =>  '` + taskname + `',
    table_names     =>  tables,
    schema_names    =>  schemas);
end;`

	db, err := InitOracleDB(dbname, uname, password)
	if err != nil {
		return err
	}

	defer db.disconnect_server()

	if err := db.OCIStmtPrepare2(sql); err != nil {
		return err
	}

	return db.OCIStmtExecute()
}

func DropOutBound(dbname, uname, password, taskname string) error {

	sql := `begin
  dbms_xstream_adm.drop_outbound('` + taskname + `');
end;`

	db, err := InitOracleDB(dbname, uname, password)
	if err != nil {
		return err
	}

	defer db.disconnect_server()

	if err := db.OCIStmtPrepare2(sql); err != nil {
		return err
	}

	return db.OCIStmtExecute()
}

func (obj *OracleDb) Rollback() error {

	if retval := C.OCITransRollback(obj.svchp, obj.errhp, OCI_SUCCESS); retval != OCI_SUCCESS {
		errnum, errmsg := obj.ErrMsg()
		return fmt.Errorf("OCITransCommit fails. retval:%d errnum:%d errmsg:%s", retval, errnum, errmsg)
	}

	return nil
}

func (obj *OracleDb) connect_server() (err error) {

	defer func() {
		if err != nil {
			errnum, errmsg := obj.ErrMsg()
			err = fmt.Errorf("%s errnum:%d errmsg:%s", err.Error(), errnum, errmsg)
		}
	}()

	cs_dbname := (*C.uchar)(unsafe.Pointer(C.CString(obj.dbname)))
	defer C.free(unsafe.Pointer(cs_dbname))

	cs_uname := C.CString(obj.uname)
	defer C.free(unsafe.Pointer(cs_uname))

	cs_password := C.CString(obj.password)
	defer C.free(unsafe.Pointer(cs_password))

	if retval := C.OCIServerAttach(obj.srvhp, obj.errhp, cs_dbname, C.int(len(obj.dbname)), OCI_DEFAULT); retval != OCI_SUCCESS {
		return fmt.Errorf("OCIServerAttach dbname[%s] fails result:%d", obj.dbname, retval)
	}

	if retval := C.OCIAttrSet(unsafe.Pointer(obj.svchp), OCI_HTYPE_SVCCTX, unsafe.Pointer(obj.srvhp), 0, OCI_ATTR_SERVER, obj.errhp); retval != OCI_SUCCESS {
		return fmt.Errorf("OCIAttrSet OCISvcCtx with OCIServer fails result:%d", retval)
	}

	if retval := C.OCIAttrSet(unsafe.Pointer(obj.authp), OCI_HTYPE_SESSION, unsafe.Pointer(cs_uname), C.uint(len(obj.uname)), OCI_ATTR_USERNAME, obj.errhp); retval != OCI_SUCCESS {
		return fmt.Errorf("OCIAttrSet set username[%s] fails result:%d", obj.uname, retval)
	}

	if retval := C.OCIAttrSet(unsafe.Pointer(obj.authp), OCI_HTYPE_SESSION, unsafe.Pointer(cs_password), C.uint(len(obj.password)), OCI_ATTR_PASSWORD, obj.errhp); retval != OCI_SUCCESS {
		return fmt.Errorf("OCIAttrSet set password[%s] fails result:%d", obj.password, retval)
	}

	if retval := C.OCISessionBegin(obj.svchp, obj.errhp, obj.authp, OCI_CRED_RDBMS, OCI_DEFAULT); retval != OCI_SUCCESS {
		return fmt.Errorf("OCISessionBegin fails retval:%d", retval)
	}

	if retval := C.OCIAttrSet(unsafe.Pointer(obj.svchp), OCI_HTYPE_SVCCTX, unsafe.Pointer(obj.authp), 0, OCI_ATTR_SESSION, obj.errhp); retval != OCI_SUCCESS {
		return fmt.Errorf("OCIAttrSet OCISvcCtx with OCISession fails retval:%d", retval)
	}

	if retval := C.OCIPing(obj.svchp, obj.errhp, OCI_DEFAULT); retval != OCI_SUCCESS {
		return fmt.Errorf("OCIPing fails retval:%d", retval)
	}

	return nil
}

func (obj *OracleDb) disconnect_server() {

	if obj.authp != nil {
		C.OCISessionEnd(obj.svchp, obj.errhp, obj.authp, OCI_DEFAULT)
	}

	if obj.srvhp != nil {
		C.OCIServerDetach(obj.srvhp, obj.errhp, OCI_DEFAULT)
	}

	if obj.stmthp != nil {
		C.OCIHandleFree(unsafe.Pointer(obj.stmthp), OCI_HTYPE_STMT)
		obj.stmthp = nil
	}

	if obj.authp != nil {
		C.OCIHandleFree(unsafe.Pointer(obj.authp), OCI_HTYPE_SESSION)
		obj.authp = nil
	}

	if obj.svchp != nil {
		C.OCIHandleFree(unsafe.Pointer(obj.svchp), OCI_HTYPE_SVCCTX)
		obj.svchp = nil
	}

	if obj.srvhp != nil {
		C.OCIHandleFree(unsafe.Pointer(obj.srvhp), OCI_HTYPE_SERVER)
		obj.srvhp = nil
	}

	if obj.errhp != nil {
		C.OCIHandleFree(unsafe.Pointer(obj.errhp), OCI_HTYPE_ERROR)
		obj.errhp = nil
	}

	if obj.envhp != nil {
		C.OCIHandleFree(unsafe.Pointer(obj.envhp), OCI_HTYPE_ENV)
		obj.envhp = nil
	}
}

func (obj *OracleDb) init_env_handle() (err error) {

	defer func() {
		if err != nil {
			errnum, errmsg := obj.ErrMsg()
			err = fmt.Errorf("%s errnum:%d errmsg:%s", err.Error(), errnum, errmsg)
		}
	}()

	if retval := C.OCIEnvCreate(&obj.envhp, OCI_THREADED|OCI_OBJECT|OCI_DEFAULT|OCI_NEW_LENGTH_SEMANTICS, nil, nil, nil, nil, 0, nil); retval != OCI_SUCCESS {
		return fmt.Errorf("OCIEnvCreate fails result:%d", retval)
	}

	if retval := C.OCIHandleAlloc(unsafe.Pointer(obj.envhp), (*unsafe.Pointer)(unsafe.Pointer(&obj.errhp)), OCI_HTYPE_ERROR, 0, nil); retval != OCI_SUCCESS {
		return fmt.Errorf("OCIHandleAlloc OCI_HTYPE_ERROR fails result:%d", retval)
	}

	if retval := C.OCIHandleAlloc(unsafe.Pointer(obj.envhp), (*unsafe.Pointer)(unsafe.Pointer(&obj.srvhp)), OCI_HTYPE_SERVER, 0, nil); retval != OCI_SUCCESS {
		return fmt.Errorf("OCIHandleAlloc OCI_HTYPE_SERVER fails result:%d", retval)
	}

	if retval := C.OCIHandleAlloc(unsafe.Pointer(obj.envhp), (*unsafe.Pointer)(unsafe.Pointer(&obj.svchp)), OCI_HTYPE_SVCCTX, 0, nil); retval != OCI_SUCCESS {
		return fmt.Errorf("OCIHandleAlloc OCI_HTYPE_SVCCTX fails result:%d", retval)
	}

	if retval := C.OCIHandleAlloc(unsafe.Pointer(obj.envhp), (*unsafe.Pointer)(unsafe.Pointer(&obj.authp)), OCI_HTYPE_SESSION, 0, nil); retval != OCI_SUCCESS {
		return fmt.Errorf("OCIHandleAlloc OCI_HTYPE_SESSION fails result:%d", retval)
	}

	if retval := C.OCIHandleAlloc(unsafe.Pointer(obj.envhp), (*unsafe.Pointer)(unsafe.Pointer(&obj.stmthp)), OCI_HTYPE_STMT, 0, nil); retval != OCI_SUCCESS {
		return fmt.Errorf("OCIHandleAlloc OCI_HTYPE_STMT fails result:%d", retval)
	}

	return nil
}

func (obj *OracleDb) get_server_version() string {

	msg_buf := C.calloc(1, 4096)
	defer func() {
		C.free(msg_buf)
	}()

	C.OCIServerVersion(unsafe.Pointer(obj.svchp), obj.errhp, (*C.OraText)(msg_buf), 4096, OCI_HTYPE_SVCCTX)

	return C.GoString((*C.char)(msg_buf))
}

func (obj *OracleDb) OCINumberToText(number *C.OCINumber, fmtstr string) (string, error) {

	var buf_size C.ub4 = 128

	cs_fmt := unsafe.Pointer(C.CString(fmtstr))
	defer C.free(cs_fmt)

	cs_buf := C.calloc(1, 128)
	defer C.free(cs_buf)

	if retval := C.OCINumberToText(
		obj.errhp,
		number,
		(*C.uchar)(cs_fmt),
		(C.ub4)(len(fmtstr)),
		nil,
		0,
		&buf_size,
		(*C.uchar)(cs_buf)); retval != OCI_SUCCESS {
		errnum, errmsg := obj.ErrMsg()
		return "", fmt.Errorf("OCINumberToText fails retval:%d. errnum:%d errmsg:%s", retval, errnum, errmsg)
	}

	return C.GoStringN((*C.char)(cs_buf), (C.int)(buf_size)), nil
}

func (obj *OracleDb) GetSCNFromPosition(b []byte) (int, error) {

	position := C.CBytes(b)
	defer C.free(position)

	result, err := C.CGOOCISCNPositionToInt(obj.svchp, obj.errhp, (*C.char)(position), (C.ub2)(len(b)))
	if err != nil {
		errnum, errmsg := obj.ErrMsg()
		return -1, fmt.Errorf("CGOOCISCNPositionToInt fails. errnum:%d errmsg:%s", errnum, errmsg)
	}

	return int(result), nil
}

func (obj *OracleDb) ErrMsg() (int, string) {

	var errcodep C.sb4

	msg_buf := C.calloc(1, 4096)
	defer func() {
		C.free(msg_buf)
	}()

	if obj.errhp != nil {
		C.OCIErrorGet(unsafe.Pointer(obj.errhp), 1, nil, &errcodep, (*C.OraText)(msg_buf), 4096, OCI_HTYPE_ERROR)
	} else if obj.envhp != nil {
		C.OCIErrorGet(unsafe.Pointer(obj.envhp), 1, nil, &errcodep, (*C.OraText)(msg_buf), 4096, OCI_HTYPE_ENV)
	}

	return int(errcodep), C.GoString((*C.char)(msg_buf))
}

func (obj *OracleDb) OCIDateToText(date *C.OCIDate) (string, error) {

	var bufsize C.ub4 = 64
	buf := make([]byte, 64)

	cs_buf := unsafe.Pointer(C.CBytes(buf))
	defer C.free(cs_buf)

	cs_fmt := unsafe.Pointer(C.CString(DATE_FORMAT))
	defer C.free(cs_fmt)

	if retval := C.OCIDateToText(
		obj.errhp,
		date,
		(*C.uchar)(cs_fmt),
		(C.ub1)(len(DATE_FORMAT)),
		nil,
		0,
		&bufsize,
		(*C.uchar)(cs_buf)); retval != OCI_SUCCESS {
		errnum, errmsg := obj.ErrMsg()
		return "", fmt.Errorf("OCIDateToText fails. retval:%d errnum:%d errmsg:%s", retval, errnum, errmsg)
	}

	return C.GoStringN((*C.char)(cs_buf), (C.int)(bufsize)), nil
}

func (obj *OracleDb) OCIDateFromText(str_date string) (*C.OCIDate, func(), error) {

	var date *C.OCIDate = nil

	cs_fmt := unsafe.Pointer(C.CString(DATE_FORMAT))
	defer C.free(cs_fmt)

	cs_date := unsafe.Pointer(C.CString(str_date))
	defer C.free(cs_date)

	if err := obj.OCIDescriptorAlloc((*unsafe.Pointer)(unsafe.Pointer(&date)), OCI_DTYPE_DATE); err != nil {
		return nil, nil, err
	}

	destory := func() {
		obj.OCIDescriptorFree(unsafe.Pointer(date), OCI_DTYPE_DATE)
	}

	if retval := C.OCIDateFromText(
		obj.errhp,
		(*C.uchar)(cs_date),
		(C.ub4)(len(str_date)),
		(*C.uchar)(cs_fmt),
		(C.ub1)(len(DATE_FORMAT)),
		nil,
		0,
		date); retval != OCI_SUCCESS {
		errnum, errmsg := obj.ErrMsg()
		destory()
		return nil, nil, fmt.Errorf("OCIDateFromText fails. retval:%d errnum:%d errmsg:%s", retval, errnum, errmsg)
	}

	return date, destory, nil
}

func (obj *OracleDb) OCIIntervalFromNumber(number *C.OCINumber, dtype int) (*C.OCIInterval, func(), error) {

	var interval *C.OCIInterval = nil

	if err := obj.OCIDescriptorAlloc((*unsafe.Pointer)(unsafe.Pointer(&interval)), dtype); err != nil {
		return nil, nil, err
	}

	destory := func() {
		obj.OCIDescriptorFree(unsafe.Pointer(interval), dtype)
	}

	if retval := C.OCIIntervalFromNumber(unsafe.Pointer(obj.authp), obj.errhp, interval, number); retval != OCI_SUCCESS {
		errnum, errmsg := obj.ErrMsg()
		destory()
		return nil, nil, fmt.Errorf("OCIIntervalFromNumber fails. retval:%d errnum:%d errmsg:%s", retval, errnum, errmsg)
	}

	return interval, destory, nil
}

func (obj *OracleDb) OCIIntervalToNumber(interval *C.OCIInterval) (*C.OCINumber, error) {

	var number C.OCINumber

	if retval := C.OCIIntervalToNumber(unsafe.Pointer(obj.authp), obj.errhp, interval, &number); retval != OCI_SUCCESS {
		errnum, errmsg := obj.ErrMsg()
		return nil, fmt.Errorf("OCIIntervalToNumber fails. retval:%d errnum:%d errmsg:%s", retval, errnum, errmsg)
	}

	return &number, nil
}

func (obj *OracleDb) LCRIntervalToOCIInterval(b []byte, dty int) (*C.OCIInterval, func(), error) {

	var interval *C.OCIInterval = nil
	var dtype int
	lcrinterval := (*C.OCIInterval)(C.CBytes(b))
	defer C.free(unsafe.Pointer(lcrinterval))

	switch dty {
	case SQLT_INTERVAL_DS:
		dtype = OCI_DTYPE_INTERVAL_DS
	case SQLT_INTERVAL_YM:
		dtype = OCI_DTYPE_INTERVAL_YM
	default:
		return nil, nil, fmt.Errorf("LCRIntervalToOCIInterval not supported dty:%d", dty)
	}

	if err := obj.OCIDescriptorAlloc((*unsafe.Pointer)(unsafe.Pointer(&interval)), dtype); err != nil {
		return nil, nil, err
	}

	destory := func() {
		obj.OCIDescriptorFree(unsafe.Pointer(interval), dtype)
	}

	if retval := C.OCIIntervalAssign(unsafe.Pointer(obj.authp), obj.errhp, lcrinterval, interval); retval != OCI_SUCCESS {
		errnum, errmsg := obj.ErrMsg()
		destory()
		return nil, nil, fmt.Errorf("OCIIntervalAssign fails. retval:%d errnum:%d errmsg:%s", retval, errnum, errmsg)
	}

	return interval, destory, nil
}

func (obj *OracleDb) LCRDateTimeToOCIDateTime(b []byte, dty int) (*C.OCIDateTime, func(), error) {

	var dtype int
	tm := strings.Split(string(b), "=")

	switch dty {
	case SQLT_TIMESTAMP:
		dtype = OCI_DTYPE_TIMESTAMP
	case SQLT_TIMESTAMP_LTZ:
		dtype = OCI_DTYPE_TIMESTAMP_LTZ
	case SQLT_TIMESTAMP_TZ:
		dtype = OCI_DTYPE_TIMESTAMP_TZ
	}

	return obj.OCIDateTimeFromText(tm[0], tm[1], dtype)

	//var dtype int
	//lcrdatetime := (*C.OCIDateTime)(C.CBytes(b))
	//defer C.free(unsafe.Pointer(lcrdatetime))
	//var datetime *C.OCIDateTime = nil

	//switch dty {
	//case SQLT_TIMESTAMP:
	//	dtype = OCI_DTYPE_TIMESTAMP
	//case SQLT_TIMESTAMP_LTZ:
	//	dtype = OCI_DTYPE_TIMESTAMP_LTZ
	//case SQLT_TIMESTAMP_TZ:
	//	dtype = OCI_DTYPE_TIMESTAMP_TZ
	//default:
	//	return nil, nil, fmt.Errorf("LCRDateTimeToOCIDateTime not supported dty:%d", dty)
	//}

	//if err := obj.OCIDescriptorAlloc((*unsafe.Pointer)(unsafe.Pointer(&datetime)), dtype); err != nil {
	//	return nil, nil, err
	//}

	//destory := func() {
	//	obj.OCIDescriptorFree(unsafe.Pointer(datetime), dtype)
	//}
	//
	//if retval := C.OCIDateTimeAssign(unsafe.Pointer(obj.authp), obj.errhp, lcrdatetime, datetime); retval != OCI_SUCCESS {
	//	errnum, errmsg := obj.ErrMsg()
	//	destory()
	//	return nil, nil, fmt.Errorf("OCIDateTimeAssign fails. retval:%d errnum:%d errmsg:%s", retval, errnum, errmsg)
	//}

	//return datetime, destory, nil
}

func (obj *OracleDb) OCIDateTimeToText(date *C.OCIDateTime, datefmt string) (string, error) {

	var bufsize C.ub4 = 64
	buf := make([]byte, 64)

	cs_buf := unsafe.Pointer(C.CBytes(buf))
	defer C.free(cs_buf)

	cs_datefmt := unsafe.Pointer(C.CString(datefmt))
	defer C.free(cs_datefmt)

	if retval := C.OCIDateTimeToText(
		unsafe.Pointer(obj.authp),
		obj.errhp,
		date,
		(*C.uchar)(cs_datefmt),
		(C.ub1)(len(datefmt)),
		(C.ub1)(DEFAULT_FS_PREC),
		nil,
		0,
		&bufsize,
		(*C.uchar)(cs_buf)); retval != OCI_SUCCESS {
		errnum, errmsg := obj.ErrMsg()
		return "", fmt.Errorf("OCIDateTimeToText fails retval:%d. errnum:%d errmsg:%s", retval, errnum, errmsg)
	}

	return C.GoStringN((*C.char)(cs_buf), (C.int)(bufsize)), nil
}

func (obj *OracleDb) OCIDateTimeFromText(strdate string, datefmt string, dtype int) (*C.OCIDateTime, func(), error) {

	var datetime *C.OCIDateTime = nil

	cs_date := unsafe.Pointer(C.CString(strdate))
	defer C.free(cs_date)

	cs_datefmt := unsafe.Pointer(C.CString(datefmt))
	defer C.free(cs_datefmt)

	if err := obj.OCIDescriptorAlloc((*unsafe.Pointer)(unsafe.Pointer(&datetime)), dtype); err != nil {
		return nil, nil, err
	}

	destory := func() {
		obj.OCIDescriptorFree(unsafe.Pointer(datetime), dtype)
	}

	if retval := C.OCIDateTimeFromText(
		unsafe.Pointer(obj.authp),
		obj.errhp,
		(*C.uchar)(cs_date),
		(C.size_t)(len(strdate)),
		(*C.uchar)(cs_datefmt),
		(C.ub1)(len(datefmt)),
		nil,
		0,
		datetime); retval != OCI_SUCCESS {
		errnum, errmsg := obj.ErrMsg()
		destory()
		return nil, nil, fmt.Errorf("OCIDateTimeFromText fails retval:%d. errnum:%d errmsg:%s", retval, errnum, errmsg)
	}

	return datetime, destory, nil
}

func (obj *OracleDb) OCIDescriptorAlloc(descpp *unsafe.Pointer, dtype int) error {

	if retval := C.OCIDescriptorAlloc(
		unsafe.Pointer(obj.envhp),
		descpp,
		(C.ub4)(dtype),
		0,
		nil); retval != OCI_SUCCESS {
		return fmt.Errorf("OCIDescriptorAlloc type:[%d] fails. retval:%d", dtype, retval)
	}

	return nil
}

func (obj *OracleDb) OCIDescriptorFree(descp unsafe.Pointer, dtype int) error {

	if retval := C.OCIDescriptorFree(descp, (C.ub4)(dtype)); retval != OCI_SUCCESS {
		return fmt.Errorf("OCIDescriptorFree type:%d fails. retval:%d", dtype, retval)
	}

	return nil
}

func (obj *OracleDb) OCIBindByName2(placeholder string, valuep unsafe.Pointer, value_sz int, dty int) error {

	var bndp *C.OCIBind

	cs_placeholder := unsafe.Pointer(C.CString(placeholder))
	defer C.free(cs_placeholder)

	if retval := C.OCIBindByName2(
		obj.stmthp,
		&bndp,
		obj.errhp,
		(*C.uchar)(cs_placeholder),
		(C.sb4)(len(placeholder)),
		valuep,
		(C.sb8)(value_sz),
		(C.ub2)(dty),
		nil,
		nil,
		nil,
		0,
		nil,
		OCI_DEFAULT); retval != OCI_SUCCESS {
		errnum, errmsg := obj.ErrMsg()
		return fmt.Errorf("OCIBindByName2 fails. retval:%d errnum:%d errmsg:%s", retval, errnum, errmsg)
	}

	return nil
}

func (obj *OracleDb) OCIBindByPos2(position int, valuep unsafe.Pointer, value_sz int, dty int) error {

	var bndp *C.OCIBind

	if retval := C.OCIBindByPos2(
		obj.stmthp,
		&bndp,
		obj.errhp,
		(C.ub4)(position),
		valuep,
		(C.sb8)(value_sz),
		(C.ub2)(dty),
		nil,
		nil,
		nil,
		0,
		nil,
		OCI_DEFAULT); retval != OCI_SUCCESS {
		errnum, errmsg := obj.ErrMsg()
		return fmt.Errorf("OCIBindByPos2 fails. retval:%d errnum:%d errmsg:%s", retval, errnum, errmsg)
	}

	return nil
}

func (obj *OracleDb) OCIStmtPrepare2(stmttext string) error {

	cs_stmttext := unsafe.Pointer(C.CString(stmttext))
	defer C.free(cs_stmttext)

	if retval := C.OCIStmtPrepare2(
		obj.svchp,
		&obj.stmthp,
		obj.errhp,
		(*C.uchar)(cs_stmttext),
		(C.ub4)(len(stmttext)),
		nil,
		0,
		OCI_NTV_SYNTAX,
		OCI_DEFAULT); retval != OCI_SUCCESS {
		errnum, errmsg := obj.ErrMsg()
		return fmt.Errorf("OCIStmtPrepare2[%s] fails. retval:%d errnum:%d errmsg:%s", stmttext, retval, errnum, errmsg)
	}

	return nil
}

func (obj *OracleDb) OCIStmtExecute() error {

	begin := time.Now()

	if retval := C.OCIStmtExecute(
		obj.svchp,
		obj.stmthp,
		obj.errhp,
		1,
		0,
		nil,
		nil,
		OCI_DEFAULT); retval != OCI_SUCCESS {
		errnum, errmsg := obj.ErrMsg()
		return fmt.Errorf("OCIStmtExecute fails. retval:%d errnum:%d errmsg:%s", retval, errnum, errmsg)
	}

	nNano := time.Now().UnixNano() - begin.UnixNano()

	glog.V(log.LOG_LEVEL6).Infof("OCIStmtExecute time:%d us", nNano/1000)

	return nil
}

func (obj *OracleDb) OCIStmtRelease() error {

	if retval := C.OCIStmtRelease(obj.stmthp, obj.errhp, nil, 0, OCI_DEFAULT); retval != OCI_SUCCESS {
		errnum, errmsg := obj.ErrMsg()
		return fmt.Errorf("OCIStmtRelease fails. retval:%d errnum:%d errmsg:%s", retval, errnum, errmsg)
	}

	return nil
}

func (obj *OracleDb) OCIUnicodeToCharSet(src string) (string, error) {

	var rsize C.size_t
	var srclen C.size_t = (C.size_t)(len(src))
	var dstlen C.size_t = (C.size_t)(len(src))

	dst := C.calloc(1, dstlen)
	defer C.free(dst)

	cs_src := unsafe.Pointer(C.CString(src))
	defer C.free(cs_src)

	if retval := C.OCIUnicodeToCharSet(
		unsafe.Pointer(obj.envhp),
		(*C.uchar)(dst),
		dstlen,
		(*C.ub2)(cs_src),
		srclen,
		&rsize); retval != OCI_SUCCESS {
		return "", fmt.Errorf("OCIUnicodeToCharSet fails. retval:%d", retval)
	}

	return C.GoStringN((*C.char)(cs_src), (C.int)(rsize)), nil
}

func (obj *OracleDb) OCINlsGetInfo(item int) (string, error) {

	var buflen C.size_t = (C.size_t)(OCI_NLS_MAXBUFSZ)
	buf := C.calloc(1, buflen)
	defer C.free(buf)

	if retval := C.OCINlsGetInfo(
		unsafe.Pointer(obj.envhp),
		obj.errhp,
		(*C.uchar)(buf),
		buflen,
		(C.ub2)(item)); retval != OCI_SUCCESS {
		errnum, errmsg := obj.ErrMsg()
		return "", fmt.Errorf("OCINlsGetInfo fails. retval:%d errnum:%d errmsg:%s", retval, errnum, errmsg)
	}

	return C.GoStringN((*C.char)(buf), (C.int)(buflen)), nil
}

func (obj *OracleDb) OCIAttrSet(trgthndlp unsafe.Pointer, trghndltyp int, attributep unsafe.Pointer, size int, attrtype int) error {

	if retval := C.OCIAttrSet(
		trgthndlp,
		(C.ub4)(trghndltyp),
		attributep,
		(C.ub4)(size),
		(C.ub4)(attrtype),
		obj.errhp); retval != OCI_SUCCESS {
		errnum, errmsg := obj.ErrMsg()
		return fmt.Errorf("OCIAttrSet fails. retval:%d errnum:%d errmsg:%s", retval, errnum, errmsg)
	}

	return nil
}

func (obj *OracleDb) OCILCRRowStmtGet(lcrp unsafe.Pointer) (string, error) {

	var row_stmt_len C.ub4 = DML_SIZE
	row_stmt := C.calloc(1, DML_SIZE)
	defer C.free(row_stmt)

	if retval := C.OCILCRRowStmtGet(
		obj.svchp,
		obj.errhp,
		(*C.uchar)(row_stmt),
		&row_stmt_len,
		lcrp,
		OCI_DEFAULT); retval != OCI_SUCCESS {
		errnum, errmsg := obj.ErrMsg()
		return "", fmt.Errorf("OCILCRRowStmtGet fails. retval:%d errnum:%d errmsg:%s", retval, errnum, errmsg)
	}

	return C.GoStringN((*C.char)(row_stmt), (C.int)(row_stmt_len)), nil
}

func (obj *OracleDb) OCILCRWhereClauseGet(lcrp unsafe.Pointer) (string, error) {

	var wc_stmt_len C.ub4 = DML_SIZE
	wc_stmt := C.calloc(1, DML_SIZE)
	defer C.free(wc_stmt)

	if retval := C.OCILCRWhereClauseGet(
		obj.svchp,
		obj.errhp,
		(*C.uchar)(wc_stmt),
		&wc_stmt_len,
		lcrp,
		OCI_DEFAULT); retval != OCI_SUCCESS {
		errnum, errmsg := obj.ErrMsg()
		return "", fmt.Errorf("OCILCRWhereClauseGet fails. retval:%d errnum:%d errmsg:%s", retval, errnum, errmsg)
	}

	return C.GoStringN((*C.char)(wc_stmt), (C.int)(wc_stmt_len)), nil
}

func (obj *OracleDb) OCILCRSCNCompare(m, n C.int64_t) (int, error) {

	var result C.sword
	var ociNum1, ociNum2 C.OCINumber

	if retval := C.OCINumberFromInt(
		obj.errhp,
		unsafe.Pointer(&m),
		8,
		OCI_NUMBER_SIGNED,
		&ociNum1); retval != OCI_SUCCESS {
		errnum, errmsg := obj.ErrMsg()
		return -1, fmt.Errorf("OCINumberFromInt fails. retvval:%d errnum:%d errmsg:%s", retval, errnum, errmsg)
	}

	if retval := C.OCINumberFromInt(
		obj.errhp,
		unsafe.Pointer(&n),
		8,
		OCI_NUMBER_SIGNED,
		&ociNum2); retval != OCI_SUCCESS {
		errnum, errmsg := obj.ErrMsg()
		return -1, fmt.Errorf("OCINumberFromInt fails. retvval:%d errnum:%d errmsg:%s", retval, errnum, errmsg)
	}

	if retval := C.OCINumberCmp(
		obj.errhp,
		&ociNum1,
		&ociNum2,
		&result); retval != OCI_SUCCESS {
		errnum, errmsg := obj.ErrMsg()
		return -1, fmt.Errorf("OCINumberCmp fails. retvval:%d errnum:%d errmsg:%s", retval, errnum, errmsg)
	}

	return int(result), nil

}
