/*************************************************************************
# File Name: oci/ocixstream.go
# Author: xiezg
# Mail: xzghyd2008@hotmail.com
# Created Time: 2019-07-09 10:56:59
# Last modified: 2019-07-10 22:55:31
************************************************************************/
package oci

const (
	OCI_LCR_COLUMN_LOB_DATA   = (0x00000001) /* col contains lob data */
	OCI_LCR_COLUMN_LONG_DATA  = (0x00000002) /* col contains long data*/
	OCI_LCR_COLUMN_EMPTY_LOB  = (0x00000004) /* col has an empty lob  */
	OCI_LCR_COLUMN_LAST_CHUNK = (0x00000008) /* last chunk of current col*/
	OCI_LCR_COLUMN_AL16UTF16  = (0x00000010) /* col is in AL16UTF16 fmt */
	OCI_LCR_COLUMN_NCLOB      = (0x00000020) /* col has NCLOB data */
	OCI_LCR_COLUMN_XML_DATA   = (0x00000040) /* col contains xml data */
	OCI_LCR_COLUMN_XML_DIFF   = (0x00000080) /* col contains xmldiff data */
	OCI_LCR_COLUMN_ENCRYPTED  = (0x00000100) /* col is encrypted */
	OCI_LCR_COLUMN_UPDATED    = (0x00000200) /* col is updated */
	OCI_LCR_COLUMN_32K_DATA   = (0x00000400) /* col contains 32k data */
	OCI_LCR_COLUMN_OBJ_XML    = (0x00000800) /* col is UDT, rep as XML*/
)

const (
	OCI_LCR_MAX_POSITION_LEN = 64
	OCI_LCR_MAX_TXID_LEN     = 128
)
