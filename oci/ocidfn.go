/*************************************************************************
# File Name: oci/ocidfn.go
# Author: xiezg
# Mail: xzghyd2008@hotmail.com
# Created Time: 2019-07-08 18:48:52
# Last modified: 2019-07-28 14:34:16
************************************************************************/
package oci

const (
	OCI_NUMBER_UNSIGNED = 0 /* Unsigned type -- ubX */
	OCI_NUMBER_SIGNED   = 2 /* Signed type -- sbX */
)

const (
	SQLT_CHR      = 1   /* (ORANET TYPE) character string */
	SQLT_NUM      = 2   /* (ORANET TYPE) oracle numeric */
	SQLT_INT      = 3   /* (ORANET TYPE) integer */
	SQLT_FLT      = 4   /* (ORANET TYPE) Floating point number */
	SQLT_STR      = 5   /* zero terminated string */
	SQLT_VNU      = 6   /* NUM with preceding length byte */
	SQLT_PDN      = 7   /* (ORANET TYPE) Packed Decimal Numeric */
	SQLT_LNG      = 8   /* long */
	SQLT_VCS      = 9   /* Variable character string */
	SQLT_NON      = 10  /* Null/empty PCC Descriptor entry */
	SQLT_RID      = 11  /* rowid */
	SQLT_DAT      = 12  /* date in oracle format */
	SQLT_VBI      = 15  /* binary in VCS format */
	SQLT_BFLOAT   = 21  /* Native Binary float*/
	SQLT_BDOUBLE  = 22  /* NAtive binary double */
	SQLT_BIN      = 23  /* binary data(DTYBIN) */
	SQLT_LBI      = 24  /* long binary */
	SQLT_UIN      = 68  /* unsigned integer */
	SQLT_SLS      = 91  /* Display sign leading separate */
	SQLT_LVC      = 94  /* Longer longs (char) */
	SQLT_LVB      = 95  /* Longer long binary */
	SQLT_AFC      = 96  /* Ansi fixed char */
	SQLT_AVC      = 97  /* Ansi Var char */
	SQLT_IBFLOAT  = 100 /* binary float canonical */
	SQLT_IBDOUBLE = 101 /* binary double canonical */
	SQLT_CUR      = 102 /* cursor  type */
	SQLT_RDD      = 104 /* rowid descriptor */
	SQLT_LAB      = 105 /* label type */
	SQLT_OSL      = 106 /* oslabel type */

	SQLT_NTY    = 108 /* named object type */
	SQLT_REF    = 110 /* ref type */
	SQLT_CLOB   = 112 /* character lob */
	SQLT_BLOB   = 113 /* binary lob */
	SQLT_BFILEE = 114 /* binary file lob */
	SQLT_CFILEE = 115 /* character file lob */
	SQLT_RSET   = 116 /* result set type */
	SQLT_NCO    = 122 /* named collection type (varray or nested table) */
	SQLT_VST    = 155 /* OCIString type */
	SQLT_ODT    = 156 /* OCIDate type */

	/* datetimes and intervals */
	SQLT_DATE          = 184 /* ANSI Date */
	SQLT_TIME          = 185 /* TIME */
	SQLT_TIME_TZ       = 186 /* TIME WITH TIME ZONE */
	SQLT_TIMESTAMP     = 187 /* TIMESTAMP */
	SQLT_TIMESTAMP_TZ  = 188 /* TIMESTAMP WITH TIME ZONE */
	SQLT_INTERVAL_YM   = 189 /* INTERVAL YEAR TO MONTH */
	SQLT_INTERVAL_DS   = 190 /* INTERVAL DAY TO SECOND */
	SQLT_TIMESTAMP_LTZ = 232 /* TIMESTAMP WITH LOCAL TZ */

	SQLT_PNTY = 241 /* pl/sql representation of named types */

	/* some pl/sql specific types */
	SQLT_REC = 250 /* pl/sql 'record' (or %rowtype) */
	SQLT_TAB = 251 /* pl/sql 'indexed table' */
	SQLT_BOL = 252 /* pl/sql 'boolean' */

	/* cxcheng: this has been added for backward compatibility -
	   it needs to be here because ocidfn.h can get included ahead of sqldef.h */
	SQLT_FILE  = SQLT_BFILEE /* binary file lob */
	SQLT_CFILE = SQLT_CFILEE
	SQLT_BFILE = SQLT_BFILEE

	/* CHAR/NCHAR/VARCHAR2/NVARCHAR2/CLOB/NCLOB char set "form" information */
	SQLCS_IMPLICIT = 1 /* for CHAR, VARCHAR2, CLOB w/o a specified set */
	SQLCS_NCHAR    = 2 /* for NCHAR, NCHAR VARYING, NCLOB */
	SQLCS_EXPLICIT = 3 /* for CHAR, etc, with "CHARACTER SET ..." syntax */
	SQLCS_FLEXIBLE = 4 /* for PL/SQL "flexible" parameters */
	SQLCS_LIT_NULL = 5 /* for typecheck of NULL and empty_clob() lits */
)
