/*************************************************************************
# File Name: capture/oci.go
# Author: xiezg
# Mail: xzghyd2008@hotmail.com
# Created Time: 2019-07-07 17:34:23
# Last modified: 2019-09-19 17:14:20
************************************************************************/
package oci

//#include <oci.h>
//#include <stdio.h>
//#include <stdlib.h>
//#include <string.h>
//#include <malloc.h>
//#include <ctype.h>
//#include <errno.h>
//char* CGOOCIXStreamOutLCRReceive( OCISvcCtx *svchp, OCIError *errhp, int *lcrtype_cgo, int *flag_cgo, int *scn ){
//
//    void *lcr = NULL;
//    ub1 lcrtype = 0;
//    oraub8 flag = 0;
//    sword status = 0;
//    char fetch_low_position[OCI_LCR_MAX_POSITION_LEN];
//    ub2 fetch_low_position_len = OCI_LCR_MAX_POSITION_LEN;
//
//    status = OCIXStreamOutLCRReceive(
//        svchp,
//        errhp,
//        &lcr,
//        &lcrtype,
//        &flag,
//        fetch_low_position,
//        &fetch_low_position_len,
//        OCI_DEFAULT);
//
//    if( status == OCI_ERROR ){
//        printf( "OCIXStreamOutLCRReceive fails.%d\n", status );
//        errno = -1;
//        return NULL;
//    }
//
//    if( status == OCI_SUCCESS ){
//        (fetch_low_position_len > 0) && ( *scn = CGOOCISCNPositionToInt( svchp, errhp, fetch_low_position, fetch_low_position_len ));
//        errno = 0;
//        return NULL;
//    }
//
//    if( status == OCI_STILL_EXECUTING ){
//
//        *lcrtype_cgo = lcrtype;
//        *flag_cgo = flag;
//        return (char*)lcr;
//    }
//
//    return NULL;
//}
import "C"
import "fmt"
import "time"
import "unsafe"

type OutBound struct {
	OracleDb
	outbound string
}

func InitOutbound(outbound string, dbname string, uname string, password string) (*OutBound, error) {

	var obj OutBound

	if err := obj.init_env_handle(); err != nil {
		return nil, err
	}

	obj.outbound = outbound
	obj.dbname = dbname
	obj.uname = uname
	obj.password = password

	if err := obj.connect_outbound(); err != nil {
		return nil, err
	}

	return &obj, nil
}

func (obj *OutBound) free_oci_handle() {

	if obj.svchp != nil {
		C.OCIXStreamOutDetach(obj.svchp, obj.errhp, OCI_DEFAULT)
	}

	obj.disconnect_server()
}

func (obj *OutBound) connect_outbound() error {

	cs_outbound := (*C.uchar)(unsafe.Pointer(C.CString(obj.outbound)))
	defer C.free(unsafe.Pointer(cs_outbound))

	if err := obj.connect_server(); err != nil {
		return err
	}

	count := 0
REATTACH_OUTBOUND:
	if retval := C.OCIXStreamOutAttach(obj.svchp, obj.errhp, cs_outbound, C.ushort(len(obj.outbound)), nil, 0, OCI_DEFAULT); retval != OCI_SUCCESS {
		errnum, errmsg := obj.ErrMsg()
		if errnum == 26812 && count < 5 {
			time.Sleep(1 * time.Second)
			count++
			goto REATTACH_OUTBOUND
		}
		return fmt.Errorf("OCIXStreamOutAttach [%s] fails retval:%d errnum:%d errmsg:%s", obj.outbound, retval, errnum, errmsg)
	}

	return nil
}

func (obj *OutBound) LWMSet(pos []byte) error {

	proclwm := C.CBytes(pos)
	defer C.free(proclwm)

	if C.OCIXStreamOutProcessedLWMSet(obj.svchp, obj.errhp, (*C.ub1)(proclwm), (C.ub2)(len(pos)), OCI_DEFAULT) != OCI_SUCCESS {
		errnum, errmsg := obj.ErrMsg()
		return fmt.Errorf("OCIXStreamOutProcessedLWMSet fails. errnum:%d errmsg:%s", errnum, errmsg)
	}

	return nil
}

func (obj *OutBound) Get_lcrs() (LCR, int, error) {

	var lcrtype, flag, scn C.int

	lcrp, err := C.CGOOCIXStreamOutLCRReceive(obj.svchp, obj.errhp, &lcrtype, &flag, &scn)
	if err != nil {
		errnum, errmsg := obj.ErrMsg()
		return nil, 0, fmt.Errorf("CGOOCIXStreamOutLCRReceive fails. err:%v errnum:%d errmsg:%s\n", err, errnum, errmsg)
	}

	if lcrp == nil {
		return nil, int(scn), nil
	}

	if lcrtype == OCI_LCR_XDDL {
		var ddl Ddl_lcr_t
		if err := ddl.Parse_lcr(obj, unsafe.Pointer(lcrp)); err != nil {
			return nil, 0, err
		}

		return &ddl, int(scn), nil
	}

	if lcrtype == OCI_LCR_XROW {
		var row Row_lcr_t

		if err := row.Parse_lcr(obj, unsafe.Pointer(lcrp)); err != nil {
			return nil, 0, err
		}

		if flag&OCI_XSTREAM_MORE_ROW_DATA == OCI_XSTREAM_MORE_ROW_DATA {
			if err := row.Recv_chunk(obj); err != nil {
				return nil, 0, err
			}
		}

		return &row, int(scn), nil
	}

	return nil, 0, nil
}
