/*************************************************************************
# File Name: oci/oci.go
# Author: xiezg
# Mail: xzghyd2008@hotmail.com
# Created Time: 2019-07-06 20:48:46
# Last modified: 2019-08-08 12:24:24
************************************************************************/
package oci

const (
	OCI_DEFAULT = 0
)

const (
	OCI_ATTR_CHARSET_ID   = 31 /* Character Set ID */
	OCI_ATTR_CHARSET_FORM = 32
	OCI_UTF16ID           = 1000 /* UTF16 charset ID */
)

const (
	OCI_ONE_PIECE   = 0 /* one piece */
	OCI_FIRST_PIECE = 1 /* the first piece */
	OCI_NEXT_PIECE  = 2 /* the next of many pieces */
	OCI_LAST_PIECE  = 3 /* the last piece */

)

const (
	OCI_NTV_SYNTAX = 1 /* Use what so ever is the native lang of server */
)

const (
	MAX_COLUMNS = 1000 /* max cols in a table */
)

const (
	OCI_CRED_RDBMS = 1
)

const (
	OCI_IND_NOTNULL     = 0  /* not NULL */
	OCI_IND_NULL        = -1 /* NULL */
	OCI_IND_BADNULL     = -2 /* BAD NULL */
	OCI_IND_NOTNULLABLE = -3 /* not NULLable */
)

const (
	OCI_ATTR_SERVER   = 6
	OCI_ATTR_SESSION  = 7
	OCI_ATTR_USERNAME = 22
	OCI_ATTR_PASSWORD = 23
)

const (
	OCI_LCR_XROW = 3
	OCI_LCR_XDDL = 4
)

const (
	OCI_XSTREAM_MORE_ROW_DATA = 0x01
)

const (
	OCI_LCR_ROW_CMD_INSERT    = "INSERT"
	OCI_LCR_ROW_CMD_DELETE    = "DELETE"
	OCI_LCR_ROW_CMD_UPDATE    = "UPDATE"
	OCI_LCR_ROW_CMD_COMMIT    = "COMMIT"
	OCI_LCR_ROW_CMD_LOB_WRITE = "LOB WRITE"
	OCI_LCR_ROW_CMD_LOB_TRIM  = "LOB TRIM"
	OCI_LCR_ROW_CMD_LOB_ERASE = "LOB ERASE"
	OCI_LCR_ROW_CMD_ROLLBACK  = "ROLLBACK"
	OCI_LCR_ROW_CMD_START_TX  = "START_TX"
	OCI_LCR_ROW_CMD_CTRL_INFO = "CONTROL INFO"
)

const (
	OCI_LCR_ROW_COLVAL_OLD = 0
	OCI_LCR_ROW_COLVAL_NEW = 1
)

const (
	OCI_SUCCESS              = 0
	OCI_SUCCESS_WITH_INFO    = 1
	OCI_RESERVED_FOR_INT_USE = 200
	OCI_NO_DATA              = 100
	OCI_ERROR                = -1
	OCI_INVALID_HANDLE       = -2
	OCI_NEED_DATA            = 99
	OCI_STILL_EXECUTING      = -3123
	OCI_CONTINUE             = -24200
	OCI_ROWCBK_DONE          = -24201
)
const (
	OCI_HTYPE_FIRST                = 1
	OCI_HTYPE_ENV                  = 1
	OCI_HTYPE_ERROR                = 2
	OCI_HTYPE_SVCCTX               = 3
	OCI_HTYPE_STMT                 = 4
	OCI_HTYPE_BIND                 = 5
	OCI_HTYPE_DEFINE               = 6
	OCI_HTYPE_DESCRIBE             = 7
	OCI_HTYPE_SERVER               = 8
	OCI_HTYPE_SESSION              = 9
	OCI_HTYPE_AUTHINFO             = OCI_HTYPE_SESSION
	OCI_HTYPE_TRANS                = 10
	OCI_HTYPE_COMPLEXOBJECT        = 11
	OCI_HTYPE_SECURITY             = 12
	OCI_HTYPE_SUBSCRIPTION         = 13
	OCI_HTYPE_DIRPATH_CTX          = 14
	OCI_HTYPE_DIRPATH_COLUMN_ARRAY = 15
	OCI_HTYPE_DIRPATH_STREAM       = 16
	OCI_HTYPE_PROC                 = 17
	OCI_HTYPE_DIRPATH_FN_CTX       = 18
	OCI_HTYPE_DIRPATH_FN_COL_ARRAY = 19
	OCI_HTYPE_XADSESSION           = 20
	OCI_HTYPE_XADTABLE             = 21
	OCI_HTYPE_XADFIELD             = 22
	OCI_HTYPE_XADGRANULE           = 23
	OCI_HTYPE_XADRECORD            = 24
	OCI_HTYPE_XADIO                = 25
	OCI_HTYPE_CPOOL                = 26
	OCI_HTYPE_SPOOL                = 27
	OCI_HTYPE_ADMIN                = 28
	OCI_HTYPE_EVENT                = 29
	OCI_HTYPE_SODA_COLLECTION      = 30
	OCI_HTYPE_SODA_DOCUMENT        = 31
	OCI_HTYPE_SODA_COLL_CURSOR     = 32
	OCI_HTYPE_SODA_OPER_OPTIONS    = 33
	OCI_HTYPE_SODA_OUTPUT_OPTIONS  = 34
	OCI_HTYPE_SODA_METADATA        = 35
	OCI_HTYPE_SODA_DOC_CURSOR      = 36
)

const (
	OCI_DTYPE_FIRST             = 50 /* start value of descriptor type */
	OCI_DTYPE_LOB               = 50 /* lob  locator */
	OCI_DTYPE_SNAP              = 51 /* snapshot descriptor */
	OCI_DTYPE_RSET              = 52 /* result set descriptor */
	OCI_DTYPE_PARAM             = 53 /* a parameter descriptor obtained from ocigparm */
	OCI_DTYPE_ROWID             = 54 /* rowid descriptor */
	OCI_DTYPE_COMPLEXOBJECTCOMP = 55
	/* complex object retrieval descriptor */
	OCI_DTYPE_FILE                 = 56 /* File Lob locator */
	OCI_DTYPE_AQENQ_OPTIONS        = 57 /* enqueue options */
	OCI_DTYPE_AQDEQ_OPTIONS        = 58 /* dequeue options */
	OCI_DTYPE_AQMSG_PROPERTIES     = 59 /* message properties */
	OCI_DTYPE_AQAGENT              = 60 /* aq agent */
	OCI_DTYPE_LOCATOR              = 61 /* LOB locator */
	OCI_DTYPE_INTERVAL_YM          = 62 /* Interval year month */
	OCI_DTYPE_INTERVAL_DS          = 63 /* Interval day second */
	OCI_DTYPE_AQNFY_DESCRIPTOR     = 64 /* AQ notify descriptor */
	OCI_DTYPE_DATE                 = 65 /* Date */
	OCI_DTYPE_TIME                 = 66 /* Time */
	OCI_DTYPE_TIME_TZ              = 67 /* Time with timezone */
	OCI_DTYPE_TIMESTAMP            = 68 /* Timestamp */
	OCI_DTYPE_TIMESTAMP_TZ         = 69 /* Timestamp with timezone */
	OCI_DTYPE_TIMESTAMP_LTZ        = 70 /* Timestamp with local tz */
	OCI_DTYPE_UCB                  = 71 /* user callback descriptor */
	OCI_DTYPE_SRVDN                = 72 /* server DN list descriptor */
	OCI_DTYPE_SIGNATURE            = 73 /* signature */
	OCI_DTYPE_RESERVED_1           = 74 /* reserved for internal use */
	OCI_DTYPE_AQLIS_OPTIONS        = 75 /* AQ listen options */
	OCI_DTYPE_AQLIS_MSG_PROPERTIES = 76 /* AQ listen msg props */
	OCI_DTYPE_CHDES                = 77 /* Top level change notification desc */
	OCI_DTYPE_TABLE_CHDES          = 78 /* Table change descriptor           */
	OCI_DTYPE_ROW_CHDES            = 79 /* Row change descriptor            */
	OCI_DTYPE_CQDES                = 80 /* Query change descriptor */
	OCI_DTYPE_LOB_REGION           = 81 /* LOB Share region descriptor */
	OCI_DTYPE_AQJMS_PROPERTIES     = 82 /* AQ JMS message properties */
	OCI_DTYPE_SHARDING_KEY         = 83 /* Sharding/Super Sharding Key Descriptor */
	OCI_DTYPE_SHARD_INST           = 84 /* Shard Instance Descriptor */
)

const (
	OCI_THREADED             = 0x00000001 /* appl. in threaded environment */
	OCI_OBJECT               = 0x00000002 /* application in object environment */
	OCI_EVENTS               = 0x00000004 /* application is enabled for events */
	OCI_RESERVED1            = 0x00000008 /* reserved */
	OCI_SHARED               = 0x00000010 /* the application is in shared mode */
	OCI_RESERVED2            = 0x00000020 /* reserved */
	OCI_NEW_LENGTH_SEMANTICS = 0x00020000 /* adopt new length semantics */
)

const (
	OCI_NLS_DAYNAME1           = 1  /* Native name for Monday */
	OCI_NLS_DAYNAME2           = 2  /* Native name for Tuesday */
	OCI_NLS_DAYNAME3           = 3  /* Native name for Wednesday */
	OCI_NLS_DAYNAME4           = 4  /* Native name for Thursday */
	OCI_NLS_DAYNAME5           = 5  /* Native name for Friday */
	OCI_NLS_DAYNAME6           = 6  /* Native name for for Saturday */
	OCI_NLS_DAYNAME7           = 7  /* Native name for for Sunday */
	OCI_NLS_ABDAYNAME1         = 8  /* Native abbreviated name for Monday */
	OCI_NLS_ABDAYNAME2         = 9  /* Native abbreviated name for Tuesday */
	OCI_NLS_ABDAYNAME3         = 10 /* Native abbreviated name for Wednesday */
	OCI_NLS_ABDAYNAME4         = 11 /* Native abbreviated name for Thursday */
	OCI_NLS_ABDAYNAME5         = 12 /* Native abbreviated name for Friday */
	OCI_NLS_ABDAYNAME6         = 13 /* Native abbreviated name for for Saturday */
	OCI_NLS_ABDAYNAME7         = 14 /* Native abbreviated name for for Sunday */
	OCI_NLS_MONTHNAME1         = 15 /* Native name for January */
	OCI_NLS_MONTHNAME2         = 16 /* Native name for February */
	OCI_NLS_MONTHNAME3         = 17 /* Native name for March */
	OCI_NLS_MONTHNAME4         = 18 /* Native name for April */
	OCI_NLS_MONTHNAME5         = 19 /* Native name for May */
	OCI_NLS_MONTHNAME6         = 20 /* Native name for June */
	OCI_NLS_MONTHNAME7         = 21 /* Native name for July */
	OCI_NLS_MONTHNAME8         = 22 /* Native name for August */
	OCI_NLS_MONTHNAME9         = 23 /* Native name for September */
	OCI_NLS_MONTHNAME10        = 24 /* Native name for October */
	OCI_NLS_MONTHNAME11        = 25 /* Native name for November */
	OCI_NLS_MONTHNAME12        = 26 /* Native name for December */
	OCI_NLS_ABMONTHNAME1       = 27 /* Native abbreviated name for January */
	OCI_NLS_ABMONTHNAME2       = 28 /* Native abbreviated name for February */
	OCI_NLS_ABMONTHNAME3       = 29 /* Native abbreviated name for March */
	OCI_NLS_ABMONTHNAME4       = 30 /* Native abbreviated name for April */
	OCI_NLS_ABMONTHNAME5       = 31 /* Native abbreviated name for May */
	OCI_NLS_ABMONTHNAME6       = 32 /* Native abbreviated name for June */
	OCI_NLS_ABMONTHNAME7       = 33 /* Native abbreviated name for July */
	OCI_NLS_ABMONTHNAME8       = 34 /* Native abbreviated name for August */
	OCI_NLS_ABMONTHNAME9       = 35 /* Native abbreviated name for September */
	OCI_NLS_ABMONTHNAME10      = 36 /* Native abbreviated name for October */
	OCI_NLS_ABMONTHNAME11      = 37 /* Native abbreviated name for November */
	OCI_NLS_ABMONTHNAME12      = 38 /* Native abbreviated name for December */
	OCI_NLS_YES                = 39 /* Native string for affirmative response */
	OCI_NLS_NO                 = 40 /* Native negative response */
	OCI_NLS_AM                 = 41 /* Native equivalent string of AM */
	OCI_NLS_PM                 = 42 /* Native equivalent string of PM */
	OCI_NLS_AD                 = 43 /* Native equivalent string of AD */
	OCI_NLS_BC                 = 44 /* Native equivalent string of BC */
	OCI_NLS_DECIMAL            = 45 /* decimal character */
	OCI_NLS_GROUP              = 46 /* group separator */
	OCI_NLS_DEBIT              = 47 /* Native symbol of debit */
	OCI_NLS_CREDIT             = 48 /* Native sumbol of credit */
	OCI_NLS_DATEFORMAT         = 49 /* Oracle date format */
	OCI_NLS_INT_CURRENCY       = 50 /* International currency symbol */
	OCI_NLS_LOC_CURRENCY       = 51 /* Locale currency symbol */
	OCI_NLS_LANGUAGE           = 52 /* Language name */
	OCI_NLS_ABLANGUAGE         = 53 /* Abbreviation for language name */
	OCI_NLS_TERRITORY          = 54 /* Territory name */
	OCI_NLS_CHARACTER_SET      = 55 /* Character set name */
	OCI_NLS_LINGUISTIC_NAME    = 56 /* Linguistic name */
	OCI_NLS_CALENDAR           = 57 /* Calendar name */
	OCI_NLS_DUAL_CURRENCY      = 78 /* Dual currency symbol */
	OCI_NLS_WRITINGDIR         = 79 /* Language writing direction */
	OCI_NLS_ABTERRITORY        = 80 /* Territory Abbreviation */
	OCI_NLS_DDATEFORMAT        = 81 /* Oracle default date format */
	OCI_NLS_DTIMEFORMAT        = 82 /* Oracle default time format */
	OCI_NLS_SFDATEFORMAT       = 83 /* Local string formatted date format */
	OCI_NLS_SFTIMEFORMAT       = 84 /* Local string formatted time format */
	OCI_NLS_NUMGROUPING        = 85 /* Number grouping fields */
	OCI_NLS_LISTSEP            = 86 /* List separator */
	OCI_NLS_MONDECIMAL         = 87 /* Monetary decimal character */
	OCI_NLS_MONGROUP           = 88 /* Monetary group separator */
	OCI_NLS_MONGROUPING        = 89 /* Monetary grouping fields */
	OCI_NLS_INT_CURRENCYSEP    = 90 /* International currency separator */
	OCI_NLS_CHARSET_MAXBYTESZ  = 91 /* Maximum character byte size      */
	OCI_NLS_CHARSET_FIXEDWIDTH = 92 /* Fixed-width charset byte size    */
	OCI_NLS_CHARSET_ID         = 93 /* Character set id */
	OCI_NLS_NCHARSET_ID        = 94 /* NCharacter set id */

	OCI_NLS_MAXBUFSZ = 100 /* Max buffer size may need for OCINlsGetInfo */

	OCI_NLS_BINARY           = 0x1  /* for the binary comparison */
	OCI_NLS_LINGUISTIC       = 0x2  /* for linguistic comparison */
	OCI_NLS_CASE_INSENSITIVE = 0x10 /* for case-insensitive comparison */

	OCI_NLS_UPPERCASE = 0x20 /* convert to uppercase */
	OCI_NLS_LOWERCASE = 0x40 /* convert to lowercase */

	OCI_NLS_CS_IANA_TO_ORA       = 0 /* Map charset name from IANA to Oracle */
	OCI_NLS_CS_ORA_TO_IANA       = 1 /* Map charset name from Oracle to IANA */
	OCI_NLS_LANG_ISO_TO_ORA      = 2 /* Map language name from ISO to Oracle */
	OCI_NLS_LANG_ORA_TO_ISO      = 3 /* Map language name from Oracle to ISO */
	OCI_NLS_TERR_ISO_TO_ORA      = 4 /* Map territory name from ISO to Oracle*/
	OCI_NLS_TERR_ORA_TO_ISO      = 5 /* Map territory name from Oracle to ISO*/
	OCI_NLS_TERR_ISO3_TO_ORA     = 6 /* Map territory name from 3-letter ISO */
	OCI_NLS_TERR_ORA_TO_ISO3     = 7 /* Map territory name from Oracle to    */
	OCI_NLS_LOCALE_A2_ISO_TO_ORA = 8 /*Map locale name from A2 ISO to oracle*/
)
