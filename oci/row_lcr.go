/*************************************************************************
# File Name: capture/row_lcr.go
# Author: xiezg
# Mail: xzghyd2008@hotmail.com
# Created Time: 2019-07-04 20:17:24
# Last modified: 2019-09-10 16:13:04
************************************************************************/
package oci

//#include <stdlib.h>
//#include <oci.h>
import "C"
import "io"
import "fmt"
import "unsafe"
import "encoding/gob"
import "dbbak/log"
import "github.com/golang/glog"
import "dbbak/Implementation/ddlcheck"

var FLAGS_ROW_LCR = []byte{0xE6, 0xA8, 0x79, 0xFE}

var NOT_SUPPORT_CMD_TYPE = fmt.Errorf("not support cmd type")

type Row_lcr_t struct {
	Common_lcr_t
	New_columns columns_t
	Old_columns columns_t
	Chunk       []chunk_t
}

func (obj *Row_lcr_t) ExecFilter(filter *ddlcheck.DDlFiter) bool {

	if filter == nil {
		return false
	}

	for _, unit := range filter.Map {
		if obj.Owner != unit.SrcUserName && unit.SrcUserName != "*" {
			continue
		}

		if obj.Oname != unit.SrcTableName && unit.SrcTableName != "*" {
			continue
		}

		if unit.DstUserName != "*" {
			obj.Owner = unit.DstUserName
		}

		if unit.DstTableName != "*" {
			obj.Oname = unit.DstTableName
		}
	}

	return true
}

func (obj *Row_lcr_t) Serialize(w io.Writer) error {
	if _, err := w.Write(FLAGS_ROW_LCR); err != nil {
		return err
	}

	return gob.NewEncoder(w).Encode(obj)
}

func (obj *Row_lcr_t) Unserialize(r io.Reader) error {
	if err := gob.NewDecoder(r).Decode(obj); err != nil {
		return err
	}

	if obj.Cmd_type == OCI_LCR_ROW_CMD_LOB_WRITE {
		return NOT_SUPPORT_CMD_TYPE
	}

	for i := 0; i < len(obj.Chunk); i++ {

		if obj.New_columns.bind_chunk(&obj.Chunk[i]) {
			continue
		}

		if obj.Old_columns.bind_chunk(&obj.Chunk[i]) {
			continue
		}

		return fmt.Errorf("can't bind chunk:%s with column\n%v", obj.Chunk[i].Column_names, obj.String())
	}

	return nil
}

func (obj *Row_lcr_t) String() string {

	str := obj.Common_lcr_t.String()
	str = str + obj.New_columns.String()
	str = str + obj.Old_columns.String()

	for _, val := range obj.Chunk {
		str = str + val.String()
	}

	return str
}

func (obj *Row_lcr_t) Parse_lcr(db *OutBound, lcr unsafe.Pointer) error {

	if err := obj.Common_lcr_t.get_lcr_header(db, lcr); err != nil {
		return err
	}

	if obj.Cmd_type == OCI_LCR_ROW_CMD_INSERT || obj.Cmd_type == OCI_LCR_ROW_CMD_UPDATE {
		if err := obj.New_columns.parse_row_column_info(db, lcr, OCI_LCR_ROW_COLVAL_NEW); err != nil {
			return err
		}
	}

	if obj.Cmd_type == OCI_LCR_ROW_CMD_DELETE || obj.Cmd_type == OCI_LCR_ROW_CMD_UPDATE {
		if err := obj.Old_columns.parse_row_column_info(db, lcr, OCI_LCR_ROW_COLVAL_OLD); err != nil {
			return err
		}
	}

	return nil
}

func (obj *Row_lcr_t) Recv_chunk(db *OutBound) error {

	for {
		var chunk chunk_t

		ok, err := chunk.get_data(db)
		if err != nil {
			return err
		}

		obj.Chunk = append(obj.Chunk, chunk)

		if !ok {
			break
		}
	}

	return nil
}

func (obj *Row_lcr_t) ExecSQL(db *OracleDb) error {

	obj.filter()

	defer func() {
		obj.New_columns.Release()
		obj.Old_columns.Release()
		db.OCIStmtRelease()
	}()

	switch obj.Cmd_type {
	case OCI_LCR_ROW_CMD_INSERT:
		return obj.insert(db)
	case OCI_LCR_ROW_CMD_COMMIT:
		return obj.commit(db)
	case OCI_LCR_ROW_CMD_UPDATE:
		return obj.update(db)
	case OCI_LCR_ROW_CMD_DELETE:
		return obj.delete(db)
	}

	return nil
}

func (obj *Row_lcr_t) filter() {
	obj.Src_db_name = "target"
}

func (obj *Row_lcr_t) delete(db *OracleDb) error {

	sql := fmt.Sprintf("DELETE FROM \"%s\".\"%s\" WHERE %s AND rownum < 2",
		obj.Owner,
		obj.Oname,
		obj.Old_columns.get_where_express())

	glog.V(log.LOG_LEVEL8).Info(sql)

	if err := db.OCIStmtPrepare2(sql); err != nil {
		return err
	}

	if err := obj.Old_columns.bind_data(db); err != nil {
		return err
	}

	return db.OCIStmtExecute()
}

func (obj *Row_lcr_t) update(db *OracleDb) error {

	sql := fmt.Sprintf("UPDATE \"%s\".\"%s\" SET %s WHERE %s AND rownum < 2",
		obj.Owner,
		obj.Oname,
		obj.New_columns.get_update_express(),
		obj.Old_columns.get_where_express())

	glog.V(log.LOG_LEVEL8).Info(sql)

	if err := db.OCIStmtPrepare2(sql); err != nil {
		return err
	}

	if err := obj.New_columns.bind_data(db); err != nil {
		return err
	}

	if err := obj.Old_columns.bind_data(db); err != nil {
		return err
	}

	return db.OCIStmtExecute()
}

func (obj *Row_lcr_t) insert(db *OracleDb) error {

	sql := fmt.Sprintf("INSERT INTO \"%s\".\"%s\" ( %s )VALUES( %s )",
		obj.Owner,
		obj.Oname,
		obj.New_columns.get_insert_key_express(),
		obj.New_columns.get_insert_val_express())

	glog.V(log.LOG_LEVEL8).Info(sql)

	if err := db.OCIStmtPrepare2(sql); err != nil {
		return err
	}

	if err := obj.New_columns.bind_data(db); err != nil {
		return err
	}

	return db.OCIStmtExecute()
}
