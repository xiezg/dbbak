/*************************************************************************
# File Name: task/common.go
# Author: xiezg
# Mail: xzghyd2008@hotmail.com
# Created Time: 2019-07-21 13:01:14
# Last modified: 2019-09-19 23:41:22
************************************************************************/
package task

//#include <sys/types.h>
//#include <unistd.h>
import "C"
import "os"
import "fmt"
import "net"
import "path"
import "time"
import "bufio"
import "bytes"
import "strings"
import "dbbak/ini"
import "dbbak/oci"
import "dbbak/log"
import "dbbak/common"

//import "dbbak/Implementation"
import "github.com/golang/glog"

//import "dbbak/Implementation/mapcheck"
//import "dbbak/Implementation/ddlcheck"
//import "dbbak/Implementation/tablecheck"

const (
	TASK_FLAG_LENGTH = 8
	TASK_CAPTURE     = 0
	TASK_APPLY       = 1
)

const (
	PROCESS_STOPED    = 0
	PROCESS_STOPING   = 1
	PROCESS_STARTING  = 2
	PROCESS_RUNNING   = 3
	PROCESS_DELETEING = 4
	PROCESS_FAULT     = 5
)

const (
	CMD_SHUTDONW = "SHUTDOWN"
)

var APPLY_TASK_FLAGS = []byte{0x71, 0x70, 0xAE, 0xAF, 0x30, 0x93, 0x89, 0x90}
var CAPTURE_TASK_FLAGS = []byte{0x51, 0x50, 0xFA, 0xF0, 0x45, 0x95, 0x11, 0x12}
var DB_LOG_TERMINAL_FLAGS = []byte{0x52, 0x51, 0x39, 0x37, 0x38, 0x36, 0x52, 0x51, 0xFA, 0xFC, 0xFB, 0xFD, 0x01, 0x03, 0x09, 0xA2}

type tableUnit struct {
	Owner string
	Oname string
}

type m2Unit struct {
	OldOwner string
	OldOname string
	NewOwner string
	NewOname string
}

type preExecUnit struct {
	tlbUnit []tableUnit
	mapUnit []m2Unit
}

type DbTask interface {
	GetDbAddr() string
	Serialize() error
	UnserializeFromJson([]byte) error
	UnserializeFromFile(fpath string) error
	TaskName() string
	SetTaskState(int)
	GetTaskState() int
	GetTaskMode() string
	ShutDown()
	EnableTask()
	RestartTask() bool
	RecvClientMsg(string) string
	SendMonitorMsg() error
	GetOrder() int
	SetOrder(int)
	LogPrefixName() string
	CreateOutBound() error
	DropOutBound() error
	Match(oci.LCR) bool
	MatchAndMapped(oci.LCR) oci.LCR
}

type Task_process_info struct {
	Order             int
	Name              string
	DbAddr            []string
	DbUserName        string
	DbUserPwd         string
	LogFileNamePrefix string
	AutoStart         int
	DDL               string
	DML               string
	Status            int
	LastTranTime      int64
	LogFileSize       int
	LogFileTotalSize  int
	CaptureType       int
	Lag               string
	RAC               bool
	CSN               int
	LogKeepTime       int
	LogKeepMaxSize    int
	ShutDownFlags     bool
	Pid               C.pid_t
	pUnit             preExecUnit
}

func (obj *Task_process_info) String() string {

	str := ""

	str = str + fmt.Sprintln("Order             :", obj.Order)
	str = str + fmt.Sprintln("Name              :", obj.Name)
	str = str + fmt.Sprintln("DbAddr            :", obj.DbAddr)
	str = str + fmt.Sprintln("DbUserName        :", obj.DbUserName)
	str = str + fmt.Sprintln("DbUserPwd         :", obj.DbUserPwd)
	str = str + fmt.Sprintln("LogFileNamePrefix :", obj.LogFileNamePrefix)
	str = str + fmt.Sprintln("AutoStart         :", obj.AutoStart)
	str = str + fmt.Sprintln("DDL               :", obj.DDL)
	str = str + fmt.Sprintln("DML               :", obj.DML)
	str = str + fmt.Sprintln("Status            :", obj.Status)
	str = str + fmt.Sprintln("LastTranTime      :", obj.LastTranTime)
	str = str + fmt.Sprintln("LogFileSize       :", obj.LogFileSize)
	str = str + fmt.Sprintln("LogFileTotalSize  :", obj.LogFileTotalSize)
	str = str + fmt.Sprintln("CaptureType       :", obj.CaptureType)
	str = str + fmt.Sprintln("Lag               :", obj.Lag)
	str = str + fmt.Sprintln("RAC               :", obj.RAC)
	str = str + fmt.Sprintln("CSN               :", obj.CSN)
	str = str + fmt.Sprintln("LogKeepTime       :", obj.LogKeepTime)
	str = str + fmt.Sprintln("LogKeepMaxSize    :", obj.LogKeepMaxSize)
	str = str + fmt.Sprintln("ShutDownFlags     :", obj.ShutDownFlags)
	str = str + fmt.Sprintln("Pid               :", obj.Pid)

	return str
}

func (obj *Task_process_info) ParseDDL() error {

	inReader := bufio.NewReader(bytes.NewBufferString(obj.DDL))
	scanner := bufio.NewScanner(inReader)
	scanner.Split(bufio.ScanLines)

	for scanner.Scan() {
		temp := scanner.Text()

		if len(temp) == 0 || temp == "\r\n" || temp == "\n" || temp == "\t" {
			continue
		}

		glog.V(log.LOG_LEVEL8).Info("DDL:", temp)

		if temp[len(temp)-1] != ';' {
			return fmt.Errorf("invalid statment:%v", temp)
		}

		temp = temp[:len(temp)-1]
		temp = strings.TrimSpace(temp)

		if len(temp) > 5 && strings.ToUpper(temp[:5]) == "TABLE" {
			owner, oname, err := obj.parseDDLTable(temp)
			if err != nil {
				return err
			}
			obj.pUnit.tlbUnit = append(obj.pUnit.tlbUnit, tableUnit{owner, oname})
		} else if len(temp) > 3 && strings.ToUpper(temp[:3]) == "MAP" {
			oldOwner, oldOname, newOwner, newOname, err := obj.parseDDLMapped(temp)
			if err != nil {
				return err
			}
			obj.pUnit.mapUnit = append(obj.pUnit.mapUnit, m2Unit{oldOwner, oldOname, newOwner, newOname})
		} else {
			return fmt.Errorf("invalid statment:%v", temp)
		}
	}

	return nil
}

func (obj *Task_process_info) parseDDLTable(str string) (string, string, error) {

	_, owner, oname, err := parseGroupUnit(str)
	if err != nil {
		return "", "", err
	}

	return owner, oname, nil
}

func (obj *Task_process_info) parseDDLMapped(str string) (string, string, string, string, error) {

	unit := strings.Split(str, ",")

	_, map_owner, map_oname, map_err := parseGroupUnit(unit[0])
	if map_err != nil {
		return "", "", "", "", map_err
	}

	_, target_owner, target_oname, target_err := parseGroupUnit(unit[1])
	if target_err != nil {
		return "", "", "", "", target_err
	}

	return map_owner, map_oname, target_owner, target_oname, nil
}

func (obj *Task_process_info) Match(lcr oci.LCR) bool {

	if lcr.CmdType() == oci.OCI_LCR_ROW_CMD_COMMIT {
		return true
	}

	for _, item := range obj.pUnit.tlbUnit {

		if item.Owner != lcr.GetOwner() && item.Owner != "*" {
			continue
		}

		if item.Oname != lcr.GetOname() && item.Oname != "*" {
			continue
		}

		return true
	}

	return false
}

func (obj *Task_process_info) MatchAndMapped(lcr oci.LCR) oci.LCR {

	for _, item := range obj.pUnit.mapUnit {

		if item.OldOwner != lcr.GetOwner() && item.OldOwner != "*" {
			continue
		}

		if item.OldOname != lcr.GetOname() && item.OldOname != "*" {
			continue
		}

		if item.NewOwner != "*" {
			lcr.SetNewOwner(item.NewOwner)
		}

		if item.NewOname != "*" {
			lcr.SetNewOname(item.NewOname)
		}
	}

	return lcr
}

//func (obj *Task_process_info) ParseDDL(taskType string) error {
//
//	var tbl []*tablecheck.TableUnit
//	var mapl []*mapcheck.MapUnit
//
//	obj.Filter = nil
//
//	CheckImp := Implementation.CheckImp{}
//	inReader := bufio.NewReader(bytes.NewBufferString(obj.DDL))
//	scanner := bufio.NewScanner(inReader)
//	scanner.Split(bufio.ScanLines)
//
//	for scanner.Scan() {
//
//		temp := scanner.Text()
//
//		if len(temp) == 0 || temp == "\r\n" || temp == "\n" || temp == "\t" {
//			continue
//		}
//
//		ddlFiter, tblFilter, mapFilter, err := CheckImp.Syntaxcheck(taskType, temp)
//		if err != nil {
//			return err
//		}
//
//		if mapFilter != nil {
//			mapl = append(mapl, mapFilter)
//		}
//
//		if tblFilter != nil {
//			tbl = append(tbl, tblFilter)
//		}
//
//		if ddlFiter != nil {
//			if obj.Filter != nil {
//				return fmt.Errorf("repeat DDL statment")
//			}
//
//			obj.Filter = ddlFiter
//		}
//	}
//
//	if obj.Filter == nil {
//		glog.Warning("DDL statment empty")
//		return nil
//	}
//
//	obj.Filter.AttachTableUnit(tbl)
//	tbl = nil
//
//	obj.Filter.AttachMapUnit(mapl)
//	mapl = nil
//
//	glog.V(log.LOG_LEVEL8).Infof("DDL:\n%s", obj.Filter.String())
//
//	return nil
//}

func (obj *Task_process_info) CreateOutBound() error {

	return oci.CreateOutBound(obj.DbAddr[0], obj.DbUserName, obj.DbUserPwd, obj.Name)
}

func (obj *Task_process_info) DropOutBound() error {

	return oci.DropOutBound(obj.DbAddr[0], obj.DbUserName, obj.DbUserPwd, obj.Name)
}

func (obj *Task_process_info) LogPrefixName() string {
	return obj.LogFileNamePrefix
}

func (obj *Task_process_info) GetDbAddr() string {
	return obj.DbAddr[0]
}

func (obj *Task_process_info) SetOrder(i int) {
	obj.Order = i
}

func (obj *Task_process_info) GetOrder() int {
	return obj.Order
}

func (obj *Task_process_info) SendTaskName(conn net.Conn) error {

	if err := conn.SetDeadline(time.Now().Add(ini.G_net_io_timeout * time.Second)); err != nil {
		return err
	}

	if err := common.WriteCmdLine(conn, obj.Name); err != nil {
		return err
	}

	return nil
}

func (obj *Task_process_info) TaskName() string {
	return obj.Name
}

func (obj *Task_process_info) GetTaskState() int {
	return obj.Status
}

func (obj *Task_process_info) SetTaskState(state int) {

	if state == PROCESS_RUNNING && obj.ShutDownFlags == true {
		obj.Status = PROCESS_STOPING
		return
	}

	obj.Status = state
}

func (obj *Task_process_info) ShutDown() {

	obj.Status = PROCESS_STOPING
	obj.ShutDownFlags = true
}

func (obj *Task_process_info) EnableTask() {
	obj.Status = PROCESS_STARTING
	obj.ShutDownFlags = false
}

func (obj *Task_process_info) RestartTask() bool {

	if obj.ShutDownFlags == true || obj.AutoStart <= 0 {
		return false
	}

	obj.AutoStart = obj.AutoStart - 1
	return true
}

func DbLogFileName(perfixName string, index int) string {
	return path.Join("../dirdat", fmt.Sprintf("%s%011d.dat", perfixName, index))
}

func dbLogFileComplate(fpath string) bool {

	fd, err := os.Open(fpath)
	if err != nil {
		return false
	}

	defer fd.Close()

	if _, err := fd.Seek(int64(len(DB_LOG_TERMINAL_FLAGS)*-1), os.SEEK_END); err != nil {
		return false
	}

	flags := make([]byte, len(DB_LOG_TERMINAL_FLAGS))

	if _, err := fd.Read(flags); err != nil {
		return false
	}

	return bytes.Equal(flags, DB_LOG_TERMINAL_FLAGS)
}

func parseGroupUnit(str string) (string, string, string, error) {

	str = strings.TrimSpace(str)

	groupUnit := strings.Split(str, " ")
	if len(groupUnit) != 2 {
		return "", "", "", fmt.Errorf("parseGroupUnit fails.[%v]", str)
	}

	groupUnit[0] = strings.TrimSpace(groupUnit[0])
	groupUnit[1] = strings.TrimSpace(groupUnit[1])

	unit := strings.Split(groupUnit[1], ".")
	if len(unit) != 2 {
		return "", "", "", fmt.Errorf("parseGroupUnit fails.[%v]", str)
	}

	unit[0] = strings.TrimSpace(unit[0])
	unit[1] = strings.TrimSpace(unit[1])

	return groupUnit[0], ConvertKey(unit[0]), ConvertKey(unit[1]), nil
}

func ConvertKey(key string) string {

	if key[0] == '"' && key[len(key)-1] == '"' {
		key = key[1 : len(key)-1]
	} else {
		key = strings.ToUpper(key)
	}

	return key
}
