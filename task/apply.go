/*************************************************************************
# File Name: manage/apply.go
# Author: xiezg
# Mail: xzghyd2008@hotmail.com
# Created Time: 2019-06-28 12:37:34
# Last modified: 2019-09-19 14:57:14
************************************************************************/
package task

import "io"
import "os"
import "net"
import "fmt"
import "path"
import "bufio"
import "time"
import "bytes"
import "io/ioutil"
import "dbbak/ini"
import "dbbak/common"
import "encoding/gob"
import "encoding/json"

type Apply_process_info struct {
	Task_process_info
	CurLogFileIndex   int
	NextLogFileOffset int64
}

func (obj *Apply_process_info) String() string {

	str := obj.Task_process_info.String()
	str = str + fmt.Sprintln("CurLogFileIndex   :", obj.CurLogFileIndex)
	str = str + fmt.Sprintln("NextLogFileOffset :", obj.NextLogFileOffset)

	return str
}

func (obj *Apply_process_info) MarshalJSON() ([]byte, error) {
	type Alias Apply_process_info

	obj.LastTranTime = time.Now().Unix()

	return json.Marshal(&struct {
		TaskType int
		*Alias
	}{
		TaskType: TASK_APPLY,
		Alias:    (*Alias)(obj),
	})
}

func (obj *Apply_process_info) GetTaskMode() string {
	return "apply"
}

func (obj *Apply_process_info) Serialize() error {

	fd, err := os.Create(path.Join("../metadata", obj.Name))
	if err != nil {
		return err
	}

	defer fd.Close()

	if n, err := fd.Write(APPLY_TASK_FLAGS); err != nil || n != len(APPLY_TASK_FLAGS) {
		return fmt.Errorf("apply task[%s] serialize fails. n:%d err:%v", obj.Name, n, err)
	}

	return gob.NewEncoder(fd).Encode(obj)
}

func (obj *Apply_process_info) RecvClientMsg(msg string) string {

	var tmp Apply_process_info

	if err := tmp.UnserializeFromJson([]byte(msg)); err == nil {
		obj.NextLogFileOffset = tmp.NextLogFileOffset
		obj.CurLogFileIndex = tmp.CurLogFileIndex
		obj.CSN = tmp.CSN
	}

	if obj.ShutDownFlags {
		return CMD_SHUTDONW
	}

	return "OK"
}

func (obj *Apply_process_info) SendPINGCmd(conn net.Conn) error {

	if err := conn.SetDeadline(time.Now().Add(ini.G_net_io_timeout * time.Second)); err != nil {
		return err
	}

	b, err := json.Marshal(obj)
	if err != nil {
		return err
	}

	if err := common.WriteCmdLine(conn, string(b)); err != nil {
		return err
	}

	return nil
}

func (obj *Apply_process_info) Unserialize(r io.Reader) error {

	if err := gob.NewDecoder(r).Decode(obj); err != nil {
		return err
	}

	if err := obj.ParseDDL(); err != nil {
		return err
	}

	return nil
}

func (obj *Apply_process_info) UnserializeFromFile(fpath string) error {

	content, err := ioutil.ReadFile(fpath)
	if err != nil {
		return err
	}

	if bytes.Equal(content[:TASK_FLAG_LENGTH], APPLY_TASK_FLAGS) == false {
		return fmt.Errorf("%s format invalid", fpath)
	}

	return obj.Unserialize(bytes.NewBuffer(content[TASK_FLAG_LENGTH:]))
}

func (obj *Apply_process_info) UnserializeFromJson(b []byte) error {

	if err := json.Unmarshal(b, obj); err != nil {
		return err
	}

	if err := obj.ParseDDL(); err != nil {
		return err
	}

	return nil
}

func (obj *Apply_process_info) SendMonitorMsg() error {

	conn, err := net.Dial("tcp", ini.G_monitor_listenAddr)
	if err != nil {
		return err
	}

	defer conn.Close()

	if err := obj.SendTaskName(conn); err != nil {
		return err
	}

	scanner := bufio.NewScanner(conn)
	for !obj.ShutDownFlags {

		time.Sleep(ini.G_heartbeat_interval * time.Second)

		if err := obj.SendPINGCmd(conn); err != nil {
			return err
		}

		if scanner.Scan() == false {
			return scanner.Err()
		}

		switch cmd := scanner.Text(); cmd {
		case "OK":
		case CMD_SHUTDONW:
			obj.ShutDownFlags = true
		default:
			fmt.Println("unknow cmd:", cmd)
		}
	}

	return nil
}
