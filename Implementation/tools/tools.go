/*************************************************************************
# File Name: Implementation/tools/tools.go
# Author: xiezg
# Mail: xzghyd2008@hotmail.com
# Created Time: 2019-09-17 11:02:37
# Last modified: 2019-09-17 11:07:23
************************************************************************/

package tools

import "strings"

func ConvertKey(key string) string {

	if key[0] == '"' && key[len(key)-1] == '"' {
		key = key[1 : len(key)-1]
	} else {
		key = strings.ToUpper(key)
	}

	return key
}
