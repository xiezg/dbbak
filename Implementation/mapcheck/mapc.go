package mapcheck

import "fmt"
import "strings"
import "dbbak/SyntaxTool"
import "dbbak/Implementation/tools"

type MapUnit struct {
	SrcUserName  string
	SrcTableName string
	DstUserName  string
	DstTableName string
}

func (obj *MapUnit) Parse(src_str, dst_str string) error {

	src := strings.Split(src_str, ".")
	dst := strings.Split(dst_str, ".")

	if len(src) != 2 || len(dst) != 2 {
		return fmt.Errorf("MapUnit parse fails. src or dst")
	}

	obj.SrcUserName = tools.ConvertKey(strings.TrimSpace(src[0]))
	obj.SrcTableName = tools.ConvertKey(strings.TrimSpace(src[1]))

	obj.DstUserName = tools.ConvertKey(strings.TrimSpace(dst[0]))
	obj.DstTableName = tools.ConvertKey(strings.TrimSpace(dst[1]))

	return nil
}

func (obj *MapUnit) String() string {

	str := ""
	str += fmt.Sprintf("[UsrName:%s TableName:%s]=>[UserName:%s TableName:%s]", obj.SrcUserName, obj.SrcTableName, obj.DstUserName, obj.DstTableName)

	return str
}

//table语法规则检查, 简单验证: TABLE httk.*;
func MAPCHECK(syntax []string) (*MapUnit, error) {

	synlen := len(syntax)
	//var LastTimeKey string    //最后一次关键字
	t := SyntaxTool.InitTABLE() //初始化TABLE关键字
	//var k SyntaxTool.KeyInfo  //声明预估语法结构体
	if synlen == 6 {

		//验证语法元素最后一个元素是否是分号.
		se := syntax[len(syntax)-1:]

		for _, v := range se {
			if v != t.SEMICOLON {
				return nil, fmt.Errorf("ERROR-DRB-00005: Syntax Unknown")
			}
		}

		for i, v := range syntax {

			if i == 0 {
				if strings.EqualFold(v, t.MAP) == false {
					return nil, fmt.Errorf("ERROR-DRB-00007: SMV-Keyword is not valid: %s", syntax)
				}
			} else if i == 1 || i == 4 {
				//效验对象名字是否合法
				onerr := SyntaxTool.ObjectNameCheck(v)
				if onerr == false {
					return nil, fmt.Errorf("ERROR-DRB-00009: ERROR-DRB-00008: SMV-Keyword Object name validation failed: %s", v)
				}
			} else if i == 2 {
				if strings.EqualFold(v, t.COMMA) == false {
					return nil, fmt.Errorf("ERROR-DRB-00007: SMV-Keyword is not valid: %s", syntax)
				}
			} else if i == 3 {
				if strings.EqualFold(v, t.TARGET) == false {
					return nil, fmt.Errorf("ERROR-DRB-00007: SMV-Keyword is not valid: %s", syntax)
				}
			} else if i == 5 {
				if strings.EqualFold(v, t.SEMICOLON) == false {
					return nil, fmt.Errorf("ERROR-DRB-00007: SMV-Keyword is not valid: %s", syntax)
				}
			} else {
				return nil, fmt.Errorf("ERROR-DRB-00007: SMV-Keyword is not valid: %s", syntax)
			}
		}

		var unit MapUnit

		if err := unit.Parse(syntax[1], syntax[4]); err != nil {
			return nil, err
		}

		return &unit, nil

	}

	return nil, fmt.Errorf("ERROR-DRB-00007: SMV-Keyword is not valid: %s", syntax)
}

//table语法规则检查, 简单验证: TABLE httk.*;
func MAPEXCLUDECHECK(syntax []string) error {
	synlen := len(syntax)
	//var LastTimeKey string    //最后一次关键字
	t := SyntaxTool.InitTABLE() //初始化TABLE关键字
	//var k SyntaxTool.KeyInfo  //声明预估语法结构体
	if synlen == 3 {

		//验证语法元素最后一个元素是否是分号.
		se := syntax[len(syntax)-1:]
		for _, v := range se {
			if strings.EqualFold(v, t.SEMICOLON) == false {
				return fmt.Errorf("ERROR-DRB-00005: Syntax Unknown")
			}
		}

		for i, v := range syntax {
			if i == 0 {
				if strings.EqualFold(v, t.MAPEXCLUDE) == false {
					return fmt.Errorf("ERROR-DRB-00007: SMV-Keyword is not valid: %s", syntax)
				}
			} else if i == 1 {
				//效验对象名字是否合法
				onerr := SyntaxTool.ObjectNameCheck(v)
				if onerr == false {
					return fmt.Errorf("ERROR-DRB-00009: ERROR-DRB-00008: SMV-Keyword Object name validation failed: %s", v)
				}
			} else if i == 2 {
				if strings.EqualFold(v, t.SEMICOLON) == false {
					return fmt.Errorf("ERROR-DRB-00007: SMV-Keyword is not valid: %s", syntax)
				}
			} else {
				return fmt.Errorf("ERROR-DRB-00007: SMV-Keyword is not valid: %s", syntax)
			}
		}
		return nil

	}

	return fmt.Errorf("ERROR-DRB-00007: SMV-Keyword is not valid: %s", syntax)
}
