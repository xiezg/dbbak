package tablecheck

import (
	"dbbak/Implementation/tools"
	"dbbak/SyntaxTool"
	"fmt"
	"strings"
)

type TableUnit struct {
	UserName  string
	TableName string
}

func (obj *TableUnit) Match(Owner string, Oname string) bool {

	if obj.UserName != "*" && obj.UserName != Owner {
		return false
	}

	if obj.TableName != "*" && obj.TableName != Oname {
		return false
	}

	return true
}

func (obj *TableUnit) Parse(val string) error {

	if n := strings.Count(val, "."); n != 1 {
		return fmt.Errorf("TableUnit parse fails. statemnt[%s] dot symbol count[%d]", val, n)
	}

	result := strings.Split(val, ".")

	if len(result) != 2 {
		return fmt.Errorf("TableUnit parse fails. Split statment[%s] with dot[%v]", val, result)
	}

	obj.UserName = tools.ConvertKey(strings.TrimSpace(result[0]))
	obj.TableName = tools.ConvertKey(strings.TrimSpace(result[1]))

	return nil
}

func (obj *TableUnit) String() string {

	str := ""
	str += fmt.Sprintf("UserName:%s TableName:%s", obj.UserName, obj.TableName)
	return str
}

//table语法规则检查, 简单验证: TABLE httk.*;
func TABLECHECK(syntax []string) (*TableUnit, error) {
	synlen := len(syntax)
	//var LastTimeKey string    //最后一次关键字
	t := SyntaxTool.InitTABLE() //初始化TABLE关键字
	//var k SyntaxTool.KeyInfo  //声明预估语法结构体
	if synlen == 3 {

		//验证语法元素最后一个元素是否是分号.
		se := syntax[len(syntax)-1:]
		for _, v := range se {
			if v != t.SEMICOLON {
				return nil, fmt.Errorf("ERROR-DRB-00005: Syntax Unknown")
			}
		}

		for i, v := range syntax {
			if i == 0 {
				if strings.EqualFold(v, t.TABLE) == false {
					return nil, fmt.Errorf("ERROR-DRB-00007: SMV-Keyword is not valid: %s", syntax)
				}
			} else if i == 1 {
				//效验对象名字是否合法
				onerr := SyntaxTool.ObjectNameCheck(v)
				if onerr == false {
					return nil, fmt.Errorf("ERROR-DRB-00009: ERROR-DRB-00008: SMV-Keyword Object name validation failed: %s\n", v)
				}
			} else if i == 2 {
				if strings.EqualFold(v, t.SEMICOLON) == false {
					return nil, fmt.Errorf("ERROR-DRB-00007: SMV-Keyword is not valid: %s", syntax)
				}
			} else {
				return nil, fmt.Errorf("ERROR-DRB-00007: SMV-Keyword is not valid: %s", syntax)
			}
		}

		var unit TableUnit

		if err := unit.Parse(syntax[1]); err != nil {
			return nil, err
		}

		return &unit, nil

	}

	return nil, fmt.Errorf("ERROR-DRB-00007: SMV-Keyword is not valid: %s", syntax)
}

//table语法规则检查, 简单验证: TABLE httk.*;
func TABLEEXCLUDECHECK(syntax []string) error {
	synlen := len(syntax)
	//var LastTimeKey string    //最后一次关键字
	t := SyntaxTool.InitTABLE() //初始化TABLE关键字
	//var k SyntaxTool.KeyInfo  //声明预估语法结构体
	fmt.Println(syntax)
	if synlen == 3 {

		//验证语法元素最后一个元素是否是分号.
		se := syntax[len(syntax)-1:]
		fmt.Println(t.SEMICOLON)
		for _, v := range se {
			if strings.EqualFold(v, t.SEMICOLON) == false {
				return fmt.Errorf("ERROR-DRB-00005: Syntax Unknown ")
			}
		}

		for i, v := range syntax {
			if i == 0 {
				if strings.EqualFold(v, t.TABLEEXCLUDE) == false {
					return fmt.Errorf("ERROR-DRB-00007: SMV-Keyword is not valid: %s\n", syntax)
				}
			} else if i == 1 {
				//效验对象名字是否合法
				onerr := SyntaxTool.ObjectNameCheck(v)
				if onerr == false {
					return fmt.Errorf("ERROR-DRB-00009: ERROR-DRB-00008: SMV-Keyword Object name validation failed: %s", v)
				}
			} else if i == 2 {
				if strings.EqualFold(v, t.SEMICOLON) == false {
					return fmt.Errorf("ERROR-DRB-00007: SMV-Keyword is not valid: %s", syntax)
				}
			} else {
				return fmt.Errorf("ERROR-DRB-00007: SMV-Keyword is not valid: %s", syntax)
			}
		}
		return nil

	}

	return fmt.Errorf("ERROR-DRB-00007: SMV-Keyword is not valid: %s", syntax)
}
