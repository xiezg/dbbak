package Implementation

import "fmt"
import "strings"
import "dbbak/SyntaxTool"
import "dbbak/Implementation/ddlcheck"
import "dbbak/Implementation/mapcheck"
import "dbbak/Implementation/tablecheck"

type CheckImp struct {
}

func (i *CheckImp) Syntaxcheck(SynTaxType string, contents string) (*ddlcheck.DDlFiter, *tablecheck.TableUnit, *mapcheck.MapUnit, error) {
	if strings.EqualFold(SynTaxType, "EXTRACT") == true {
		return ExtractCheck(contents)
	} else if strings.EqualFold(SynTaxType, "REPLICAT") == true {
		return ReplicatCheck(contents)
	} else {
		err := fmt.Errorf("ERROR-DBR-00001: Syntax type is not supported")
		return nil, nil, nil, err
	}

	return nil, nil, nil, nil
}

func ExtractCheck(contents string) (*ddlcheck.DDlFiter, *tablecheck.TableUnit, *mapcheck.MapUnit, error) {

	var ddl *ddlcheck.DDlFiter
	var table *tablecheck.TableUnit

	if len(contents) > 0 {
		d := SyntaxTool.InitDDL()
		t := SyntaxTool.InitTABLE()
		syncon := contents

		//去除前后空格
		syncon = SyntaxTool.ClearBackspace(syncon)
		if len(syncon) == 0 {
			return nil, nil, nil, fmt.Errorf("ERROR-DRB-00003: Syntax content should not be empty")
		}

		//检查是否是分号结尾
		if strings.HasSuffix(syncon, d.SEMICOLON) == false {
			return nil, nil, nil, fmt.Errorf("ERROR-DRB-00006: Syntax is not a semicolon ending: %s\n", syncon)
		}

		//查询TAB符号是否存在.
		inds := strings.Index(syncon, "\t")
		if inds != -1 {
			return nil, nil, nil, fmt.Errorf("ERROR-DRB-00010: tabulation(tab) symbols are not allowed: %s", syncon)
		}

		//检查注释符
		bl := SyntaxTool.CommentCheck(syncon)
		if bl == true {
			return nil, nil, nil, fmt.Errorf("WARNING-DRB-00004: Syntax has Comment symbols that are skipped: %s", contents)
		} else {
			sc, err := SyntaxTool.Format(syncon)
			if err != nil {
				return nil, nil, nil, err
			} else {
				switch {
				case strings.EqualFold(sc[0], d.DDL) == true:
					//检查ddl语法中的关键字
					ddl, err = ddlcheck.DDLCHECK(sc)
					if err != nil {
						return nil, nil, nil, err
					}
				case strings.EqualFold(sc[0], t.TABLE) == true:
					table, err = tablecheck.TABLECHECK(sc)
					if err != nil {
						return nil, nil, nil, err
					}
				case strings.EqualFold(sc[0], t.TABLEEXCLUDE) == true:
					err = tablecheck.TABLEEXCLUDECHECK(sc)
					if err != nil {
						return nil, nil, nil, err
					}

				default:
					return nil, nil, nil, fmt.Errorf("ERROR-DRB-00005: Syntax Unknown: %s", contents)
				}

			}
		}
	}

	return ddl, table, nil, nil
}

func ReplicatCheck(contents string) (*ddlcheck.DDlFiter, *tablecheck.TableUnit, *mapcheck.MapUnit, error) {

	var ddl *ddlcheck.DDlFiter
	var table *tablecheck.TableUnit
	var mapObj *mapcheck.MapUnit

	if len(contents) > 0 {
		d := SyntaxTool.InitDDL()
		t := SyntaxTool.InitTABLE()
		syncon := contents

		//去除前后空格
		syncon = SyntaxTool.ClearBackspace(syncon)
		if len(syncon) == 0 {
			return nil, nil, nil, fmt.Errorf("ERROR-DRB-00003: Syntax content should not be empty")
		}

		//检查是否是分号结尾
		if strings.HasSuffix(syncon, d.SEMICOLON) == false {
			return nil, nil, nil, fmt.Errorf("ERROR-DRB-00006: Syntax is not a semicolon ending: %s", syncon)
		}

		//查询TAB符号是否存在.
		inds := strings.Index(syncon, "\t")
		if inds != -1 {
			return nil, nil, nil, fmt.Errorf("ERROR-DRB-00010: tabulation(tab) symbols are not allowed: %s", syncon)
		}

		//检查注释符
		bl := SyntaxTool.CommentCheck(syncon)
		if bl == true {
			return nil, nil, nil, fmt.Errorf("WARNING-DRB-00004: Syntax has Comment symbols that are skipped: %s", contents)
		} else {
			sc, err := SyntaxTool.Format(syncon)
			if err != nil {
				return nil, nil, nil, err
			} else {
				switch {
				case strings.EqualFold(sc[0], d.DDL) == true:
					//检查ddl语法中的关键字
					ddl, err = ddlcheck.DDLCHECK(sc)
					if err != nil {
						return nil, nil, nil, err
					}
				case strings.EqualFold(sc[0], t.MAP) == true:
					mapObj, err = mapcheck.MAPCHECK(sc)
					if err != nil {
						return nil, nil, nil, err
					}
				case strings.EqualFold(sc[0], t.MAPEXCLUDE) == true:
					err = mapcheck.MAPEXCLUDECHECK(sc)
					if err != nil {
						return nil, nil, nil, err
					}

				default:
					return nil, nil, nil, fmt.Errorf("ERROR-DRB-00005: Syntax Unknown: %s", contents)
				}

			}
		}
	}

	return ddl, table, mapObj, nil
}
