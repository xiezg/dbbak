package ddlcheck

import "fmt"
import "strings"
import "dbbak/SyntaxTool"
import "dbbak/Implementation/mapcheck"
import "dbbak/Implementation/tablecheck"

type FilteraUnit struct {
	CmdType    string
	OBJECTNAME string              //"Default:*"
	Other      map[string][]string //actionType actionValue
}

func (obj *FilteraUnit) Match(objType, objName, operType string) bool {
	return false
}

func (obj *FilteraUnit) String() string {

	str := ""
	str += fmt.Sprintf("CDMTYPE:%s\n", obj.CmdType)
	str += fmt.Sprintf("OBJECTNAME:%s\n", obj.OBJECTNAME)
	for key, val := range obj.Other {
		str += fmt.Sprintf("%s:%v\n", key, val)
	}

	return str
}

func (obj *FilteraUnit) Parse(val []string) error {

	obj.Other = make(map[string][]string)
	obj.OBJECTNAME = "*"

	for i := 0; i < len(val); i += 2 {
		switch val[i] {
		case "OBJECTNAME":
			obj.OBJECTNAME = val[i+1]
		case "OBJECTTYPE", "OPERATIONTYPE":
			obj.Other[val[i]] = append(obj.Other[val[i]], val[i+1])
		default:
			return fmt.Errorf("unknow ddl FilteraUnit[%s]", val[i])
		}
	}

	return nil
}

type DDlFiter struct {
	Map      []*mapcheck.MapUnit
	Mapped   []*tablecheck.TableUnit
	UnMapped []*tablecheck.TableUnit
	All      bool
	Unit     []FilteraUnit
}

func (obj *DDlFiter) Match(Owner, objType, Oname, operType string) bool {

	if obj.All {
		return true
	}

	if objType == "TABLE" || objType == "INDEX" {

		for _, item := range obj.Mapped {
			if item.Match(Owner, Oname) {
				return true
			}
		}
	}

	return false
}

func (obj *DDlFiter) String() string {

	str := ""

	if len(obj.Mapped) > 0 {
		str += fmt.Sprintf("MAPPED\n")
		for _, item := range obj.Mapped {
			str += item.String()
			str += "\n"
		}
		str += "\n"
	}

	if len(obj.UnMapped) > 0 {
		str += fmt.Sprintf("UNMAPPED\n")
		for _, item := range obj.UnMapped {
			str += item.String()
			str += "\n"
		}
		str += "\n"
	}

	if len(obj.Map) > 0 {
		str += fmt.Sprintf("MAP\n")
		for _, item := range obj.Map {
			str += item.String()
			str += "\n"
		}
		str += "\n"
	}

	if obj.All {
		str += fmt.Sprintf("ALL\n")
		str += "\n"
	}

	for _, val := range obj.Unit {
		str += val.String()
		str += "\n"
	}

	return str
}

func (obj *DDlFiter) AttachMapUnit(mapl []*mapcheck.MapUnit) {

	obj.Map = append(obj.Map, mapl...)
}

func (obj *DDlFiter) AttachTableUnit(tbl []*tablecheck.TableUnit) {

	if obj.Mapped != nil {
		obj.Mapped = append(obj.Mapped, tbl...)
		return
	}

	if obj.UnMapped != nil {
		obj.UnMapped = append(obj.UnMapped, tbl...)
		return
	}
}

func (obj *DDlFiter) Parse(val []string) error {

	for {

		var group []string
		var unit FilteraUnit

		cmdType := val[0]
		val = val[1:]

		for i := 0; i < len(val); i++ {
			if val[i] == "INCLUDE" || val[i] == "EXCLUDE" {
				group = val[:i]
				break
			}
		}

		if group == nil {
			group = val
		}

		if len(group) == 1 {
			switch group[0] {
			case "MAPPED":
				obj.Mapped = make([]*tablecheck.TableUnit, 0)
			case "UNMAPPED":
				obj.UnMapped = make([]*tablecheck.TableUnit, 0)
			case "ALL":
				if obj.All {
					return fmt.Errorf("repeat All statment")
				}
				obj.All = true
			default:
				return fmt.Errorf("unknow ddl DDlFiter:[%s]", group[0])
			}
		} else {

			if err := unit.Parse(group); err != nil {
				return err
			}

			if cmdType == "INCLUDE" || cmdType == "EXCLUDE" {
				unit.CmdType = cmdType
				obj.Unit = append(obj.Unit, unit)
			} else {
				return fmt.Errorf("unknow ddl DDlFiter[%s]", cmdType)
			}
		}

		if len(group) >= len(val) {
			break
		}

		val = val[len(group):]
	}

	return nil
}

//DDL语法规则检查
func DDLCHECK(syntax []string) (*DDlFiter, error) {

	var filter DDlFiter
	result := make([]string, 0)

	synlen := len(syntax)
	var LastTimeKey string    //最后一次关键字
	d := SyntaxTool.InitDDL() //初始化DDL关键字
	var k SyntaxTool.KeyInfo  //声明预估语法结构体
	if synlen > 0 {

		//验证语法元素最后一个元素是否是分号.
		se := syntax[len(syntax)-1:]
		for _, v := range se {
			if v != d.SEMICOLON {
				return nil, fmt.Errorf("ERROR-DRB-00005: Syntax Unknown ")
			}
		}

		//循环语法切片
		for i, vsx := range syntax {

			//fmt.Println(i, vsx)

			if i == 0 {
				//第一个位置必须是DDL, 如果不是根本进不到这一层. 拿到一个关键字, 就可以预估下一个关键字.
				if strings.EqualFold(vsx, d.DDL) == true {
					LastTimeKey = d.DDL
					result = append(result, "DDL")
				} else {
					return nil, fmt.Errorf("DDLCHECK INCLUDE parse fails.")
				}
			} else if i > 0 {
				if LastTimeKey == d.OBJECTNAME {

					result = append(result, "OBJECTNAME")
					result = append(result, vsx)

					//fmt.Println("LastTimeKey:", LastTimeKey, "  ", vsx)
					//效验对象名字是否合法
					onerr := SyntaxTool.ObjectNameCheck(vsx)
					if onerr == false {
						return nil, fmt.Errorf("ERROR-DRB-00009: ERROR-DRB-00008: SMV-Keyword Object name validation failed: %s", vsx)
					}

					LastTimeKey = d.OBJNAME

				} else if LastTimeKey == d.OPERATIONTYPE {
					LastTimeKey = d.OPERTYPE
					result = append(result, "OPERATIONTYPE")
					result = append(result, vsx)
				} else if LastTimeKey == d.OBJECTTYPE {
					LastTimeKey = d.OBJTYPE
					result = append(result, "OBJECTTYPE")
					result = append(result, vsx)
				} else {
					//调用预估关键字接口, 如果预估不到关键字, 不会返回任何数据.

					data := k.RecDDLKey(LastTimeKey)

					//fmt.Println( "index:", i, "vsx:", vsx, "LastTimeKey:", LastTimeKey, "data:", data)
					//判断预估关键字是否是nil
					if data != nil {
						ok := SyntaxTool.KeyCompare(vsx, data)
						if ok == false {
							return nil, fmt.Errorf("ERROR-DRB-00007: SMV-Keyword is not valid: %s", vsx)
						}

						LastTimeKey = vsx

						if strings.EqualFold(vsx, d.INCLUDE) {
							result = append(result, "INCLUDE")
						} else if strings.EqualFold(vsx, d.ALL) {
							result = append(result, "ALL")
						} else if strings.EqualFold(vsx, d.EXCLUDE) {
							result = append(result, "EXCLUDE")
						} else if strings.EqualFold(vsx, d.MAPPED) {
							result = append(result, "MAPPED")
						} else if strings.EqualFold(vsx, d.UNMAPPED) {
							result = append(result, "UNMAPPED")
						}
					} else {
						return nil, fmt.Errorf("ERROR-DRB-00007: SMV-Keyword is not valid: (%s) - %s", vsx, syntax)
					}
				}

			}
		}

		//fmt.Println( "DDLCHECK:", result )

		if err := filter.Parse(result[1:]); err != nil {
			return nil, err
		}

		//fmt.Println("DDLCHECK:", result)
		//fmt.Println(filter.String())

		return &filter, nil
	}

	return nil, fmt.Errorf("ERROR-DRB-00007: SMV-Keyword is not valid: %s", syntax)
}
