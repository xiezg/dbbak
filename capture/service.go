/*************************************************************************
# File Name: capture/service.go
# Author: xiezg
# Mail: xzghyd2008@hotmail.com
# Created Time: 2019-06-28 18:20:39
# Last modified: 2019-09-19 18:01:51
************************************************************************/
package capture

import "os"
import "io"
import "fmt"
import "time"
import "sync"
import "path"
import "bytes"
import "dbbak/oci"
import "dbbak/log"
import "dbbak/task"
import "github.com/golang/glog"

type LCRChunk struct {
	Buf          io.Reader
	LastPosition []byte
}

const (
	MAX_BUF_SIZE = 32 * 1024 * 1024
)

var m chan LCRChunk
var c chan oci.LCR
var count int

func SerializeLCRBufToFile(capInfo *task.Capture_process_info, outbound *oci.OutBound) error {

	fd, err := os.OpenFile(capInfo.ScanNextLogFileName(), os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}

	defer fd.Close()

	ticker := time.NewTicker(time.Second * 3)
	defer ticker.Stop()

	for !capInfo.ShutDownFlags {
		select {
		case item := <-m:
			if _, err := io.Copy(fd, item.Buf); err != nil {
				return err
			}

			if err := fd.Sync(); err != nil {
				return err
			}

			if err := outbound.LWMSet(item.LastPosition); err != nil {
				return err
			}

			scn, err := outbound.GetSCNFromPosition(item.LastPosition)
			if err != nil {
				return err
			}

			capInfo.CSN = scn
		case <-ticker.C:
		}

		finfo, err := fd.Stat()
		if err != nil {
			return err
		}

		if finfo.Size() >= int64(capInfo.LogFileSize*1024*1024) {
			break
		}
	}

	if capInfo.ShutDownFlags {
		glog.Info("SerializeLCRBufToFile task exit")
		return nil
	}

	if _, err := fd.Write(task.DB_LOG_TERMINAL_FLAGS); err != nil {
		return err
	}

	return nil
}

func SerializeLCRToBuffer(capInfo *task.Capture_process_info) error {

	buf := new(bytes.Buffer)
	var lastPos []byte

	ticker := time.NewTicker(time.Second * 3)
	defer ticker.Stop()

	for !capInfo.ShutDownFlags {
		select {
		case lcr := <-c:
			glog.V(log.LOG_LEVEL8).Info(lcr.String())
			lastPos = lcr.GetPosition()
			if err := lcr.Serialize(buf); err != nil {
				return err
			}

			if buf.Len() >= MAX_BUF_SIZE {
				m <- LCRChunk{buf, lcr.GetPosition()}
				buf = new(bytes.Buffer)
			}
		case <-ticker.C:
			if buf.Len() > 0 {
				m <- LCRChunk{buf, lastPos}
				buf = new(bytes.Buffer)
			}
		}
	}

	if capInfo.ShutDownFlags {
		glog.Info("SerializeLCRToBuffer task exit")
		return nil
	}

	return nil
}

func Start(name string, printInfo bool) (err error) {

	var wg sync.WaitGroup
	c = make(chan oci.LCR, 1000)
	m = make(chan LCRChunk, 10)

	defer func() {
		if err != nil {
			glog.Error(name, " err:", err)
		}
	}()

	var capInfo task.Capture_process_info

	if err := capInfo.UnserializeFromFile(path.Join("../metadata", name)); err != nil {
		return err
	}

	capInfo.ShutDownFlags = false
	glog.Info("metadata:\n", capInfo.String())

	if printInfo {
		fmt.Println(capInfo.String())
		return nil
	}

	outbound, err := oci.InitOutbound(capInfo.Name, capInfo.DbAddr[0], capInfo.DbUserName, capInfo.DbUserPwd)
	if err != nil {
		return err
	}

	wg.Add(1)
	go func() {
		defer wg.Done()
		for !capInfo.ShutDownFlags {
			if err := SerializeLCRBufToFile(&capInfo, outbound); err != nil {
				glog.Error("SerializeLCRBufToFile fails.", err)
				os.Exit(0)
			}
		}
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		if err := SerializeLCRToBuffer(&capInfo); err != nil {
			glog.Error("SerializeLCRToBuffer fails.", err)
			os.Exit(0)
		}
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		for !capInfo.ShutDownFlags {
			lcr, scn, err := outbound.Get_lcrs()
			if err != nil {
				glog.Error("Get_lcrs fails.", err)
				capInfo.ShutDownFlags = true
				return
			}

			if scn != 0 {
				capInfo.CSN = scn
			}

			if lcr == nil {
				glog.Info("lcr null")
				continue
			}

			if !capInfo.Match(lcr) {
				glog.Warningf("lcr [%s]Filter drop", lcr.String())
				if err := outbound.LWMSet(lcr.GetPosition()); err != nil {
					glog.Error(err)
					capInfo.ShutDownFlags = true
					return
				}

				continue
			}

			lcr = capInfo.MatchAndMapped(lcr)

			lcr.String()
			c <- lcr
		}

		if capInfo.ShutDownFlags {
			glog.Info("Get_lcrs task exit")
		}

	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		for !capInfo.ShutDownFlags {
			capInfo.SendMonitorMsg()
			time.Sleep(time.Second * 1)
		}

		if capInfo.ShutDownFlags {
			glog.Info("Monitor task exit")
		}
	}()

	wg.Wait()

	glog.Info("task exit")

	return nil
}
