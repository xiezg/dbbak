/*************************************************************************
# File Name: manage/apply.go
# Author: xiezg
# Mail: xzghyd2008@hotmail.com
# Created Time: 2019-07-21 12:51:18
# Last modified: 2019-07-24 17:37:08
************************************************************************/
package manage

//import "io"
//import "fmt"
//import "sync"
//import "dbbak/ini"
//import "dbbak/task"
//import "encoding/json"
//
//var applyTaskMap sync.Map
//
//type apply_process_stop_cmd struct {
//	Name string `json:"Name"`
//}
//
//type apply_process_start_cmd struct {
//	Name string `json:"Name"`
//}
//
//type apply_process_stat_cmd struct {
//	Name string `json:"Name"`
//}
//
//type apply_process_delete_cmd struct {
//	Name []string `json:"Name"`
//}
//
//func load_apply_proc(r io.Reader) error {
//
//	var applyInfo task.Apply_process_info
//
//	if err := applyInfo.Unserialize(r); err != nil {
//		return err
//	}
//
//	applyInfo.ShutDown = false
//	applyInfo.Status = PROCESS_STOPED
//
//	if _, ok := applyTaskMap.LoadOrStore(applyInfo.Name, &applyInfo); ok == true {
//		return fmt.Errorf("apply instance has exists in memory")
//	}
//
//	return ForkMyself(applyInfo.Name, "apply")
//}
//
//func create_apply_proc(b []byte) (interface{}, error) {
//
//	var applyInfo task.Apply_process_info
//
//	if err := json.Unmarshal(b, &applyInfo); err != nil {
//		return nil, err
//	}
//
//	applyInfo.Status = PROCESS_STOPED
//
//	if _, ok := applyTaskMap.LoadOrStore(applyInfo.Name, &applyInfo); ok == true {
//		return nil, fmt.Errorf("apply instance has exists in memory")
//	}
//
//	if ini.IsExistFile(applyInfo.Name) {
//		return nil, fmt.Errorf("apply instance has exists in file")
//	}
//
//	if err := applyInfo.Serialize(); err != nil {
//		return nil, fmt.Errorf("create_apply_proc instance fails. err:%v", err)
//	}
//
//	return nil, ForkMyself(applyInfo.Name, "apply")
//}
//
//func start_apply_proc(b []byte) (interface{}, error) {
//
//	var cmd apply_process_start_cmd
//
//	if err := json.Unmarshal(b, &cmd); err != nil {
//		return nil, err
//	}
//
//	obj, ok := applyTaskMap.Load(cmd.Name)
//	if !ok {
//		return nil, fmt.Errorf("can't find apply instance")
//	}
//
//	applyInfo := obj.(*task.Apply_process_info)
//
//	if applyInfo.Status != PROCESS_STOPED {
//		return nil, fmt.Errorf("apply instance not ready")
//	}
//
//	applyInfo.ShutDown = false
//	applyInfo.Status = PROCESS_STARTING
//
//	return nil, ForkMyself(applyInfo.Name, "apply")
//}
//
//func stop_apply_proc(b []byte) (interface{}, error) {
//
//	var cmd apply_process_stop_cmd
//
//	if err := json.Unmarshal(b, &cmd); err != nil {
//		return nil, err
//	}
//
//	obj, ok := applyTaskMap.Load(cmd.Name)
//	if !ok {
//		return nil, fmt.Errorf("can't find apply instance")
//	}
//
//	applyInfo := obj.(*task.Apply_process_info)
//
//	if applyInfo.Status != PROCESS_RUNNING {
//		return nil, fmt.Errorf("apply instance not ready")
//	}
//
//	applyInfo.ShutDown = true
//
//	return nil, nil
//}
//
//func delete_apply_proc(b []byte) (interface{}, error) {
//
//    var cmd apply_process_delete_cmd
//
//	if err := json.Unmarshal(b, &cmd); err != nil {
//		return nil, err
//	}
//
//	for _, name := range cmd.Name {
//
//		obj, ok := applyTaskMap.Load(name)
//		if !ok {
//			return nil, fmt.Errorf("can't find apply instance")
//		}
//
//		applyInfo := obj.(*task.Apply_process_info)
//
//		if applyInfo.Status != PROCESS_STOPED {
//			return nil, fmt.Errorf("apply instance not ready")
//		}
//
//		applyTaskMap.Delete(name)
//	}
//
//	return nil, nil
//}
//
//func stat_apply_proc(b []byte) (interface{}, error) {
//
//	var cmd apply_process_stat_cmd
//
//	if err := json.Unmarshal(b, &cmd); err != nil {
//		return nil, err
//	}
//
//	obj, ok := applyTaskMap.Load(cmd.Name)
//	if !ok {
//		return nil, fmt.Errorf("can't find apply instance")
//	}
//
//	applyInfo := obj.(*task.Apply_process_info)
//
//	return applyInfo.Status, nil
//}
//
//func test_apply_proc_connect(b []byte) (interface{}, error) {
//
//	var applyInfo task.Apply_process_info
//
//	fmt.Println("test_apply_proc_connect:", string(b))
//
//	if err := json.Unmarshal(b, &applyInfo); err != nil {
//		return nil, err
//	}
//
//	return nil, nil
//}
//
//func proc_apply_beatheart_cmd(appInfo *task.Apply_process_info, cmd string) (string, error) {
//	return "OK", nil
//}
