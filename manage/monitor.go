/*************************************************************************
# File Name: manage/monitor.go
# Author: xiezg
# Mail: xzghyd2008@hotmail.com
# Created Time: 2019-06-29 18:41:57
# Last modified: 2019-09-16 17:50:28
************************************************************************/
package manage

import "os"
import "fmt"
import "net"
import "time"
import "bufio"
import "syscall"
import "os/signal"
import "dbbak/ini"
import "dbbak/task"
import "dbbak/common"
import "github.com/golang/glog"

func handle_child_process() {

	c := make(chan os.Signal, 1)

	signal.Notify(c, syscall.SIGCHLD)

	for {
		<-c
		syscall.Wait4(-1, nil, 0, nil)
	}
}

func handle_task_proc_message(conn net.Conn) (err error) {

	defer conn.Close()

	defer func() {
		if err != nil {
			glog.Error(err)
		}
	}()

	scanner := bufio.NewScanner(conn)

	if err := conn.SetDeadline(time.Now().Add(ini.G_net_io_timeout * time.Second)); err != nil {
		return err
	}

	if !scanner.Scan() {
		return fmt.Errorf("get task name fails. %T %v", scanner.Err(), scanner.Err())
	}

	value, ok := GlobalTaskMap.Load(scanner.Text())
	if ok == false {
		return fmt.Errorf("can't find instance:%s", scanner.Text())
	}

	obj := value.(task.DbTask)

RETRY_WITH_TIMEOUT:

	if err := conn.SetDeadline(time.Now().Add(ini.G_net_io_timeout * time.Second)); err != nil {
		return err
	}

	for scanner.Scan() {

		response := obj.RecvClientMsg(scanner.Text())

		common.WriteCmdLine(conn, response)

		if err := conn.SetDeadline(time.Now().Add(ini.G_net_io_timeout * time.Second)); err != nil {
			return err
		}

		obj.SetTaskState(task.PROCESS_RUNNING)
	}

	if scanner.Err() != nil {
		obj.SetTaskState(task.PROCESS_FAULT)
		scanner = bufio.NewScanner(conn)
		goto RETRY_WITH_TIMEOUT
	}

	obj.SetTaskState(task.PROCESS_STOPED)
	if obj.RestartTask() == false {
		return nil
	}

	time.Sleep(ini.G_restart_task_interval)
	return ForkMyself(obj.TaskName(), obj.GetTaskMode())
}

func start_monitor_server() error {

	ln, err := net.Listen("tcp", ini.G_monitor_listenAddr)
	if err != nil {
		return err
	}

	defer ln.Close()

	for {

		conn, err := ln.Accept()
		if err != nil {
			return err
		}

		go handle_task_proc_message(conn)
	}
}
