/*************************************************************************
# File Name: manage/service.go
# Author: xiezg
# Mail: xzghyd2008@hotmail.com
# Created Time: 2019-05-31 16:57:51
# Last modified: 2019-09-17 17:20:41
************************************************************************/
package manage

import "io"
import "os"
import "fmt"
import "sync"
import "time"
import "sort"
import "path"
import "bytes"
import "net/http"
import "dbbak/oci"
import "dbbak/ini"
import "dbbak/task"
import "path/filepath"
import "encoding/json"
import "github.com/gorilla/mux"
import "github.com/golang/glog"

type process_stop_cmd struct {
	Name string `json:"Name"`
}

type process_start_cmd struct {
	Name string `json:"Name"`
}

type process_stat_cmd struct {
	Name string `json:"Name"`
}

type process_delete_cmd struct {
	Name []string `json:"Name"`
}

type response struct {
	Ret  int         `json:"rst"`
	Msg  string      `json:"msg"`
	Data interface{} `json:"data"`
}

type task_search_cmd struct {
	Level0 string `json:"level0"`
	Level1 string `json:"level1"`
	Name   string `json:"Name"`
}

var GlobalTaskMap sync.Map
var GloablTaskSize int

var AUTH_FAILS error = fmt.Errorf("login fails")

func common_response(w http.ResponseWriter, data interface{}, err error) {

	var rsp response = response{Ret: 0, Msg: "success", Data: data}

	if err != nil {
		rsp.Ret = 1
		rsp.Msg = err.Error()
	}

	if err == AUTH_FAILS {
		rsp.Ret = 2
	}

	b, err := json.Marshal(rsp)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	io.WriteString(w, string(b))
}

func load_task(fpath string) (err error) {

	defer func() {
		if err != nil {
			fmt.Sprintf("load_task[%s] fails.err:%v", fpath, err)
		}
	}()

	flags := make([]byte, task.TASK_FLAG_LENGTH, task.TASK_FLAG_LENGTH)

	fd, err := os.Open(fpath)
	if err != nil {
		return err
	}

	defer fd.Close()

	if _, err := io.ReadFull(fd, flags); err != nil {
		return err
	}

	if bytes.Equal(flags, task.CAPTURE_TASK_FLAGS) {
		return load_task_from_file(new(task.Capture_process_info), fpath)
	}

	if bytes.Equal(flags, task.APPLY_TASK_FLAGS) {
		return load_task_from_file(new(task.Apply_process_info), fpath)
	}

	return fmt.Errorf("invalid flags:%v", flags)
}

func scan_task_store_file() {

	filepath.Walk("../metadata", func(fpath string, info os.FileInfo, err error) error {
		if err != nil {
			fmt.Println(fpath, err)
			return nil
		}

		if info.IsDir() {
			return nil
		}

		if err := load_task(fpath); err != nil {
			glog.Errorf("load task[%s] fails. err:%v", fpath, err)
		}

		return nil
	})
}

func list_tree_task(b []byte) (interface{}, error) {

	list, err := list_task(b)
	if err != nil {
		return nil, err
	}

	taskList := list.([]task.DbTask)

	capTaskGroup := make(map[string][]interface{})
	applyTaskGroup := make(map[string][]task.DbTask)

	for _, val := range taskList {
		if val.GetTaskMode() == "apply" {
			applyTaskGroup[val.LogPrefixName()] = append(applyTaskGroup[val.LogPrefixName()], val)
		}
	}

	for _, val := range taskList {
		if val.GetTaskMode() == "capture" {
			capTaskGroup[val.GetDbAddr()] = append(capTaskGroup[val.GetDbAddr()], map[string]interface{}{"capture": val, "apply": applyTaskGroup[val.LogPrefixName()]})
		}
	}

	return capTaskGroup, nil
}

func list_task(b []byte) (interface{}, error) {

	var filter task_search_cmd

	if err := json.Unmarshal(b, &filter); err != nil {
		return nil, err
	}

	list := make([]task.DbTask, 0, 100)

	GlobalTaskMap.Range(func(key, value interface{}) bool {

		if filter.Name != "" && key.(string) != filter.Name {
			return true
		}

		list = append(list, value.(task.DbTask))
		return true
	})

	sort.Slice(list, func(i, j int) bool {
		return list[i].GetOrder() < list[j].GetOrder()
	})

	return list, nil
}

func load_task_from_file(obj task.DbTask, fpath string) error {

	if err := obj.UnserializeFromFile(fpath); err != nil {
		return err
	}

	obj.SetTaskState(task.PROCESS_STOPED)

	if _, ok := GlobalTaskMap.LoadOrStore(obj.TaskName(), obj); ok == true {
		return fmt.Errorf("instance has exist in memory")
	}

	GloablTaskSize += 1

	go func(name string) {
		time.Sleep(5 * time.Second)

		val, ok := GlobalTaskMap.Load(name)
		if !ok {
			return
		}

		obj := val.(task.DbTask)

		if obj.GetTaskState() == task.PROCESS_STOPED {
			ForkMyself(obj.TaskName(), obj.GetTaskMode())
		}
	}(obj.TaskName())

	return nil
}

func create_apply_proc(b []byte) (interface{}, error) {

	return nil, create_task_from_json(new(task.Apply_process_info), b)
}

func create_capture_proc(b []byte) (interface{}, error) {

	return nil, create_task_from_json(new(task.Capture_process_info), b)
}

func create_task_from_json(obj task.DbTask, b []byte) error {

	if err := obj.UnserializeFromJson(b); err != nil {
		return err
	}

	obj.SetTaskState(task.PROCESS_STOPED)

	if _, ok := GlobalTaskMap.LoadOrStore(obj.TaskName(), obj); ok == true {
		return fmt.Errorf("instance has exist in memory")
	}

	GloablTaskSize += 1

	if ini.IsExistFile(obj.TaskName()) {
		return fmt.Errorf("instance has exist in file")
	}

	obj.SetOrder(GloablTaskSize)

	if obj.GetTaskMode() == "capture" {

		if err := obj.CreateOutBound(); err != nil {
			glog.Errorf("CreateOutBound fails. err:%v", err)
			GlobalTaskMap.Delete(obj.TaskName())
			GloablTaskSize -= 1
			return err
		}
	}

	if err := obj.Serialize(); err != nil {
		return err
	}

	return ForkMyself(obj.TaskName(), obj.GetTaskMode())
}

func stop_proc(b []byte) (interface{}, error) {

	var cmd process_stop_cmd

	if err := json.Unmarshal(b, &cmd); err != nil {
		return nil, err
	}

	val, ok := GlobalTaskMap.Load(cmd.Name)
	if !ok {
		return nil, fmt.Errorf("can't find instance")
	}

	obj := val.(task.DbTask)

	if obj.GetTaskState() != task.PROCESS_RUNNING {
		return nil, fmt.Errorf("task not ready")
	}

	obj.ShutDown()

	return nil, nil
}

func start_proc(b []byte) (interface{}, error) {

	var cmd process_start_cmd

	if err := json.Unmarshal(b, &cmd); err != nil {
		return nil, err
	}

	val, ok := GlobalTaskMap.Load(cmd.Name)
	if !ok {
		return nil, fmt.Errorf("can't find instance")
	}

	obj := val.(task.DbTask)

	if obj.GetTaskState() != task.PROCESS_STOPED {
		return nil, fmt.Errorf("task not ready")
	}

	obj.EnableTask()

	return nil, ForkMyself(obj.TaskName(), obj.GetTaskMode())
}

func delete_proc(b []byte) (interface{}, error) {

	var cmd process_delete_cmd

	if err := json.Unmarshal(b, &cmd); err != nil {
		return nil, err
	}

	for _, item := range cmd.Name {
		val, ok := GlobalTaskMap.Load(item)
		if !ok {
			return nil, fmt.Errorf("can't find instance")
		}

		obj := val.(task.DbTask)

		if obj.GetTaskState() != task.PROCESS_STOPED {
			return nil, fmt.Errorf("task not ready")
		}

		if err := os.Remove(path.Join("../metadata", obj.TaskName())); err != nil {
			return nil, err
		}

		GlobalTaskMap.Delete(item)

		if obj.GetTaskMode() == "capture" {
			if err := obj.DropOutBound(); err != nil {
				glog.Errorf("DropOutBound fails. err:%v", err)
				return nil, err
			}
		}
	}

	return nil, nil
}

func stat_proc(b []byte) (interface{}, error) {
	return nil, nil
}

func conn_test(b []byte) (interface{}, error) {

	var cmd task.Task_process_info

	if err := json.Unmarshal(b, &cmd); err != nil {
		return nil, err
	}

	if len(cmd.DbAddr) == 0 {
		return nil, fmt.Errorf("invalid dbaddr information")
	}

	return nil, oci.OracleDBConnTest(cmd.DbAddr[0], cmd.DbUserName, cmd.DbUserPwd)
}

func Start() {

	scan_task_store_file()

	go handle_child_process()

	go start_monitor_server()

	r := mux.NewRouter()

	r.HandleFunc("/dbbak/login", login)
	r.HandleFunc("/dbbak/logout", auth(logout))

	r.HandleFunc("/dbbak/task/list", auth(list_task))
	r.HandleFunc("/dbbak/task/list/tree", auth(list_tree_task))

	r.HandleFunc("/dbbak/apply/create", auth(create_apply_proc))
	r.HandleFunc("/dbbak/capture/create", auth(create_capture_proc))
	r.HandleFunc("/dbbak/{taskId:apply|capture}/stop", auth(stop_proc))
	r.HandleFunc("/dbbak/{taskId:apply|capture}/start", auth(start_proc))
	r.HandleFunc("/dbbak/{taskId:apply|capture}/delete", auth(delete_proc))
	r.HandleFunc("/dbbak/{taskId:apply|capture}/stat", auth(stat_proc))
	r.HandleFunc("/dbbak/{taskId:apply|capture}/test/connect", auth(conn_test))

	err := http.ListenAndServe(ini.G_http_listenAddr, r)
	fmt.Println("ListenAndServe fails", ini.G_http_listenAddr, err)
}
