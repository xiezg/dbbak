/*************************************************************************
# File Name: manage/common.go
# Author: xiezg
# Mail: xzghyd2008@hotmail.com
# Created Time: 2019-06-29 10:26:08
# Last modified: 2019-07-25 15:30:47
************************************************************************/
package manage

const (
	CAPTURE_TYPE_DML = 1
	CAPTURE_TYPE_DDL = 2
	CAPTURE_TYPE_ALL = 3
)
