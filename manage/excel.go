/*************************************************************************
# File Name: manage/excel.go
# Author: xiezg
# Mail: xzghyd2008@hotmail.com
# Created Time: 2019-06-28 18:10:59
# Last modified: 2019-08-16 11:00:29
************************************************************************/
package manage

//#include <sys/types.h>
//#include <stdlib.h>
//#include <unistd.h>
//#include <stdio.h>
//int start_process(const char *exec_path, const char *exec_name, const char * start_mode, const char *logLevel, const char *logdir ){
//  return execl( exec_path, exec_name, "-name", exec_name, "-mode", start_mode, "-v", logLevel, "-log_dir", logdir, NULL );
//}
import "C"
import "os"
import "fmt"
import "unsafe"
import "dbbak/ini"
import "dbbak/log"
import "github.com/golang/glog"

func ForkMyself(exec_name string, start_mode string) error {

	logdir := "../logs/process/" + exec_name
	if err := os.MkdirAll(logdir, 0755); err != nil {
		return err
	}

	arg0 := C.CString(os.Args[0])
	defer C.free(unsafe.Pointer(arg0))

	c_exec_name := C.CString(exec_name)
	defer C.free(unsafe.Pointer(c_exec_name))

	c_start_mode := C.CString(start_mode)
	defer C.free(unsafe.Pointer(c_start_mode))

	c_logLevel := C.CString(fmt.Sprintf("%d", ini.G_taskLevel))
	defer C.free(unsafe.Pointer(c_logLevel))

	c_logDir := C.CString(logdir)
	defer C.free(unsafe.Pointer(c_logDir))

	glog.V(log.LOG_LEVEL6).Infof("create task name:%s mode:%s begin", exec_name, start_mode)

	pid, err := C.fork()

	if pid > 0 {
		glog.V(log.LOG_LEVEL6).Infof("create task name:%s mode:%s complate", exec_name, start_mode)
		return nil
	}

	if pid < 0 {
		glog.Errorf("create task fails. name:%s mode:%s", exec_name, start_mode)
		return err
	}

	result, err := C.start_process(arg0, c_exec_name, c_start_mode, c_logLevel, c_logDir)
	if result == -1 {
		glog.Errorf("start process %s exec_name:%s start_mode:%s fails. err:%v", os.Args[0], exec_name, start_mode, err)
		os.Exit(-1)
	}

	return nil
}
