/*************************************************************************
# File Name: manage/capture.go
# Author: xiezg
# Mail: xzghyd2008@hotmail.com
# Created Time: 2019-07-21 12:51:36
# Last modified: 2019-07-24 17:37:09
************************************************************************/
package manage

//import "io"
//import "fmt"
//import "dbbak/ini"
//import "dbbak/task"
//import "encoding/json"

//func load_capture_proc(r io.Reader) error {
//
//	var capInfo task.Capture_process_info
//
//	if err := capInfo.Unserialize(r); err != nil {
//		return err
//	}
//
//	capInfo.ShutDown = false
//	capInfo.Status = PROCESS_STOPED
//
//	if _, ok := captureTaskMap.LoadOrStore(capInfo.Name, &capInfo); ok == true {
//		return fmt.Errorf("capture instance has exists in memory")
//	}
//
//	return ForkMyself(capInfo.Name, "capture")
//}
//
//func create_capture_proc(b []byte) (interface{}, error) {
//
//	var capInfo task.Capture_process_info
//
//	if err := json.Unmarshal(b, &capInfo); err != nil {
//		return nil, err
//	}
//
//	capInfo.Status = PROCESS_STOPED
//
//	if _, ok := captureTaskMap.LoadOrStore(capInfo.Name, &capInfo); ok == true {
//		return nil, fmt.Errorf("capture instance has exists in memory")
//	}
//
//	if ini.IsExistFile(capInfo.Name) {
//		return nil, fmt.Errorf("capture instance has exists in file")
//	}
//
//	if err := capInfo.Serialize(); err != nil {
//		return nil, fmt.Errorf("create_capture_proc instance fails. err:%v", err)
//	}
//
//	return nil, ForkMyself(capInfo.Name, "capture")
//}
//
//func start_capture_proc(b []byte) (interface{}, error) {
//
//	var cmd capture_process_start_cmd
//
//	if err := json.Unmarshal(b, &cmd); err != nil {
//		return nil, err
//	}
//
//	obj, ok := captureTaskMap.Load(cmd.Name)
//	if !ok {
//		return nil, fmt.Errorf("can't find capture instance")
//	}
//
//	capInfo := obj.(*task.Capture_process_info)
//
//	if capInfo.Status != PROCESS_STOPED {
//		return nil, fmt.Errorf("capture instance not ready")
//	}
//
//	capInfo.ShutDown = false
//	capInfo.Status = PROCESS_STARTING
//
//	return nil, ForkMyself(capInfo.Name, "capture")
//}
//
//func stop_capture_proc(b []byte) (interface{}, error) {
//
//	var cmd capture_process_stop_cmd
//
//	if err := json.Unmarshal(b, &cmd); err != nil {
//		return nil, err
//	}
//
//	obj, ok := captureTaskMap.Load(cmd.Name)
//	if !ok {
//		return nil, fmt.Errorf("can't find capture instance")
//	}
//
//	capInfo := obj.(*task.Capture_process_info)
//
//	if capInfo.Status != PROCESS_RUNNING {
//		return nil, fmt.Errorf("capture instance not ready")
//	}
//
//	capInfo.ShutDown = true
//
//	return nil, nil
//}
//
//func delete_capture_proc(b []byte) (interface{}, error) {
//
//	var cmd capture_process_delete_cmd
//
//	if err := json.Unmarshal(b, &cmd); err != nil {
//		return nil, err
//	}
//
//	for _, name := range cmd.Name {
//
//		obj, ok := captureTaskMap.Load(name)
//		if !ok {
//			return nil, fmt.Errorf("can't find capture instance")
//		}
//
//		capInfo := obj.(*task.Capture_process_info)
//
//		if capInfo.Status != PROCESS_STOPED {
//			return nil, fmt.Errorf("capture instance not ready")
//		}
//
//		captureTaskMap.Delete(name)
//	}
//
//	return nil, nil
//}
//
//func stat_capture_proc(b []byte) (interface{}, error) {
//
//	var cmd capture_process_stat_cmd
//
//	if err := json.Unmarshal(b, &cmd); err != nil {
//		return nil, err
//	}
//
//	obj, ok := captureTaskMap.Load(cmd.Name)
//	if !ok {
//		return nil, fmt.Errorf("can't find capture instance")
//	}
//
//	capInfo := obj.(*task.Capture_process_info)
//
//	return capInfo.Status, nil
//}
//
//func test_capture_proc_connect(b []byte) (interface{}, error) {
//
//	var capInfo task.Capture_process_info
//
//	if err := json.Unmarshal(b, &capInfo); err != nil {
//		return nil, err
//	}
//
//	return nil, nil
//}
