/*************************************************************************
# File Name: manage/auth.go
# Author: xiezg
# Mail: xzghyd2008@hotmail.com
# Created Time: 2019-06-28 12:35:21
# Last modified: 2019-09-14 20:10:23
************************************************************************/
package manage

import "fmt"
import "sync"
import "time"
import "net/http"
import "io/ioutil"
import "dbbak/ini"
import "crypto/md5"
import "encoding/hex"
import "encoding/json"

var cookieMap sync.Map
var session_maxAge int = 7 * 24 * 3600

type account_info struct {
	Name string `json:"name"`
	Pwd  string `json:"password"`
}

func logout(param []byte) (interface{}, error) {
	return nil, nil
}

func login(w http.ResponseWriter, r *http.Request) {

	var accInfo account_info

	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		common_response(w, nil, err)
		return
	}

	if err := json.Unmarshal(b, &accInfo); err != nil {
		common_response(w, nil, err)
		return
	}

	pwd, exists := ini.G_AccountMap[accInfo.Name]
	if !exists || pwd != accInfo.Pwd {
		common_response(w, nil, fmt.Errorf("login fails"))
		return
	}

	sessId := md5.Sum([]byte(time.Now().String() + accInfo.Name))

	cookie := &http.Cookie{
		Name:   "jsessionid",
		Value:  hex.EncodeToString(sessId[:]),
		Path:   "/",
		MaxAge: session_maxAge,
		Secure: false,
	}

	cookieMap.Store(hex.EncodeToString(sessId[:]), accInfo.Name)

	http.SetCookie(w, cookie)
	common_response(w, nil, nil)
}

func auth(proc func(b []byte) (interface{}, error)) func(w http.ResponseWriter, r *http.Request) {

	return func(w http.ResponseWriter, r *http.Request) {

		defer r.Body.Close()

		cookie, err := r.Cookie("jsessionid")
		if err != nil {
			common_response(w, nil, AUTH_FAILS)
			return
		}

		if _, ok := cookieMap.Load(cookie.Value); !ok {
			common_response(w, nil, AUTH_FAILS)
			return
		}

		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			http.Error(w, fmt.Sprintf("%v", err), http.StatusBadRequest)
			return
		}

		result, err := proc(body)
		common_response(w, result, err)
	}
}
