/*************************************************************************
# File Name: common/common.go
# Author: xiezg
# Mail: xzghyd2008@hotmail.com
# Created Time: 2019-07-02 02:40:34
# Last modified: 2019-07-25 11:38:48
************************************************************************/
package common

import "net"

func WriteCmdLine(conn net.Conn, cmd string) error {

	cmd = cmd + "\r\n"

	b := ([]byte)(cmd)

	for len(b) > 0 {
		n, err := conn.Write(b)
		if err != nil {
			return err
		}

		b = b[n:]
	}

	return nil
}
